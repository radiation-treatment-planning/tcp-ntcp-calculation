# Tcp Ntcp calculation

A .Net Framework 4.7.2 library for Tumor Control Probability (TCP) and Normal Tissue Complication Probability (NTCP) calculations in radiation therapy.

Foundational library is [RadiationTreatmentPlanner.Utils](https://gitlab.com/radiation-treatment-planning/radiation-treatment-planner-utils).

## Installation
Build it from source or install it via [NuGet](https://www.nuget.org/packages/TcpNtcpCalculation/).

## Available calculators
All calculators are based on the Linear-Quadratic (LQ) model. Following calculators are available

### TCP calculators
- Poisson TCP calculator with normalized volume correction
- Logistic TCP calculator with normalized volume correction
- Poisson TCP with cell density and $\alpha$ calculator

### NTCP calculators
- Relative Seriality Poisson NTCP calculator
- Relative Seriality Logistiy NTCP calculator
- Lyman-Kutcher-Burman (LKB) NTCP calculator

### Other calculators
- Probability of Cure calculator
- Probability of Injury calculator
- Biologically effective uniform dose calculator
- Complication Free Tumor Control ($P+$) calculator
- TCP Curve calculator
- NTCP Curve calculator

## Usage
1. Create a Dose-Volume Histogram (DVH) as described [here](https://gitlab.com/radiation-treatment-planning/radiation-treatment-planner-utils) in detail.

```csharp
var doseVolumePoints = new List<Tuple<UDose, UVolume>>
{
    new Tuple<UDose, UVolume>(new UDose(69, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
    new Tuple<UDose, UVolume>(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(2, UVolume.VolumeUnit.ccm)),
    new Tuple<UDose, UVolume>(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm))
};
var dvhCurve = new DVHCurve(doseVolumePoints, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
```
2. Initialize a calculator

```csharp
var calculator = new PoissonTcpWithDensityAndAlphaCalculator(new ClonogenicCellDensity(2.8E8),
    new Alpha(0.1205),
    new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(1.6, UDose.UDoseUnit.Gy)));
```
3. Calculate the desired properties
```csharp
var numberOfTreatmentFractions = 12u;
var result = calculator.Calculate(dvhCurve, numberOfTreatmentFractions);
```

For more examples see `TcpNtcpCalculation.Tests` project.

# Literature
- Allen Li, X., Alber, M., Deasy, J.O., Jackson, A., Ken Jee, K.-W., Marks, L.B., Martel, M.K., Mayo, C., Moiseenko, V., Nahum, A.E., Niemierko, A., Semenenko, V.A. and Yorke, E.D. (2012), The use and QA of biologically related models for treatment planning: Short report of the TG-166 of the therapy physics committee of the AAPM. Med. Phys., 39: 1386-1409. \
https://doi.org/10.1118/1.3685447
- Francesco Tommasino, Alan Nahum, & Laura Cella (2017). Increasing the power of tumour control and normal tissue complication probability modelling in radiotherapy: recent trends and current issues. Translational Cancer Research, 6(Suppl 5). \
https://doi.org/10.21037/tcr.2017.06.03
