﻿using RadiationTreatmentPlanner.Utils.DVH;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    /// <summary>
    /// An interface for tumor control probability calculation.
    /// </summary>
    public interface ITumorControlProbabilityCalculator
    {
        /// <summary>
        /// Description of tumor control probability calculator.
        /// </summary>
        string Description { get; }


        /// <summary>
        /// Calculates the Tumor Control Probability for non-uniform dose distributions from a DVHCurve.
        /// </summary>
        /// <param name="dvhCurve">A list of UDose, UVolume tuples.
        ///     An ascending sorting of the dose values is assumed. The UDoses must be the total
        ///     physical dose.</param>
        /// <param name="numberOfFractions">Number of fractions.</param>
        /// <returns>The tumor control probability, also called total response.</returns>
        TumorControlProbability Calculate(DVHCurve dvhCurve, uint numberOfFractions);

        /// <summary>
        /// Get a dictionary with all utilized parameters of the TCP Calculator.
        /// </summary>
        /// <returns>TcpNtcpParametersDictionary</returns>
        TcpNtcpParametersDictionary GetParametersDictionary();
    }
}
