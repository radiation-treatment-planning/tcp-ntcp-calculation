﻿using System;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.DBarBar;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation
{
    public static class BiologicallyEffectiveUniformDoseCalculator
    {
        public static BiologicallyEffectiveUniformDose CalculateBiologicallyEffectiveUniformDose(
            this ITumorControlProbabilityCalculator tcpCalculator,
            TumorControlProbability tcp, uint numberOfFractions, UVolume absoluteStructureVolume)
        {
            if (tcpCalculator == null) throw new ArgumentNullException(nameof(tcpCalculator));
            if (absoluteStructureVolume == null) throw new ArgumentNullException(nameof(absoluteStructureVolume));
            if (numberOfFractions == 0) throw new ArgumentOutOfRangeException(nameof(numberOfFractions)); if (absoluteStructureVolume == null) throw new ArgumentNullException(nameof(absoluteStructureVolume));
            if (!absoluteStructureVolume.IsAbsolute())
                throw new ArgumentException(
                    $"Absolute structure volume is required for {nameof(absoluteStructureVolume)}, " +
                    $"but was {absoluteStructureVolume.Unit}.");

            var tcpDBarBarCurve = tcpCalculator.CalculateTcpAndDBarBarCurve(numberOfFractions, absoluteStructureVolume);
            return tcpDBarBarCurve.GetDBarBarWithTcp(tcp);
        }
        public static BiologicallyEffectiveUniformDose CalculateBiologicallyEffectiveUniformDose(
            this INormalTissueComplicationProbabilityCalculator ntcpCalculator,
            NormalTissueComplicationProbability ntcp, uint numberOfFractions, UVolume absoluteStructureVolume)
        {
            if (ntcpCalculator == null) throw new ArgumentNullException(nameof(ntcpCalculator));
            if (absoluteStructureVolume == null) throw new ArgumentNullException(nameof(absoluteStructureVolume));
            if (numberOfFractions == 0) throw new ArgumentOutOfRangeException(nameof(numberOfFractions));
            if (!absoluteStructureVolume.IsAbsolute())
                throw new ArgumentException(
                    $"Absolute structure volume is required for {nameof(absoluteStructureVolume)}, " +
                    $"but was {absoluteStructureVolume.Unit}.");

            var tcpDBarBarCurve = ntcpCalculator.CalculateNtcpAndDBarBarCurve(numberOfFractions, absoluteStructureVolume);
            return tcpDBarBarCurve.GetDBarBarWithNtcp(ntcp);
        }

    }
}
