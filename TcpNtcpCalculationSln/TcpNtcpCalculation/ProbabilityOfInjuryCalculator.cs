﻿using System;
using System.Collections.Generic;
using System.Linq;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation
{
    /// <summary>
    /// A class for calculating the probability of injury. A.k.a.: total/overall
    /// normal tissue complication probability 
    /// <see href="https://books.google.de/books?id=azE8DQAAQBAJ"/>.
    /// </summary>
    public class ProbabilityOfInjuryCalculator
    {
        public IEnumerable<NtcpCalculationConfig> NtcpCalculationConfigs { get; }

        public ProbabilityOfInjuryCalculator(IEnumerable<NtcpCalculationConfig> ntcpCalculationConfigs)
        {
            NtcpCalculationConfigs =
                ntcpCalculationConfigs ?? throw new ArgumentNullException(nameof(ntcpCalculationConfigs));
            if (!ntcpCalculationConfigs.Any())
                throw new ArgumentException(
                    $"At least 1 {nameof(ntcpCalculationConfigs)} must be given, but 0 were obtained.");
        }

        public ProbabilityOfInjuryCalculationResult Calculate(IEnumerable<StructureDvhTuple> structureDvhTuples,
            uint numberOfFractions)
        {
            if (structureDvhTuples == null) throw new ArgumentNullException(nameof(structureDvhTuples));
            if (numberOfFractions < 1)
                throw new ArgumentOutOfRangeException($"{nameof(numberOfFractions)} must be larger 1, but was 0");

            var ntcpPoints = new List<NtcpPoint>(structureDvhTuples.Count());
            foreach (var ntcpCalculationConfig in NtcpCalculationConfigs)
            {
                var structureId = ntcpCalculationConfig.StructureId;
                var structureDvhTuple =
                    structureDvhTuples.FirstOrDefault(x => x.StructureId == structureId);
                if (structureDvhTuple == null)
                    throw new ArgumentException($"No DVH curve for structure with ID {structureId} is available.");
                var ntcp = ntcpCalculationConfig.NtcpCalculator.Calculate(structureDvhTuple.DvhCurve,
                    numberOfFractions);
                ntcpPoints.Add(new NtcpPoint(structureDvhTuple.StructureId, ntcp, ntcpCalculationConfig.Weight));
            }

            var probabilityOfInjury = 1.0;
            foreach (var ntcpPoint in ntcpPoints)
                probabilityOfInjury *= ntcpPoint.Weight * (1 - ntcpPoint.NormalTissueComplicationProbability.Value);

            probabilityOfInjury = 1 - probabilityOfInjury;

            return new ProbabilityOfInjuryCalculationResult(ntcpPoints, new ProbabilityOfInjury(probabilityOfInjury));
        }

        protected bool Equals(ProbabilityOfInjuryCalculator other)
        {
            return Equals(NtcpCalculationConfigs, other.NtcpCalculationConfigs);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ProbabilityOfInjuryCalculator)obj);
        }

        public override int GetHashCode()
        {
            return (NtcpCalculationConfigs != null ? NtcpCalculationConfigs.GetHashCode() : 0);
        }
    }
}
