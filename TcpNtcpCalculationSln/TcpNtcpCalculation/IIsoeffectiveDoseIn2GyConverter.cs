﻿using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    public interface IIsoeffectiveDoseIn2GyConverter
    {
        /// <summary>
        /// Convert physical dose in iso-effective dose in 2Gy fractions.
        /// </summary>
        /// <param name="physicalDose">Physical dose.</param>
        /// <param name="numberOfFractions">Number of fractions (>0).</param>
        /// <returns></returns>
        IsoeffectiveDoseIn2Gy Convert(UDose physicalDose, uint numberOfFractions);

        /// <summary>
        /// Get a dictionary with all utilized parameters.
        /// </summary>
        /// <returns>TcpNtcpParametersDictionary</returns>
        TcpNtcpParametersDictionary GetParametersDictionary();
    }
}
