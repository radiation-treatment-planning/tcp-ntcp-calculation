﻿using System;
using Optional;

namespace TcpNtcpCalculation.Parameters
{
    public class TcpNtcpParametersDictionaryBuilder
    {
        private readonly TcpNtcpParametersDictionary _dict;

        private TcpNtcpParametersDictionaryBuilder()
        {
            _dict = new TcpNtcpParametersDictionary();
        }

        public static TcpNtcpParametersDictionaryBuilder Initialize() => new TcpNtcpParametersDictionaryBuilder();

        public TcpNtcpParametersDictionaryBuilder WithParameterSet(string parameterId, double parameterValue,
            Option<string> parameterUnit)
        {
            if (parameterId == null) throw new ArgumentNullException(nameof(parameterId));
            _dict.AddParameterSet(parameterId, new ValueUnitTuple(parameterValue, parameterUnit));
            return this;
        }

        public TcpNtcpParametersDictionary Build() => _dict;

        protected bool Equals(TcpNtcpParametersDictionaryBuilder other)
        {
            return Equals(_dict, other._dict);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpNtcpParametersDictionaryBuilder)obj);
        }

        public override int GetHashCode()
        {
            return (_dict != null ? _dict.GetHashCode() : 0);
        }
    }
}
