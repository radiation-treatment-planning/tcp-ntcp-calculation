﻿using System;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Parameters
{
    public class TargetVolumeParameters
    {
        public FiftyPercentControlRate D50 { get; private set; }
        public Slope Gamma { get; private set; }
        public AlphaOverBeta AlphaOverBeta { get; private set; }

        private TargetVolumeParameters()
        {
        }

        public static TargetVolumeParameters CreateCustomParameters(FiftyPercentControlRate d50, Slope gamma, AlphaOverBeta alphaOverBeta)
        {
            return new TargetVolumeParameters
            {
                D50 = d50,
                Gamma = gamma,
                AlphaOverBeta = alphaOverBeta
            };
        }

        protected bool Equals(TargetVolumeParameters other)
        {
            return D50.Equals(other.D50) && Gamma.Equals(other.Gamma) && AlphaOverBeta.Equals(other.AlphaOverBeta);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TargetVolumeParameters)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(D50, Gamma, AlphaOverBeta);
        }
    }
}
