﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Parameters
{
    public class TcpNtcpParameterConfig
    {
        public OrganAtRiskParameters OrganAtRisk { get; private set; }
        public TargetVolumeParameters TargetVolume { get; private set; }

        private TcpNtcpParameterConfig()
        {

        }

        public static TcpNtcpParameterConfig CreateDefaultConfigForPoissonModel()
        {
            return new TcpNtcpParameterConfig
            {
                TargetVolume = TargetVolumeParameters.CreateCustomParameters(
                    new FiftyPercentControlRate(23.73, UDose.UDoseUnit.Gy), new Slope(0.487),
                    new AlphaOverBeta(26.53, UDose.UDoseUnit.Gy)),
                OrganAtRisk = OrganAtRiskParameters.CreateCustomParameters(
                    new FiftyPercentControlRate(56.3, UDose.UDoseUnit.Gy), new Slope(2.3),
                    new AlphaOverBeta(3.0, UDose.UDoseUnit.Gy), new DegreeOfSeriality(1.5))
            };
        }

        public static TcpNtcpParameterConfig CreateCustom(OrganAtRiskParameters organAtRiskParameters,
            TargetVolumeParameters targetVolumeParameters)
        {
            if (!(organAtRiskParameters.AlphaOverBeta.DoseUnit == organAtRiskParameters.D50.DoseUnit
                  && organAtRiskParameters.D50.DoseUnit == targetVolumeParameters.D50.DoseUnit
                  && targetVolumeParameters.D50.DoseUnit == targetVolumeParameters.AlphaOverBeta.DoseUnit))
                throw new ArgumentException($"All dose units must be equal, but were:\n" +
                                            $"Organ at risk:\n" +
                                            $"- D50: {organAtRiskParameters.D50.DoseUnit}\n" +
                                            $"- AlphaOverBeta: {organAtRiskParameters.AlphaOverBeta.DoseUnit}\n" +
                                            $"Target volume:\n" +
                                            $"- D50: {targetVolumeParameters.D50.DoseUnit}\n" +
                                            $"- AlphaOverBeta: {targetVolumeParameters.AlphaOverBeta.DoseUnit}");

            return new TcpNtcpParameterConfig
            { OrganAtRisk = organAtRiskParameters, TargetVolume = targetVolumeParameters };
        }

        public static TcpNtcpParameterConfig CreateDefaultConfigForLogisticModel()
        {
            return CreateCustom(
                OrganAtRiskParameters.CreateCustomParameters(new FiftyPercentControlRate(56.3, UDose.UDoseUnit.Gy),
                    new Slope(2.3), new AlphaOverBeta(3, UDose.UDoseUnit.Gy), new DegreeOfSeriality(1.5)),
                TargetVolumeParameters.CreateCustomParameters(new FiftyPercentControlRate(25.37, UDose.UDoseUnit.Gy),
                    new Slope(0.801), new AlphaOverBeta(27.45, UDose.UDoseUnit.Gy)));
        }

        protected bool Equals(TcpNtcpParameterConfig other)
        {
            return Equals(OrganAtRisk, other.OrganAtRisk) && Equals(TargetVolume, other.TargetVolume);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpNtcpParameterConfig)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(OrganAtRisk, TargetVolume);
        }
    }
}
