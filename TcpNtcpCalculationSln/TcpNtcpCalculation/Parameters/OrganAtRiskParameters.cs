﻿using System;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Parameters
{
    public class OrganAtRiskParameters
    {
        public AlphaOverBeta AlphaOverBeta { get; private set; }
        public Slope Gamma { get; private set; }
        public FiftyPercentControlRate D50 { get; private set; }
        public DegreeOfSeriality DegreeOfSeriality { get; private set; }

        private OrganAtRiskParameters()
        {
        }

        public static OrganAtRiskParameters CreateCustomParameters(FiftyPercentControlRate d50, Slope gamma, AlphaOverBeta alphaOverBeta,
            DegreeOfSeriality degreeOfSeriality)
        {
            return new OrganAtRiskParameters
            {
                AlphaOverBeta = alphaOverBeta,
                Gamma = gamma,
                D50 = d50,
                DegreeOfSeriality = degreeOfSeriality
            };
        }

        protected bool Equals(OrganAtRiskParameters other)
        {
            return AlphaOverBeta.Equals(other.AlphaOverBeta) && Gamma.Equals(other.Gamma) && D50.Equals(other.D50) &&
                   DegreeOfSeriality.Equals(other.DegreeOfSeriality);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((OrganAtRiskParameters)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(AlphaOverBeta, Gamma, D50, DegreeOfSeriality);
        }
    }
}
