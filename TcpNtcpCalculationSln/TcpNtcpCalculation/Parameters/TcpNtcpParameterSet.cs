﻿using System;

namespace TcpNtcpCalculation.Parameters
{
    public class TcpNtcpParameterSet
    {
        public string ParameterId { get; }
        public ValueUnitTuple ValueUnitTuple { get; }

        public TcpNtcpParameterSet(string parameterId, ValueUnitTuple valueUnitTuple)
        {
            ParameterId = parameterId;
            ValueUnitTuple = valueUnitTuple;
        }

        protected bool Equals(TcpNtcpParameterSet other)
        {
            return ParameterId == other.ParameterId && Equals(ValueUnitTuple, other.ValueUnitTuple);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpNtcpParameterSet) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ParameterId, ValueUnitTuple);
        }
    }
}