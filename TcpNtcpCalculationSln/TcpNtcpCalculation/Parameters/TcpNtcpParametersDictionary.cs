﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TcpNtcpCalculation.Parameters
{
    public class TcpNtcpParametersDictionary
    {
        private readonly Dictionary<string, ValueUnitTuple> _dict;

        public TcpNtcpParametersDictionary()
        {
            _dict = new Dictionary<string, ValueUnitTuple>();
        }

        public void AddParameterSet(string parameterId, ValueUnitTuple valueUnitTuple)
        {
            if (parameterId == null) throw new ArgumentNullException(nameof(parameterId));
            if (valueUnitTuple == null) throw new ArgumentNullException(nameof(valueUnitTuple));
            _dict.Add(parameterId, valueUnitTuple);
        }

        public IEnumerable<TcpNtcpParameterSet> GetAllParameterSets()
        {
            return _dict.Select(x => new TcpNtcpParameterSet(x.Key, x.Value));
        }

        protected bool Equals(TcpNtcpParametersDictionary other)
        {
            if (_dict.Count != other._dict.Count) return false;
            foreach (var paramSet in _dict)
            {
                if (!other._dict.ContainsKey(paramSet.Key)) return false;
                ValueUnitTuple tuple;
                var success = other._dict.TryGetValue(paramSet.Key, out tuple);
                if (!success) return false;
                if (!Equals(paramSet.Value, tuple)) return false;
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpNtcpParametersDictionary)obj);
        }

        public override int GetHashCode()
        {
            return (_dict != null ? _dict.GetHashCode() : 0);
        }
    }
}
