﻿using System;
using Optional;

namespace TcpNtcpCalculation.Parameters
{
    public class ValueUnitTuple
    {
        public double Value { get; }
        public Option<string> Unit { get; }

        public ValueUnitTuple(double value, Option<string> unit)
        {
            Value = value;
            Unit = unit;
        }

        protected bool Equals(ValueUnitTuple other)
        {
            return Value.Equals(other.Value) && Unit.Equals(other.Unit);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ValueUnitTuple) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value, Unit);
        }
    }
}