﻿using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    public interface IVoxelResponseCalculationStrategy
    {
        FiftyPercentControlRate D50 { get; }
        Slope Gamma { get; }

        /// <summary>
        /// Calculate voxel response or response of a DVH bin.
        /// </summary>
        /// <param name="isoeffectiveDoseIn2Gy">The EQD2 dose in Gy.</param>
        /// <returns>Voxel response.</returns>
        /// 
        VoxelResponse Calculate(IsoeffectiveDoseIn2Gy isoeffectiveDoseIn2Gy);

        /// <summary>
        /// Get a dictionary with all utilized parameters.
        /// </summary>
        /// <returns>TcpNtcpParametersDictionary</returns>
        TcpNtcpParametersDictionary GetParametersDictionary();
    }
}
