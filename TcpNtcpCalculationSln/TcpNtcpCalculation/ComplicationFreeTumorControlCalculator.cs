﻿using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation
{
    public class ComplicationFreeTumorControlCalculator : IComplicationFreeTumorControlCalculator
    {
        public ComplicationFreeTumorControl Calculate(TcpNtcpResult tcpNtcpResult)
        {
            var tcp = tcpNtcpResult.TumorControlProbability;
            var ntcp = tcpNtcpResult.NormalTissueComplicationProbability;
            return new ComplicationFreeTumorControl(tcp.Value * (1.0 - ntcp.Value));
        }

        public ComplicationFreeTumorControl Calculate(ProbabilityOfCureCalculationResult probabilityOfCureCalculationResult,
            ProbabilityOfInjuryCalculationResult probabilityOfInjuryCalculationResult)
        {
            var probabilityOfCure = probabilityOfCureCalculationResult.ProbabilityOfCure;
            var probabilityOfInjury = probabilityOfInjuryCalculationResult.ProbabilityOfInjury;
            return new ComplicationFreeTumorControl(probabilityOfCure.Value * (1.0 - probabilityOfInjury.Value));
        }
    }
}
