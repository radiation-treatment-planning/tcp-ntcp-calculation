﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public struct TcpNtcpResult
    {
        public TumorControlProbability TumorControlProbability { get; }
        public NormalTissueComplicationProbability NormalTissueComplicationProbability { get; }

        public TcpNtcpResult(TumorControlProbability tumorControlProbability, NormalTissueComplicationProbability normalTissueComplicationProbability)
        {
            TumorControlProbability = tumorControlProbability;
            NormalTissueComplicationProbability = normalTissueComplicationProbability;
        }

        public bool Equals(TcpNtcpResult other)
        {
            return TumorControlProbability.Equals(other.TumorControlProbability) &&
                   NormalTissueComplicationProbability.Equals(other.NormalTissueComplicationProbability);
        }

        public override bool Equals(object obj)
        {
            return obj is TcpNtcpResult other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TumorControlProbability, NormalTissueComplicationProbability);
        }

        public static bool operator ==(TcpNtcpResult left, TcpNtcpResult right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(TcpNtcpResult left, TcpNtcpResult right)
        {
            return !left.Equals(right);
        }
    }
}
