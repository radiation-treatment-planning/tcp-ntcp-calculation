﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TcpNtcpCalculation.Immutables
{
    public class ProbabilityOfCureCalculationResult
    {
        public IEnumerable<TcpPoint> TcpPoints { get; }
        public ProbabilityOfCure ProbabilityOfCure { get; }

        /// <summary>
        /// Probability of cure calculation result.
        /// </summary>
        /// <param name="tcpPoints"></param>
        /// <param name="probabilityOfCure"></param>
        public ProbabilityOfCureCalculationResult(IEnumerable<TcpPoint> tcpPoints, ProbabilityOfCure probabilityOfCure)
        {
            TcpPoints = tcpPoints ?? throw new ArgumentNullException(nameof(tcpPoints));
            ProbabilityOfCure = probabilityOfCure;
        }

        protected bool Equals(ProbabilityOfCureCalculationResult other)
        {
            if (!ProbabilityOfCure.Equals(other.ProbabilityOfCure)) return false;
            return TcpPoints.SequenceEqual(other.TcpPoints);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ProbabilityOfCureCalculationResult)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TcpPoints, ProbabilityOfCure);
        }
    }
}
