﻿using System;
using RadiationTreatmentPlanner.Utils;
using RadiationTreatmentPlanner.Utils.Dose;

namespace TcpNtcpCalculation.Immutables
{
    /// <summary>
    /// The biologically effective uniform dose. Equals operator allows a
    /// tolerance of 0.0001 to handle floating point inaccuracies.
    /// </summary>
    public readonly struct BiologicallyEffectiveUniformDose
    {
        public double Value { get; }
        public UDose.UDoseUnit Unit { get; }

        /// <summary>
        /// Biologically effective uniform dose.
        /// </summary>
        /// <param name="value">Dose value >= 0.</param>
        /// <param name="unit">Dose unit.</param>
        public BiologicallyEffectiveUniformDose(double value, UDose.UDoseUnit unit)
        {
            if (value < 0) throw new ArgumentOutOfRangeException(nameof(value));
            Value = value;
            Unit = unit;
        }

        public bool Equals(BiologicallyEffectiveUniformDose other)
        {
            return Math.Abs(Value - other.Value) < 0.0001
                && Unit == other.Unit;
        }

        public override bool Equals(object obj)
        {
            return obj is BiologicallyEffectiveUniformDose other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value, (int)Unit);
        }

        public override string ToString()
        {
            var formatProvider = UStringHelper.GetDoubleAndStringFormatProvider();
            return $"{Value.ToString(formatProvider)}{Unit}";
        }
    }
}
