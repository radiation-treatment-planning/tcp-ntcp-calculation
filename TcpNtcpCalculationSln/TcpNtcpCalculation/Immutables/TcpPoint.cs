﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public class TcpPoint
    {
        public string StructureId { get; }
        public TumorControlProbability TumorControlProbability { get; }

        /// <summary>
        /// Tumor Control Probability for a structure with a weight.
        /// </summary>
        /// <param name="structureId">Structure ID.</param>
        /// <param name="tumorControlProbability">Tumor Control Probability.</param>
        public TcpPoint(string structureId, TumorControlProbability tumorControlProbability)
        {
            StructureId = structureId ?? throw new ArgumentNullException(nameof(structureId));
            TumorControlProbability = tumorControlProbability;
        }

        protected bool Equals(TcpPoint other)
        {
            return StructureId == other.StructureId
                   && TumorControlProbability.Equals(other.TumorControlProbability);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpPoint)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StructureId, TumorControlProbability);
        }
    }
}
