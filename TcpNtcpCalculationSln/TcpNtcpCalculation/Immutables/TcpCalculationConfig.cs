﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public class TcpCalculationConfig
    {
        public ITumorControlProbabilityCalculator TcpCalculator { get; }
        public string StructureId { get; }
        public AlphaOverBeta AlphaOverBeta { get; }

        /// <summary>
        /// A configuration for weighted tumor control probability calculation.
        /// </summary>
        /// <param name="tcpCalculator">Tumor control probability calculator.</param>
        /// <param name="structureId">ID of according structure.</param>
        /// <param name="alphaOverBeta">Alpha / Beta ratio of LQ model.</param>
        public TcpCalculationConfig(ITumorControlProbabilityCalculator tcpCalculator,
            string structureId, AlphaOverBeta alphaOverBeta)
        {
            TcpCalculator = tcpCalculator ?? throw new ArgumentNullException(nameof(tcpCalculator));
            StructureId = structureId ?? throw new ArgumentNullException(nameof(structureId));
            AlphaOverBeta = alphaOverBeta;
        }

        protected bool Equals(TcpCalculationConfig other)
        {
            return Equals(TcpCalculator, other.TcpCalculator) && StructureId == other.StructureId &&
                   AlphaOverBeta.Equals(other.AlphaOverBeta);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpCalculationConfig)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TcpCalculator, StructureId, AlphaOverBeta);
        }
    }
}
