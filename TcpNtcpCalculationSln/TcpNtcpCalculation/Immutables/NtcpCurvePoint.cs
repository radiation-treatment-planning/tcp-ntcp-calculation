﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;

namespace TcpNtcpCalculation.Immutables
{
    public class NtcpCurvePoint
    {
        public NormalTissueComplicationProbability NormalTissueComplicationProbability { get; }
        public UDose Dose { get; }

        public NtcpCurvePoint(NormalTissueComplicationProbability normalTissueComplicationProbability, UDose dose)
        {
            NormalTissueComplicationProbability = normalTissueComplicationProbability;
            Dose = dose ?? throw new ArgumentNullException(nameof(dose));
        }

        protected bool Equals(NtcpCurvePoint other)
        {
            return NormalTissueComplicationProbability.Equals(other.NormalTissueComplicationProbability) &&
                   Equals(Dose, other.Dose);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((NtcpCurvePoint)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(NormalTissueComplicationProbability, Dose);
        }
    }
}
