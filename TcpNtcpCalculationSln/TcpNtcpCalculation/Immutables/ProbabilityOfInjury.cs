﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct ProbabilityOfInjury
    {
        public double Value { get; }

        /// <summary>
        /// Probability of injury. A.k.a.: total/overall normal tissue
        /// complication probability.
        /// </summary>
        /// <param name="value">The probability in range [0, 1].</param>
        public ProbabilityOfInjury(double value)
        {
            if (value < 0 || value > 1)
                throw new ArgumentOutOfRangeException($"{nameof(value)} " +
                                                      $"must be between 0 and 1 but was {value}.");
            Value = value;
        }

        public bool Equals(ProbabilityOfInjury other)
        {
            return Math.Abs(Value - other.Value) < 0.0001;
        }

        public override bool Equals(object obj)
        {
            return obj is ProbabilityOfInjury other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
