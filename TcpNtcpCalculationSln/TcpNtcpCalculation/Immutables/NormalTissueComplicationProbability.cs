﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct NormalTissueComplicationProbability
    {
        public double Value { get; }

        /// <summary>
        /// Normal tissue complication probability.
        /// </summary>
        /// <param name="value">Normal tissue complication probability value in range [0, 1].</param>
        public NormalTissueComplicationProbability(double value)
        {
            if (value < 0 || value > 1)
                throw new ArgumentException($"Normal tissue probability " +
                                            $"must be between 0 and 1 but was {value}.");

            Value = value;
        }

        public bool Equals(NormalTissueComplicationProbability other)
        {
            return Math.Abs(Value - other.Value) < 0.0001;
        }

        public override bool Equals(object obj)
        {
            return obj is NormalTissueComplicationProbability other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
