﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public struct VoxelResponse
    {
        public double Value { get; }

        /// <summary>
        /// Voxel response probability.
        /// </summary>
        /// <param name="value">Voxel response probability value in range [0, 1].</param>
        public VoxelResponse(double value)
        {
            if (value < 0 || value > 1)
                throw new ArgumentException(
                    $"Value of voxel response must be a probability between 0 and 1 but was {value}.");

            Value = value;
        }

        public bool Equals(VoxelResponse other)
        {
            return Math.Abs(Value - other.Value) < 0.0001;
        }

        public override bool Equals(object obj)
        {
            return obj is VoxelResponse other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
