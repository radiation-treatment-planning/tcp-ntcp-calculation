﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct Alpha
    {
        public double Value { get; }

        public Alpha(double value)
        {
            if (value <= 0) throw new ArgumentOutOfRangeException($"Alpha must be larger zero, but was {value}.");
            Value = value;
        }

        public bool Equals(Alpha other)
        {
            return Value.Equals(other.Value);
        }

        public override bool Equals(object obj)
        {
            return obj is Alpha other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
