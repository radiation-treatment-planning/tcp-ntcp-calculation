﻿using System;
using RadiationTreatmentPlanner.Utils.Volume;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct ClonogenicCellDensity
    {
        public double Value { get; }
        public UVolume.VolumeUnit VolumeUnit { get; }

        /// <summary>
        /// The tumor clonogenic cell density in cubic centimeters.
        /// </summary>
        /// <param name="value">Value larger or equal zero.</param>
        public ClonogenicCellDensity(double value)
        {
            if (value < 0) throw new ArgumentOutOfRangeException($"Cell density cannot be negative, but was {value}.");
            Value = value;
            VolumeUnit = UVolume.VolumeUnit.ccm;
        }

        public bool Equals(ClonogenicCellDensity other)
        {
            return Value.Equals(other.Value);
        }

        public override bool Equals(object obj)
        {
            return obj is ClonogenicCellDensity other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}