﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct AlphaOverBeta
    {
        public double Value { get; }
        public UDose.UDoseUnit DoseUnit { get; }

        /// <summary>
        /// Alpha/Beta. A parameter for voxel response calculation.
        /// </summary>
        /// <param name="value">Alpha/beta value >= 0.</param>
        /// <param name="doseUnit"></param>
        public AlphaOverBeta(double value, UDose.UDoseUnit doseUnit)
        {
            if (value <= 0)
                throw new ArgumentException($"Alpha over beta cannot be zero or negative, but was {value}.");
            Value = value;
            DoseUnit = doseUnit;
        }

        public bool Equals(AlphaOverBeta other)
        {
            return Math.Abs(Value - other.Value) < 0.0001 && DoseUnit == other.DoseUnit;
        }

        public override bool Equals(object obj)
        {
            return obj is AlphaOverBeta other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Value.GetHashCode() * 397) ^ (int)DoseUnit;
            }
        }
    }
}
