﻿using System;
using RadiationTreatmentPlanner.Utils.DVH;

namespace TcpNtcpCalculation.Immutables
{
    /// <summary>
    /// Structure ID and dose volume histogram tuple.
    /// </summary>
    public class StructureDvhTuple
    {
        public string StructureId { get; }
        public DVHCurve DvhCurve { get; }

        /// <summary>
        /// Structure ID and dose volume histogram tuple.
        /// </summary>
        /// <param name="structureId"></param>
        /// <param name="dvhCurve"></param>
        public StructureDvhTuple(string structureId, DVHCurve dvhCurve)
        {
            StructureId = structureId ?? throw new ArgumentNullException(nameof(structureId));
            DvhCurve = dvhCurve ?? throw new ArgumentNullException(nameof(dvhCurve));
        }

        protected bool Equals(StructureDvhTuple other)
        {
            return StructureId == other.StructureId && Equals(DvhCurve, other.DvhCurve);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((StructureDvhTuple)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StructureId, DvhCurve);
        }
    }
}