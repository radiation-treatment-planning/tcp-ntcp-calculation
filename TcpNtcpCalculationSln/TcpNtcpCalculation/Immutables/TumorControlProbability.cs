﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct TumorControlProbability
    {
        public double Value { get; }

        /// <summary>
        /// Tumor control probability.
        /// </summary>
        /// <param name="value">Tumor control probability value in range [0, 1].</param>
        public TumorControlProbability(double value)
        {
            if (value < 0 || value > 1)
                throw new ArgumentException($"Tumor control probability must be between 0 and 1 but was {value}.");

            Value = value;
        }

        public bool Equals(TumorControlProbability other)
        {
            return Math.Abs(Value - other.Value) < 0.0001;
        }

        public override bool Equals(object obj)
        {
            return obj is TumorControlProbability other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
