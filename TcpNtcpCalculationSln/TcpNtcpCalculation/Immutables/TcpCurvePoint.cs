﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;

namespace TcpNtcpCalculation.Immutables
{
    public class TcpCurvePoint
    {
        public TumorControlProbability TumorControlProbability { get; }
        public UDose Dose { get; }

        public TcpCurvePoint(TumorControlProbability tumorControlProbability, UDose physicalDose)
        {
            TumorControlProbability = tumorControlProbability;
            Dose = physicalDose ?? throw new ArgumentNullException(nameof(physicalDose));
        }

        protected bool Equals(TcpCurvePoint other)
        {
            return TumorControlProbability.Equals(other.TumorControlProbability) &&
                   Equals(Dose, other.Dose);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpCurvePoint)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TumorControlProbability, Dose);
        }
    }
}
