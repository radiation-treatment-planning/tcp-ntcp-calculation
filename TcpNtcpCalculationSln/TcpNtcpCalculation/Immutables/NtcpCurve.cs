﻿using System;
using System.Collections.Generic;
using System.Linq;
using RadiationTreatmentPlanner.Utils.Dose;

namespace TcpNtcpCalculation.Immutables
{
    public class NtcpCurve
    {
        private readonly List<NtcpCurvePoint> _curvePoints;

        public IEnumerable<NtcpCurvePoint> CurvePoints => _curvePoints;

        public NtcpCurve(IEnumerable<NtcpCurvePoint> curvePoints)
        {
            if (curvePoints == null) throw new ArgumentNullException(nameof(curvePoints));
            if (!curvePoints.Any())
                throw new ArgumentException($"At least one {nameof(TcpCurvePoint)} must be given.");
            _curvePoints = curvePoints.ToList();
        }

        public UDose GetDoseWithNtcp(NormalTissueComplicationProbability ntcp)
        {
            // TCP is lower minimum case.
            var firstPoint = _curvePoints.First();
            if (ntcp.Value <= firstPoint.NormalTissueComplicationProbability.Value)
                return firstPoint.Dose;

            // TCP is larger maximum case.
            var lastPoint = CurvePoints.Last();
            if (ntcp.Value >= lastPoint.NormalTissueComplicationProbability.Value)
                return lastPoint.Dose;


            for (var i = 0; i < _curvePoints.Count - 1; i++)
            {
                var point = _curvePoints[i];
                var nextPoint = _curvePoints[i + 1];

                // Exact match case.
                if (ntcp.Value.Equals(point.NormalTissueComplicationProbability.Value))
                    return point.Dose;

                // Lager than current interval case.
                if (ntcp.Value >= nextPoint.NormalTissueComplicationProbability.Value) continue;

                // In current interval case.
                var lowerDistance = Math.Abs(ntcp.Value - point.NormalTissueComplicationProbability.Value);
                var upperDistance = Math.Abs(ntcp.Value - nextPoint.NormalTissueComplicationProbability.Value);
                if (lowerDistance <= upperDistance) return point.Dose;
                return nextPoint.Dose;
            }

            return lastPoint.Dose;
        }

        protected bool Equals(NtcpCurve other)
        {
            return _curvePoints.SequenceEqual(other._curvePoints);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((NtcpCurve)obj);
        }

        public override int GetHashCode()
        {
            return (_curvePoints != null ? _curvePoints.GetHashCode() : 0);
        }
    }
}
