﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct FiftyPercentControlRate
    {
        public double Value { get; }
        public UDose.UDoseUnit DoseUnit { get; }

        /// <summary>
        /// Fifty percent control rate.
        /// </summary>
        /// <param name="value">Fifty percent control rate value >= 0.</param>
        /// <param name="doseUnit">Dose unit.</param>
        public FiftyPercentControlRate(double value, UDose.UDoseUnit doseUnit)
        {
            if (value < 0)
                throw new ArgumentException(
                    $"Fifty percent control rate cannot have value lower zero, but was {value}");
            Value = value;
            DoseUnit = doseUnit;
        }

        public bool Equals(FiftyPercentControlRate other)
        {
            return Math.Abs(Value - other.Value) < 0.0001 && DoseUnit == other.DoseUnit;
        }

        public override bool Equals(object obj)
        {
            return obj is FiftyPercentControlRate other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Value.GetHashCode() * 397) ^ (int)DoseUnit;
            }
        }
    }
}
