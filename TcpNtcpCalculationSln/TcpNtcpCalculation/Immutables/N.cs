﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct N
    {
        public double Value { get; }

        /// <summary>
        /// Parameter that correlates to the serial (n lower 1) or parallel (n~1)
        /// nature of a tissue in the Lyman-Kutcher-Burman NTCP model.
        /// </summary>
        /// <param name="value">Value larger zero.</param>
        public N(double value)
        {
            if (value <= 0) throw new ArgumentOutOfRangeException($"N cannot be lower or equal zero, but was {value}.");
            Value = value;
        }

        public bool Equals(N other)
        {
            return Value.Equals(other.Value);
        }

        public override bool Equals(object obj)
        {
            return obj is N other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
