﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct Slope
    {
        public double Value { get; }

        /// <summary>
        /// Slope.
        /// </summary>
        /// <param name="value">Slope value &gt; 0.</param>
        public Slope(double value)
        {
            if (value < 0) throw new ArgumentException($"The slope cannot be lower than zero, but was {value}");
            Value = value;
        }

        public bool Equals(Slope other)
        {
            return Math.Abs(Value - other.Value) < 0.0001;
        }

        public override bool Equals(object obj)
        {
            return obj is Slope other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
