﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct IsoeffectiveDoseIn0Gy
    {
        public double Value { get; }
        public UDose.UDoseUnit Unit { get; }

        /// <summary>
        /// Isoeffective dose in 0Gy.
        /// </summary>
        /// <param name="value">Isoeffective dose in 0Gy value > 0.</param>
        /// <param name="unit">Dose unit</param>
        public IsoeffectiveDoseIn0Gy(double value, UDose.UDoseUnit unit)
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(
                    $"Isoeffective dose in 0 Gy cannot be lower zero, but was {value}");
            Value = value;
            Unit = unit;
        }

        public bool Equals(IsoeffectiveDoseIn0Gy other)
        {
            return Value.Equals(other.Value) && Unit == other.Unit;
        }

        public override bool Equals(object obj)
        {
            return obj is IsoeffectiveDoseIn0Gy other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value, (int)Unit);
        }
    }
}
