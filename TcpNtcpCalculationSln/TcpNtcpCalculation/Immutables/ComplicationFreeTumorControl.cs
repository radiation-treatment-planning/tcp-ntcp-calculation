﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct ComplicationFreeTumorControl
    {
        public double Value { get; }

        public ComplicationFreeTumorControl(double value)
        {
            if (value < 0 || value > 1)
                throw new ArgumentException(
                    $"Complication free tumor control must be a value between 0 and 1 but was {value}.");
            Value = value;
        }

        public bool Equals(ComplicationFreeTumorControl other)
        {
            return Math.Abs(Value - other.Value) < 0.0001;
        }

        public override bool Equals(object obj)
        {
            return obj is ComplicationFreeTumorControl other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
