﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public class NtcpCalculationConfig
    {
        public INormalTissueComplicationProbabilityCalculator NtcpCalculator { get; }
        public string StructureId { get; }
        public AlphaOverBeta AlphaOverBeta { get; }
        public double Weight { get; }

        /// <summary>
        /// A configuration for weighted normal tissue complication probability calculation.
        /// </summary>
        /// <param name="ntcpCalculator">Normal tissue complication probability calculator.</param>
        /// <param name="structureId">ID of according structure.</param>
        /// <param name="weight">Weighting of this calculation in range [0, 1].</param>
        /// <param name="alphaOverBeta">Alpha / Beta ratio of LQ model.</param>
        public NtcpCalculationConfig(INormalTissueComplicationProbabilityCalculator ntcpCalculator,
            string structureId, double weight, AlphaOverBeta alphaOverBeta)
        {
            if (weight < 0 || weight > 1) throw new ArgumentOutOfRangeException(nameof(weight));
            NtcpCalculator = ntcpCalculator ??
                                                            throw new ArgumentNullException(
                                                                nameof(ntcpCalculator));
            StructureId = structureId ?? throw new ArgumentNullException(nameof(structureId));
            Weight = weight;
            AlphaOverBeta = alphaOverBeta;
        }

        protected bool Equals(NtcpCalculationConfig other)
        {
            return Equals(NtcpCalculator, other.NtcpCalculator) && StructureId == other.StructureId &&
                   AlphaOverBeta.Equals(other.AlphaOverBeta) && Weight.Equals(other.Weight);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((NtcpCalculationConfig)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(NtcpCalculator, StructureId, AlphaOverBeta, Weight);
        }
    }
}