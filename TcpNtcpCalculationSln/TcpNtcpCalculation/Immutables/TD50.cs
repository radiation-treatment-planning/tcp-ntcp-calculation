﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct TD50
    {
        public double Value { get; }
        public UDose.UDoseUnit Unit { get; }

        /// <summary>
        /// Dose in EQD2 that corresponds to the 50% probability of a complication
        /// if the organ were irradiated uniformly.
        /// </summary>
        /// <param name="value">Dose value larger or equal zero. Dose is require to be in EQD2 (2Gy fractions).</param>
        /// <param name="unit">Dose unit.</param>
        public TD50(double value, UDose.UDoseUnit unit)
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException($"TD50 cannot have value lower zero, but was {value}");
            Value = value;
            Unit = unit;
        }

        public bool Equals(TD50 other)
        {
            return Value.Equals(other.Value) && Unit == other.Unit;
        }

        public override bool Equals(object obj)
        {
            return obj is TD50 other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value, (int)Unit);
        }
    }
}
