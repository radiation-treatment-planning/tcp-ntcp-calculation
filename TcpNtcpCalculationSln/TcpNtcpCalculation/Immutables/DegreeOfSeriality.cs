﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct DegreeOfSeriality
    {
        public double Value { get; }

        /// <summary>
        /// Degree of seriality.
        /// </summary>
        /// <param name="value">Degree of seriality value > 0.</param>
        public DegreeOfSeriality(double value)
        {
            if (value <= 0)
                throw new ArgumentException($"Degree of seriality cannot be lower or equal zero, but was {value}.");
            Value = value;
        }

        public bool Equals(DegreeOfSeriality other)
        {
            return Math.Abs(Value - other.Value) < 0.0001;
        }

        public override bool Equals(object obj)
        {
            return obj is DegreeOfSeriality other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
