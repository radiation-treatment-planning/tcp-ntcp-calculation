﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    /// <summary>
    /// Normal tissue complication probability point.
    /// </summary>
    public class NtcpPoint
    {
        public string StructureId { get; }
        public NormalTissueComplicationProbability NormalTissueComplicationProbability { get; }
        public double Weight { get; }

        /// <summary>
        /// Normal tissue complication probability for a structure with a weight. 
        /// </summary>
        /// <param name="structureId">Structure ID.</param>
        /// <param name="normalTissueComplicationProbability">Normal tissue complication probability.</param>
        /// <param name="weight">Weighting of the point.</param>
        public NtcpPoint(string structureId, NormalTissueComplicationProbability normalTissueComplicationProbability,
            double weight)
        {
            StructureId = structureId ?? throw new ArgumentNullException(nameof(structureId));
            NormalTissueComplicationProbability = normalTissueComplicationProbability;
            Weight = weight;
        }

        protected bool Equals(NtcpPoint other)
        {
            return StructureId == other.StructureId &&
                   NormalTissueComplicationProbability.Equals(other.NormalTissueComplicationProbability) &&
                   Math.Abs(Weight - other.Weight) < 0.0001;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((NtcpPoint)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StructureId, NormalTissueComplicationProbability, Weight);
        }
    }
}