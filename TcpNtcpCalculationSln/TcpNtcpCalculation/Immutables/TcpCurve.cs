﻿using System;
using System.Collections.Generic;
using System.Linq;
using RadiationTreatmentPlanner.Utils.Dose;

namespace TcpNtcpCalculation.Immutables
{
    public class TcpCurve
    {
        private readonly List<TcpCurvePoint> _curvePoints;

        public IEnumerable<TcpCurvePoint> CurvePoints => _curvePoints;

        public TcpCurve(IEnumerable<TcpCurvePoint> curvePoints)
        {
            if (curvePoints == null) throw new ArgumentNullException(nameof(curvePoints));
            if (!curvePoints.Any())
                throw new ArgumentException($"At least one {nameof(TcpCurvePoint)} must be given.");
            _curvePoints = curvePoints.ToList();
        }

        public UDose GetDoseWithTcp(TumorControlProbability tcp)
        {
            // TCP is lower minimum case.
            var firstPoint = _curvePoints.First();
            if (tcp.Value <= firstPoint.TumorControlProbability.Value)
                return firstPoint.Dose;

            // TCP is larger maximum case.
            var lastPoint = CurvePoints.Last();
            if (tcp.Value >= lastPoint.TumorControlProbability.Value)
                return lastPoint.Dose;


            for (var i = 0; i < _curvePoints.Count - 1; i++)
            {
                var point = _curvePoints[i];
                var nextPoint = _curvePoints[i + 1];

                // Exact match case.
                if (tcp.Value.Equals(point.TumorControlProbability.Value))
                    return point.Dose;

                // Lager than current interval case.
                if (tcp.Value >= nextPoint.TumorControlProbability.Value) continue;

                // In current interval case.
                var lowerDistance = Math.Abs(tcp.Value - point.TumorControlProbability.Value);
                var upperDistance = Math.Abs(tcp.Value - nextPoint.TumorControlProbability.Value);
                if (lowerDistance <= upperDistance) return point.Dose;
                return nextPoint.Dose;
            }

            return lastPoint.Dose;
        }

        protected bool Equals(TcpCurve other)
        {
            return _curvePoints.SequenceEqual(other._curvePoints);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpCurve)obj);
        }

        public override int GetHashCode()
        {
            return (_curvePoints != null ? _curvePoints.GetHashCode() : 0);
        }
    }
}
