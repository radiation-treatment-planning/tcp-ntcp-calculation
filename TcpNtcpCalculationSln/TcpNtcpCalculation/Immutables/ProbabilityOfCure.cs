﻿using System;

namespace TcpNtcpCalculation.Immutables
{
    public readonly struct ProbabilityOfCure
    {
        public double Value { get; }

        /// <summary>
        /// Probability of cure. A.k.a: total/overall tumor control 
        /// probability.
        /// </summary>
        /// <param name="value">The probability in range [0, 1].</param>
        public ProbabilityOfCure(double value)
        {
            if (value < 0 || value > 1)
                throw new ArgumentOutOfRangeException($"{nameof(value)} " +
                                                      $"must be between 0 and 1 but was {value}.");
            Value = value;
        }

        public bool Equals(ProbabilityOfCure other)
        {
            return Math.Abs(Value - other.Value) < 0.0001;
        }

        public override bool Equals(object obj)
        {
            return obj is ProbabilityOfCure other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
