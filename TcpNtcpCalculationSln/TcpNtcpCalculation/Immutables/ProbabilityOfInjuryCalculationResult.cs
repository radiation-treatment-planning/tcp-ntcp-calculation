﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TcpNtcpCalculation.Immutables
{
    /// <summary>
    /// Total normal tissue complication probability calculation result.
    /// </summary>
    public class ProbabilityOfInjuryCalculationResult
    {
        public IEnumerable<NtcpPoint> NtcpPoints { get; }
        public ProbabilityOfInjury ProbabilityOfInjury { get; }

        /// <summary>
        /// Probability of injury calculation result.
        /// </summary>
        /// <param name="ntcpPoints">Normal tissue complication probability points.</param>
        /// <param name="probabilityOfInjury">Probability of injury.</param>
        public ProbabilityOfInjuryCalculationResult(IEnumerable<NtcpPoint> ntcpPoints,
            ProbabilityOfInjury probabilityOfInjury)
        {
            NtcpPoints = ntcpPoints ?? throw new ArgumentNullException(nameof(ntcpPoints));
            ProbabilityOfInjury = probabilityOfInjury;
        }

        protected bool Equals(ProbabilityOfInjuryCalculationResult other)
        {
            if (!ProbabilityOfInjury.Equals(other.ProbabilityOfInjury)) return false;
            return NtcpPoints.SequenceEqual(other.NtcpPoints);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ProbabilityOfInjuryCalculationResult)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(NtcpPoints, ProbabilityOfInjury);
        }
    }
}