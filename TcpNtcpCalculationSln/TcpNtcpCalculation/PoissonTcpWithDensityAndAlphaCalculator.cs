﻿using System;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    public class PoissonTcpWithDensityAndAlphaCalculator : ITumorControlProbabilityCalculator
    {
        private TcpNtcpParametersDictionary _paramsDict;
        public string Description { get; } = "Poisson TCP with cell density and alpha calculator";
        public ClonogenicCellDensity Density { get; }
        public Alpha Alpha { get; }
        public IsoeffectiveDoseIn2GyConverter IsoeffectiveDoseIn2GyConverter { get; }

        public PoissonTcpWithDensityAndAlphaCalculator(ClonogenicCellDensity density, Alpha alpha,
            IsoeffectiveDoseIn2GyConverter isoeffectiveDoseIn2GyConverter)
        {
            Density = density;
            Alpha = alpha;
            IsoeffectiveDoseIn2GyConverter = isoeffectiveDoseIn2GyConverter;
        }

        /// <summary>
        /// Calculate tumor control probability with cell density and Alpha Poisson model .
        /// </summary>
        /// <param name="dvhCurve">Dose-Volume histogram curve. Cubic-centimeters is expected
        /// to be the volume unit.</param>
        /// <param name="numberOfFractions">Number of fractions.</param>
        /// <returns>Tumor control probability.</returns>
        public TumorControlProbability Calculate(DVHCurve dvhCurve, uint numberOfFractions)
        {
            if (dvhCurve == null) throw new ArgumentNullException(nameof(dvhCurve));
            if (numberOfFractions == 0)
                throw new ArgumentOutOfRangeException($"{nameof(numberOfFractions)} cannot be zero.");
            if (dvhCurve.GetVolumeUnit() != Density.VolumeUnit)
                throw new ArgumentException(
                    $"Volume units of DVH Curve and {nameof(Density)} must match, " +
                    $"but were {dvhCurve.GetVolumeUnit()} and {Density.VolumeUnit}.");

            var differentialDvhCurve = dvhCurve.ToDifferential();
            var eqd0DvhCurve = differentialDvhCurve.ToEqd0(IsoeffectiveDoseIn2GyConverter.AlphaOverBeta.Value, numberOfFractions);
            var product = 1.0;
            foreach (var curvePoint in eqd0DvhCurve.CurvePoints)
            {
                var eqd0 = curvePoint.Item1;
                var volume = curvePoint.Item2;
                var secondExponent = Math.Exp(-Alpha.Value * eqd0.Value);
                var voxelResponse = Math.Exp(-Density.Value * volume.Value * secondExponent);
                product *= voxelResponse;
            }

            return new TumorControlProbability(product);
        }

        public TcpNtcpParametersDictionary GetParametersDictionary()
        {
            if (_paramsDict != null) return _paramsDict;

            var builder = TcpNtcpParametersDictionaryBuilder.Initialize();
            foreach (var paramSet in IsoeffectiveDoseIn2GyConverter.GetParametersDictionary().GetAllParameterSets())
                builder.WithParameterSet(paramSet.ParameterId, paramSet.ValueUnitTuple.Value, paramSet.ValueUnitTuple.Unit);
            _paramsDict = builder
                .WithParameterSet("Density", Density.Value, Option.Some($"/{Density.VolumeUnit}"))
                .WithParameterSet("Alpha", Alpha.Value, Option.None<string>())
                .Build();
            return _paramsDict;
        }

        private IsoeffectiveDoseIn2Gy ConvertPossiblyPhysicalDoseToIsoeffectiveDoseIn2Gy(DVHCurve differentialDvhCurve,
            UDose possiblyPhysicalDose, uint numberOfFractions)
        {
            return differentialDvhCurve.TypeOfDose == DVHCurve.DoseType.PHYSICAL
                ? IsoeffectiveDoseIn2GyConverter.Convert(possiblyPhysicalDose, numberOfFractions)
                : new IsoeffectiveDoseIn2Gy(possiblyPhysicalDose.Value, possiblyPhysicalDose.Unit);
        }

        protected bool Equals(PoissonTcpWithDensityAndAlphaCalculator other)
        {
            return Description == other.Description && Density.Equals(other.Density) && Alpha.Equals(other.Alpha) &&
                   Equals(IsoeffectiveDoseIn2GyConverter, other.IsoeffectiveDoseIn2GyConverter);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PoissonTcpWithDensityAndAlphaCalculator)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Description, Density, Alpha, IsoeffectiveDoseIn2GyConverter);
        }
    }
}
