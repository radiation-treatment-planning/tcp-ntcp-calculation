﻿using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation
{
    public interface IComplicationFreeTumorControlCalculator
    {
        /// <summary>
        /// Calculate the complication free tumor control from a TCP and NTCP result.
        /// </summary>
        /// <param name="tcpNtcpResult">TCP and NTCP result.</param>
        /// <returns>ComplicationFreeTumorControl</returns>
        ComplicationFreeTumorControl Calculate(TcpNtcpResult tcpNtcpResult);

        /// <summary>
        /// Calculate the complication free tumor control from a probability of cure and
        /// a probability of injury.
        /// </summary>
        /// <param name="probabilityOfCureCalculationResult">Probability of cure calculation result.</param>
        /// <param name="probabilityOfInjuryCalculationResult">Probability of injury calculation result.</param>
        /// <returns>ComplicationFreeTumorControl</returns>
        ComplicationFreeTumorControl Calculate(ProbabilityOfCureCalculationResult probabilityOfCureCalculationResult,
            ProbabilityOfInjuryCalculationResult probabilityOfInjuryCalculationResult);
    }
}
