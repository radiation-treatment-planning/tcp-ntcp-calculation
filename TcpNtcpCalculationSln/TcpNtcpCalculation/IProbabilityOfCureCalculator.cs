﻿using System.Collections.Generic;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation
{
    public interface IProbabilityOfCureCalculator
    {
        /// <summary>
        /// TCP Calculation configurations.
        /// </summary>
        IEnumerable<TcpCalculationConfig> TcpCalculationConfigs { get; }

        /// <summary>
        /// Calculate the probability of cure.
        /// </summary>
        /// <param name="structureDvhTuples">Structure ID and Dose-Volume-Histogram tuples.</param>
        /// <param name="numberOfFractions">Number of fractions (>0).</param>
        /// <returns>ProbabilityOfCureCalculationResult</returns>
        ProbabilityOfCureCalculationResult Calculate(IEnumerable<StructureDvhTuple> structureDvhTuples,
            uint numberOfFractions);
    }
}
