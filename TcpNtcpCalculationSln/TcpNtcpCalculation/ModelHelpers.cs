﻿using System;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation
{
    static class ModelHelpers
    {
        /// <summary>
        /// Calculate Gaussian error function.
        /// </summary>
        /// <param name="x">Border.</param>
        /// <returns>The probability that a normally distributed random variable
        /// is in range [-x, x].</returns>
        public static double Erf(double x)
        {
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x);

            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return sign * y;
        }

        public static IsoeffectiveDoseIn0Gy Convert(IsoeffectiveDoseIn2Gy eqd2, AlphaOverBeta alphaOverBeta)
        {
            if (eqd2.Unit != alphaOverBeta.DoseUnit)
                throw new ArgumentException(
                    $"Expected equal dose units for {nameof(eqd2)} " +
                    $"and {nameof(alphaOverBeta)}, but received {eqd2.Unit} and {alphaOverBeta.DoseUnit}");
            var doseValue = ((alphaOverBeta.Value + 2.0) * eqd2.Value) / alphaOverBeta.Value;
            return new IsoeffectiveDoseIn0Gy(doseValue, eqd2.Unit);
        }
    }
}
