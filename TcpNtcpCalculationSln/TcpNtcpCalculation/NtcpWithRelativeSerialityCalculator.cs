﻿using System;
using System.Linq;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    public class NtcpWithRelativeSerialityCalculator : INormalTissueComplicationProbabilityCalculator
    {
        private TcpNtcpParametersDictionary _paramsDict;
        public IIsoeffectiveDoseIn2GyConverter IsoeffectiveDoseIn2GyConverter { get; }
        public IVoxelResponseCalculationStrategy VoxelResponseCalculator { get; }
        public string Description { get; } = "Relative Seriality Normal Tissue Complication Probability Calculator.";
        public DegreeOfSeriality DegreeOfSeriality { get; }

        /// <summary>
        /// Normal tissue complication probability with Relative Seriality calculator.
        /// </summary>
        /// <param name="isoeffectiveDoseIn2GyConverter">Isoeffective dose in 2Gy/fraction converter.</param>
        /// <param name="voxelResponseCalculator">Voxel response calculator.</param>
        /// <param name="degreeOfSeriality">Degree of seriality.</param>
        public NtcpWithRelativeSerialityCalculator(IIsoeffectiveDoseIn2GyConverter isoeffectiveDoseIn2GyConverter,
            IVoxelResponseCalculationStrategy voxelResponseCalculator, DegreeOfSeriality degreeOfSeriality)
        {
            IsoeffectiveDoseIn2GyConverter = isoeffectiveDoseIn2GyConverter ??
                                             throw new ArgumentNullException(nameof(isoeffectiveDoseIn2GyConverter));
            VoxelResponseCalculator = voxelResponseCalculator ??
                                      throw new ArgumentNullException(nameof(voxelResponseCalculator));
            DegreeOfSeriality = degreeOfSeriality;
        }

        /// <summary>
        /// Calculates the Normal Tissue Complication Value for an inhomogeneous irradiation 
        /// of an organ with the relative seriality model from a List of differential dose volume 
        /// tuples.
        /// DOI: https://doi.org/10.1186/1748-717X-3-3
        /// </summary>
        /// <param name="dvhCurve">A list of UDose, UVolume tuples. UDoses must be isoeffective doses in 2 Gy fractions. 
        /// An ascending sorting of the UDose values is assumed.</param>
        /// <param name="numberOfFractions">Number of fractions.</param>
        /// <returns>The normal tissue complication probability.</returns>
        public NormalTissueComplicationProbability Calculate(DVHCurve dvhCurve, uint numberOfFractions)
        {
            if (dvhCurve.TypeOfDose == DVHCurve.DoseType.UNDEFINED)
                throw new ArgumentException(
                    "Cannot calculate normal tissue complication probability from DVHCure of undefined dose type.");
            var differentialDvhCurve = dvhCurve.ToDifferential();

            // Convert total volume.
            var dvhPoints = differentialDvhCurve.GetPoints().ToArray();
            var totalVolume = dvhPoints.Sum(tuple => tuple.Item2.Value);

            // Convert total response.
            double product = 1;
            foreach (var tuple in dvhPoints)
            {
                // Convert voxel response.
                var possiblyPhysicalDose = tuple.Item1;
                var isoeffectiveDoseIn2Gy = ConvertPossiblyPhysicalDoseToIsoeffectiveDoseIn2Gy(differentialDvhCurve,
                    possiblyPhysicalDose,
                    numberOfFractions);
                var voxelResponse = VoxelResponseCalculator.Calculate(isoeffectiveDoseIn2Gy).Value;

                // Convert fractional volume.
                var fractionalVolume = tuple.Item2.Value / totalVolume;

                product *= Math.Pow(1 - Math.Pow(voxelResponse, DegreeOfSeriality.Value), fractionalVolume);
            }

            var totalResponse = Math.Pow(1 - product, 1 / DegreeOfSeriality.Value);
            return new NormalTissueComplicationProbability(totalResponse);
        }

        public TcpNtcpParametersDictionary GetParametersDictionary()
        {
            if (_paramsDict != null) return _paramsDict;
            var builder = TcpNtcpParametersDictionaryBuilder.Initialize();
            foreach (var paramSet in IsoeffectiveDoseIn2GyConverter.GetParametersDictionary().GetAllParameterSets())
                builder.WithParameterSet(paramSet.ParameterId, paramSet.ValueUnitTuple.Value,
                    paramSet.ValueUnitTuple.Unit);

            foreach (var paramSet in VoxelResponseCalculator.GetParametersDictionary().GetAllParameterSets())
                builder.WithParameterSet(paramSet.ParameterId, paramSet.ValueUnitTuple.Value,
                    paramSet.ValueUnitTuple.Unit);

            builder.WithParameterSet("Seriality", DegreeOfSeriality.Value, Option.None<string>());
            _paramsDict = builder.Build();
            return _paramsDict;
        }

        private IsoeffectiveDoseIn2Gy ConvertPossiblyPhysicalDoseToIsoeffectiveDoseIn2Gy(DVHCurve differentialDvhCurve, UDose possiblyPhysicalDose, uint numberOfFractions)
        {
            return differentialDvhCurve.TypeOfDose == DVHCurve.DoseType.PHYSICAL
                    ? IsoeffectiveDoseIn2GyConverter.Convert(possiblyPhysicalDose, numberOfFractions)
                    : new IsoeffectiveDoseIn2Gy(possiblyPhysicalDose.Value, possiblyPhysicalDose.Unit);
        }

        protected bool Equals(NtcpWithRelativeSerialityCalculator other)
        {
            return Equals(IsoeffectiveDoseIn2GyConverter, other.IsoeffectiveDoseIn2GyConverter) &&
                   Equals(VoxelResponseCalculator, other.VoxelResponseCalculator) &&
                   DegreeOfSeriality.Equals(other.DegreeOfSeriality);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((NtcpWithRelativeSerialityCalculator)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(IsoeffectiveDoseIn2GyConverter, VoxelResponseCalculator, DegreeOfSeriality);
        }

        public static bool operator ==(NtcpWithRelativeSerialityCalculator left, NtcpWithRelativeSerialityCalculator right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(NtcpWithRelativeSerialityCalculator left, NtcpWithRelativeSerialityCalculator right)
        {
            return !Equals(left, right);
        }
    }
}
