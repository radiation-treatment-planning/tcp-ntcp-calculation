﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    public class TcpWithNormalizedVolumeCorrectionCalculator : ITumorControlProbabilityCalculator
    {
        private TcpNtcpParametersDictionary _paramsDict;
        public IIsoeffectiveDoseIn2GyConverter IsoeffectiveDoseIn2GyConverter { get; }
        public IVoxelResponseCalculationStrategy VoxelResponseCalculator { get; }

        public TcpWithNormalizedVolumeCorrectionCalculator(
            IIsoeffectiveDoseIn2GyConverter isoeffectiveDoseIn2GyConverter,
            IVoxelResponseCalculationStrategy voxelResponseCalculator)
        {
            IsoeffectiveDoseIn2GyConverter = isoeffectiveDoseIn2GyConverter;
            VoxelResponseCalculator = voxelResponseCalculator;
        }

        public string Description { get; } = "Calculates tumor control probability with normalized volume correction.";

        /// <summary>
        /// Calculates the total Tumor Control Probability for non-uniform dose distributions from a DVHCurve
        /// with normalized volume correction.
        ///  TODO: add reference.
        /// </summary>
        /// <param name="dvhCurve">A list of UDose, UVolume tuples.
        ///     An ascending sorting of the dose values is assumed. The UDoses must be the total
        ///     physical dose.</param>
        /// <param name="numberOfFractions">Number of fractions.</param>
        /// <returns>The tumor control probability, also called total response.</returns>
        public TumorControlProbability Calculate(DVHCurve dvhCurve, uint numberOfFractions)
        {
            if (dvhCurve.TypeOfDose == DVHCurve.DoseType.UNDEFINED)
                throw new ArgumentException(
                    "Cannot calculate tumor control probability from DVHCure of undefined dose type.");

            var differentialDvhCurve = dvhCurve.ToDifferential();
            var totalVolume = differentialDvhCurve.TotalVolume();

            // Convert total response.
            double totalResponse = 1;
            foreach (var doseVolumeTuple in differentialDvhCurve.GetPoints())
            {
                var possiblyPhysicalDose = doseVolumeTuple.Item1;
                var volume = doseVolumeTuple.Item2;

                var isoeffectiveDoseIn2Gy = ConvertPossiblyPhysicalDoseToIsoeffectiveDoseIn2Gy(
                    differentialDvhCurve, possiblyPhysicalDose, numberOfFractions);

                var voxelResponse = VoxelResponseCalculator.Calculate(isoeffectiveDoseIn2Gy);

                // Convert correction with normalized volume.
                var fractionalVolume = volume.Value / totalVolume.Value;
                var correctedVoxelResponse = Math.Pow(voxelResponse.Value, fractionalVolume);
                totalResponse *= correctedVoxelResponse;
            }

            return new TumorControlProbability(totalResponse);
        }

        public TcpNtcpParametersDictionary GetParametersDictionary()
        {
            if (_paramsDict != null) return _paramsDict;
            var builder = TcpNtcpParametersDictionaryBuilder.Initialize();
            foreach (var paramSet in IsoeffectiveDoseIn2GyConverter.GetParametersDictionary().GetAllParameterSets())
                builder.WithParameterSet(paramSet.ParameterId, paramSet.ValueUnitTuple.Value,
                    paramSet.ValueUnitTuple.Unit);

            foreach (var paramSet in VoxelResponseCalculator.GetParametersDictionary().GetAllParameterSets())
                builder.WithParameterSet(paramSet.ParameterId, paramSet.ValueUnitTuple.Value,
                    paramSet.ValueUnitTuple.Unit);

            _paramsDict = builder.Build();
            return _paramsDict;
        }

        private IsoeffectiveDoseIn2Gy ConvertPossiblyPhysicalDoseToIsoeffectiveDoseIn2Gy(DVHCurve differentialDvhCurve,
            UDose possiblyPhysicalDose, uint numberOfFractions)
        {
            return differentialDvhCurve.TypeOfDose == DVHCurve.DoseType.PHYSICAL
                ? IsoeffectiveDoseIn2GyConverter.Convert(possiblyPhysicalDose, numberOfFractions)
                : new IsoeffectiveDoseIn2Gy(possiblyPhysicalDose.Value, possiblyPhysicalDose.Unit);
        }

        protected bool Equals(TcpWithNormalizedVolumeCorrectionCalculator other)
        {
            return Equals(IsoeffectiveDoseIn2GyConverter, other.IsoeffectiveDoseIn2GyConverter) &&
                   Equals(VoxelResponseCalculator, other.VoxelResponseCalculator);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpWithNormalizedVolumeCorrectionCalculator)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(IsoeffectiveDoseIn2GyConverter, VoxelResponseCalculator);
        }

        public static bool operator ==(TcpWithNormalizedVolumeCorrectionCalculator left, TcpWithNormalizedVolumeCorrectionCalculator right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TcpWithNormalizedVolumeCorrectionCalculator left, TcpWithNormalizedVolumeCorrectionCalculator right)
        {
            return !Equals(left, right);
        }
    }
}
