﻿using RadiationTreatmentPlanner.Utils.Dose;
using System;
using Optional;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    public class IsoeffectiveDoseIn2GyConverter : IIsoeffectiveDoseIn2GyConverter
    {
        private TcpNtcpParametersDictionary _parametersDict;

        /// <summary>
        /// Convert physical doses to EQD2 doses with Linear-Quadratic model.
        /// </summary>
        /// <param name="alphaOverBeta">Alpha/Beta</param>
        public IsoeffectiveDoseIn2GyConverter(AlphaOverBeta alphaOverBeta)
        {
            AlphaOverBeta = alphaOverBeta;
        }

        public AlphaOverBeta AlphaOverBeta { get; }

        /// <summary>
        /// Converts physical dose to Isoeffective dose in 2Gy.
        /// </summary>
        /// <param name="physicalDose">The physical dose.</param>
        /// <param name="numberOfFractions">Number of fractions, a treatment plan is split into.</param>
        /// <returns></returns>
        public IsoeffectiveDoseIn2Gy Convert(UDose physicalDose, uint numberOfFractions)
        {
            if (numberOfFractions == 0)
                throw new ArgumentException("Number of fractions cannot be zero.");
            var dose = physicalDose.Value;
            var dividend = dose * (1 + dose / numberOfFractions / AlphaOverBeta.Value);
            var divisor = 1 + 2 / AlphaOverBeta.Value;
            return new IsoeffectiveDoseIn2Gy(dividend / divisor, physicalDose.Unit);
        }

        public TcpNtcpParametersDictionary GetParametersDictionary()
        {
            if (_parametersDict != null) return _parametersDict;
            _parametersDict = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("Alpha/Beta", AlphaOverBeta.Value,
                    Option.Some(AlphaOverBeta.DoseUnit.ToString()))
                .Build();
            return _parametersDict;
        }

        protected bool Equals(IsoeffectiveDoseIn2GyConverter other)
        {
            return AlphaOverBeta.Equals(other.AlphaOverBeta);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((IsoeffectiveDoseIn2GyConverter)obj);
        }

        public override int GetHashCode()
        {
            return AlphaOverBeta.GetHashCode();
        }

        public static bool operator ==(IsoeffectiveDoseIn2GyConverter left, IsoeffectiveDoseIn2GyConverter right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(IsoeffectiveDoseIn2GyConverter left, IsoeffectiveDoseIn2GyConverter right)
        {
            return !Equals(left, right);
        }
    }
}
