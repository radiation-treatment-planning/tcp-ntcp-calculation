﻿using System;
using Optional;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    public class LogisticVoxelResponseCalculator : IVoxelResponseCalculationStrategy
    {
        private TcpNtcpParametersDictionary _parametersDict;
        public FiftyPercentControlRate D50 { get; }
        public Slope Gamma { get; }
        public double Epsilon { get; }

        /// <summary>
        /// The Logistic model for voxel response calculation.
        /// </summary>
        /// <param name="d50">Fifty percent control rate in Gy.</param>
        /// <param name="gamma">The gamma of the response curve.</param>
        /// <param name="epsilon">A very small number to avoid division by zero.</param>
        public LogisticVoxelResponseCalculator(FiftyPercentControlRate d50, Slope gamma, double epsilon = 1E-08)
        {
            D50 = d50;
            Gamma = gamma;
            Epsilon = epsilon;
        }

        /// <summary>
        /// Calculates the voxel response with the Logistic Model.
        /// DOI: 10.1016/j.ijrobp.2020.11.017 
        /// </summary>
        /// <param name="isoeffectiveDoseIn2Gy">The isoeffective isoeffectiveDoseIn2Gy in 2 Gy.</param>
        /// <returns>Voxel response.</returns>
        public VoxelResponse Calculate(IsoeffectiveDoseIn2Gy isoeffectiveDoseIn2Gy)
        {
            if (isoeffectiveDoseIn2Gy.Unit != D50.DoseUnit)
                throw new ArgumentException(
                    $"Dose units of D50 and isoeffectiveDoseIn2Gy must be equal, but were {D50.DoseUnit} and {isoeffectiveDoseIn2Gy.Unit}");

            return new VoxelResponse(1 / (1 + Math.Pow(D50.Value / (isoeffectiveDoseIn2Gy.Value + Epsilon),
                4.0 * Gamma.Value)));
        }

        public TcpNtcpParametersDictionary GetParametersDictionary()
        {
            if (_parametersDict != null) return _parametersDict;
            _parametersDict = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("D50", D50.Value, Option.Some(D50.DoseUnit.ToString()))
                .WithParameterSet("Gamma", Gamma.Value, Option.None<string>())
                .Build();
            return _parametersDict;
        }

        protected bool Equals(LogisticVoxelResponseCalculator other)
        {
            return D50.Equals(other.D50) && Gamma.Equals(other.Gamma) && Epsilon.Equals(other.Epsilon);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((LogisticVoxelResponseCalculator)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(D50, Gamma, Epsilon);
        }

        public static bool operator ==(LogisticVoxelResponseCalculator left, LogisticVoxelResponseCalculator right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(LogisticVoxelResponseCalculator left, LogisticVoxelResponseCalculator right)
        {
            return !Equals(left, right);
        }
    }
}
