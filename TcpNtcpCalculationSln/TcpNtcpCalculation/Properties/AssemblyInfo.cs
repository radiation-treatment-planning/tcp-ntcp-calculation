﻿using System.Reflection;
using System.Runtime.InteropServices;

// Allgemeine Informationen über eine Assembly werden über die folgenden
// Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
// die einer Assembly zugeordnet sind.
[assembly: AssemblyTitle("TcpNtcpCalculation")]
[assembly: AssemblyDescription("A .Net Framework 4.7.2 library for tumor control probability, normal tissue complication probability and complication free tumor control calculation. For examples how to use see the TcpNtcpCalculation.Tests library at https://gitlab.com/radiation-treatment-planning/tcp-ntcp-calculation/")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Dejan Kuhn (form. Kostyszyn), Medical Center University of Freiburg, Germany")]
[assembly: AssemblyProduct("TcpNtcpCalculation")]
[assembly: AssemblyCopyright("Copyright © Dejan Kuhn (form. Kostyszyn) 2024")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Durch Festlegen von ComVisible auf FALSE werden die Typen in dieser Assembly
// für COM-Komponenten unsichtbar.  Wenn Sie auf einen Typ in dieser Assembly von
// COM aus zugreifen müssen, sollten Sie das ComVisible-Attribut für diesen Typ auf "True" festlegen.
[assembly: ComVisible(false)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
[assembly: Guid("2dd1a07f-be5a-42fb-9559-7ca74152d165")]

// Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
//
//      Hauptversion
//      Nebenversion
//      Buildnummer
//      Revision
//
// Sie können alle Werte angeben oder Standardwerte für die Build- und Revisionsnummern verwenden,
// indem Sie "*" wie unten gezeigt eingeben:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyInformationalVersion("8.0.0")]
[assembly: AssemblyVersion("8.0.0")]
[assembly: AssemblyFileVersion("8.0.0")]
