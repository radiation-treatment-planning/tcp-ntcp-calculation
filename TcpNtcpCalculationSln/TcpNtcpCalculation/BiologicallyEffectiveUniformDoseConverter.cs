﻿using RadiationTreatmentPlanner.Utils;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation
{
    public static class BiologicallyEffectiveUniformDoseConverter
    {
        public static BiologicallyEffectiveUniformDose ToBiologicallyEffectiveUniformDose(this string doseAsString)
        {
            var doseUnit = UDose.UDoseUnit.Unknown;
            var doseUnitExtractionResult = DoseUnitExtractor.Extract(doseAsString);
            if (doseUnitExtractionResult.Success)
            {
                doseUnit = doseUnitExtractionResult.DoseUnit;
                doseAsString = doseAsString.Replace(doseUnit.ToDescription(), "");
            }

            var doseValue = UStringHelper.ConvertStringToDoubleOrDefault(doseAsString);
            return new BiologicallyEffectiveUniformDose(doseValue, doseUnit);

        }
    }
}
