﻿using System;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    public class LymanKutcherBurmanNtcpCalculator : INormalTissueComplicationProbabilityCalculator
    {
        private TcpNtcpParametersDictionary _paramsDict;
        public TD50 Td50 { get; }
        public Slope M { get; }
        public N N { get; }
        public IIsoeffectiveDoseIn2GyConverter IsoeffectiveDoseIn2GyConverter { get; }
        public string Description { get; } = "Lyman-Kutcher-Burman NTCP Calculator.";

        /// <summary>
        /// Lyman-Kutcher-Burman model for normal tissue complication probability calculation.
        /// </summary>
        /// <param name="td50"></param>
        /// <param name="m"></param>
        /// <param name="n"></param>
        /// <param name="isoeffectiveDoseIn2GyConverter">Physical dose to EQD2 converter.</param>
        public LymanKutcherBurmanNtcpCalculator(TD50 td50, Slope m, N n,
            IIsoeffectiveDoseIn2GyConverter isoeffectiveDoseIn2GyConverter)
        {
            Td50 = td50;
            M = m;
            N = n;
            IsoeffectiveDoseIn2GyConverter = isoeffectiveDoseIn2GyConverter ??
                                             throw new ArgumentNullException(nameof(isoeffectiveDoseIn2GyConverter));
        }

        public NormalTissueComplicationProbability Calculate(DVHCurve dvhCurve, uint numberOfFractions)
        {
            if (dvhCurve == null) throw new ArgumentNullException(nameof(dvhCurve));
            if (numberOfFractions == 0)
                throw new ArgumentOutOfRangeException($"{nameof(numberOfFractions)} cannot be 0.");
            var eud = CalculateEud(dvhCurve, numberOfFractions);
            var t = CalculateT(eud);
            var x = t * (1.0 / Math.Sqrt(2));
            double ntcp = 0.5 * (1.0 + ModelHelpers.Erf(x));
            return new NormalTissueComplicationProbability(ntcp);
        }

        public TcpNtcpParametersDictionary GetParametersDictionary()
        {
            if (_paramsDict != null) return _paramsDict;

            var builder = TcpNtcpParametersDictionaryBuilder.Initialize();
            foreach (var paramSet in IsoeffectiveDoseIn2GyConverter.GetParametersDictionary().GetAllParameterSets())
                builder.WithParameterSet(paramSet.ParameterId, paramSet.ValueUnitTuple.Value, paramSet.ValueUnitTuple.Unit);
            _paramsDict = builder
                .WithParameterSet("TD50", Td50.Value, Option.Some(Td50.Unit.ToString()))
                .WithParameterSet("M", M.Value, Option.None<string>())
                .WithParameterSet("N", N.Value, Option.None<string>())
                .Build();
            return _paramsDict;
        }

        private double CalculateT(double eud)
        {
            return (eud - Td50.Value) / (M.Value * Td50.Value);
        }

        private double CalculateEud(DVHCurve dvhCurve, uint numberOfFractions)
        {
            var differentialDvh = dvhCurve.ToDifferential();
            var totalVolume = differentialDvh.TotalVolume();
            var sum = 0.0;
            foreach (var curvePoint in differentialDvh.CurvePoints)
            {
                var dose = curvePoint.Item1;
                var eqd2 = ConvertPossiblyPhysicalDoseToIsoeffectiveDoseIn2Gy(
                    differentialDvh, dose, numberOfFractions);
                var volume = curvePoint.Item2;
                sum += Math.Pow(eqd2.Value, 1.0 / N.Value) * (volume.Value / totalVolume.Value);
            }

            return Math.Pow(sum, N.Value);
        }

        private IsoeffectiveDoseIn2Gy ConvertPossiblyPhysicalDoseToIsoeffectiveDoseIn2Gy(DVHCurve differentialDvhCurve,
            UDose possiblyPhysicalDose, uint numberOfFractions)
        {
            return differentialDvhCurve.TypeOfDose == DVHCurve.DoseType.PHYSICAL
                ? IsoeffectiveDoseIn2GyConverter.Convert(possiblyPhysicalDose, numberOfFractions)
                : new IsoeffectiveDoseIn2Gy(possiblyPhysicalDose.Value, possiblyPhysicalDose.Unit);
        }

        protected bool Equals(LymanKutcherBurmanNtcpCalculator other)
        {
            return Td50.Equals(other.Td50) && M.Equals(other.M) && N.Equals(other.N) &&
                   Description == other.Description;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((LymanKutcherBurmanNtcpCalculator)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Td50, M, N, Description);
        }
    }
}
