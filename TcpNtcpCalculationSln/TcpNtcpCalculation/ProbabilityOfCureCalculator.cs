﻿using System;
using System.Collections.Generic;
using System.Linq;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation
{
    /// <summary>
    /// Calculate the probability of cure by multiplying all tumor control probabilities.
    /// <see href="https://books.google.de/books?id=azE8DQAAQBAJ"/>.
    /// </summary>
    public class ProbabilityOfCureCalculator : IProbabilityOfCureCalculator
    {
        public IEnumerable<TcpCalculationConfig> TcpCalculationConfigs { get; }

        public ProbabilityOfCureCalculator(IEnumerable<TcpCalculationConfig> tcpCalculationConfigs)
        {
            TcpCalculationConfigs =
                tcpCalculationConfigs ?? throw new ArgumentNullException(nameof(tcpCalculationConfigs));
            if (!tcpCalculationConfigs.Any())
                throw new ArgumentException(
                    $"At least 1 {nameof(tcpCalculationConfigs)} must be given, but 0 were obtained.");
        }

        public ProbabilityOfCureCalculationResult Calculate(IEnumerable<StructureDvhTuple> structureDvhTuples,
            uint numberOfFractions)
        {
            if (structureDvhTuples == null) throw new ArgumentNullException(nameof(structureDvhTuples));
            if (numberOfFractions == 0)
                throw new ArgumentOutOfRangeException($"{nameof(numberOfFractions)} must be larger 0, but was 0");

            var tcpPoints = new List<TcpPoint>(structureDvhTuples.Count());
            foreach (var tcpCalculationConfig in TcpCalculationConfigs)
            {
                var structureId = tcpCalculationConfig.StructureId;
                var structureDvhTuple =
                    structureDvhTuples.FirstOrDefault(x => x.StructureId == structureId);
                if (structureDvhTuple == null)
                    throw new ArgumentException($"No DVH curve for structure with ID {structureId} is available.");
                var tcp = tcpCalculationConfig.TcpCalculator.Calculate(structureDvhTuple.DvhCurve,
                    numberOfFractions);
                tcpPoints.Add(new TcpPoint(structureDvhTuple.StructureId, tcp));
            }

            var probabilityOfCure = 1.0;
            foreach (var point in tcpPoints) probabilityOfCure *= point.TumorControlProbability.Value;
            return new ProbabilityOfCureCalculationResult(tcpPoints, new ProbabilityOfCure(probabilityOfCure));
        }

        protected bool Equals(ProbabilityOfCureCalculator other)
        {
            return Equals(TcpCalculationConfigs, other.TcpCalculationConfigs);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ProbabilityOfCureCalculator)obj);
        }

        public override int GetHashCode()
        {
            return (TcpCalculationConfigs != null ? TcpCalculationConfigs.GetHashCode() : 0);
        }
    }
}
