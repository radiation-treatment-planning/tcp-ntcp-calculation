﻿using RadiationTreatmentPlanner.Utils.DVH;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    /// <summary>
    /// An interface for normal tissue complication probability calculation.
    /// </summary>
    public interface INormalTissueComplicationProbabilityCalculator
    {
        /// <summary>
        /// Description of tumor control probability calculator.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Calculates the Normal Tissue Complication Value for an inhomogeneous irradiation 
        /// of an organ from a DVH Curve.
        /// </summary>
        /// <param name="dvhCurve">A list of UDose, UVolume tuples. UDoses must be isoeffective doses in 2 Gy fractions. 
        /// An ascending sorting of the UDose values is assumed.</param>
        /// <param name="numberOfFractions">Number of fractions.</param>
        /// <returns>The normal tissue complication probability.</returns>
        NormalTissueComplicationProbability Calculate(DVHCurve dvhCurve, uint numberOfFractions);

        /// <summary>
        /// Get a dictionary with all utilized parameters of the NTCP Calculator.
        /// </summary>
        /// <returns>TcpNtcpParametersDictionary</returns>
        TcpNtcpParametersDictionary GetParametersDictionary();
    }
}
