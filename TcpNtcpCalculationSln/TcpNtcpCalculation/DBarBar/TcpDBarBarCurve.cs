﻿using System;
using System.Collections.Generic;
using System.Linq;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.DBarBar
{
    public class TcpDBarBarCurve
    {
        private readonly TcpDBarBarPoint[] _dataPoints;
        public IEnumerable<TcpDBarBarPoint> DataPoints => _dataPoints;

        public TcpDBarBarCurve(IEnumerable<TcpDBarBarPoint> dataPoints)
        {
            if (dataPoints == null) throw new ArgumentNullException(nameof(dataPoints));
            if (!dataPoints.Any())
                throw new ArgumentException($"At least one {nameof(TcpDBarBarPoint)} must be given.");
            _dataPoints = dataPoints.ToArray();
        }

        public BiologicallyEffectiveUniformDose GetDBarBarWithTcp(TumorControlProbability tcp)
        {
            // TCP is lower minimum case.
            var firstPoint = DataPoints.First();
            if (tcp.Value <= firstPoint.TumorControlProbability.Value)
                return firstPoint.BiologicallyEffectiveUniformDose;

            // TCP is larger maximum case.
            var lastPoint = DataPoints.Last();
            if (tcp.Value >= lastPoint.TumorControlProbability.Value)
                return lastPoint.BiologicallyEffectiveUniformDose;


            for (var i = 0; i < _dataPoints.Length - 1; i++)
            {
                var point = _dataPoints[i];
                var nextPoint = _dataPoints[i + 1];

                // Exact match case.
                if (tcp.Value.Equals(point.TumorControlProbability.Value))
                    return point.BiologicallyEffectiveUniformDose;

                // Lager than current interval case.
                if (tcp.Value >= nextPoint.TumorControlProbability.Value) continue;

                // In current interval case.
                var lowerDistance = Math.Abs(tcp.Value - point.TumorControlProbability.Value);
                var upperDistance = Math.Abs(tcp.Value - nextPoint.TumorControlProbability.Value);
                if (lowerDistance <= upperDistance) return point.BiologicallyEffectiveUniformDose;
                return nextPoint.BiologicallyEffectiveUniformDose;
            }

            return lastPoint.BiologicallyEffectiveUniformDose;
        }

        protected bool Equals(TcpDBarBarCurve other)
        {
            if (other._dataPoints.Length != _dataPoints.Length) return false;
            foreach (var point in other._dataPoints)
                if (!_dataPoints.Contains(point))
                    return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpDBarBarCurve)obj);
        }

        public override int GetHashCode()
        {
            return (DataPoints != null ? DataPoints.GetHashCode() : 0);
        }
    }
}
