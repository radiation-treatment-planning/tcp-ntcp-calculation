﻿using System;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.DBarBar
{
    public class TcpDBarBarPoint
    {
        public TumorControlProbability TumorControlProbability { get; }
        public BiologicallyEffectiveUniformDose BiologicallyEffectiveUniformDose { get; }

        public TcpDBarBarPoint(TumorControlProbability tumorControlProbability,
            BiologicallyEffectiveUniformDose biologicallyEffectiveUniformDose)
        {
            TumorControlProbability = tumorControlProbability;
            BiologicallyEffectiveUniformDose = biologicallyEffectiveUniformDose;
        }

        protected bool Equals(TcpDBarBarPoint other)
        {
            return TumorControlProbability.Equals(other.TumorControlProbability) &&
                   BiologicallyEffectiveUniformDose.Equals(other.BiologicallyEffectiveUniformDose);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TcpDBarBarPoint)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TumorControlProbability, BiologicallyEffectiveUniformDose);
        }
    }
}
