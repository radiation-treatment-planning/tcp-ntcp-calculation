﻿using System;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.DBarBar
{
    public class NtcpDBarBarPoint
    {
        public NormalTissueComplicationProbability NormalTissueComplicationProbability { get; }
        public BiologicallyEffectiveUniformDose BiologicallyEffectiveUniformDose { get; }

        public NtcpDBarBarPoint(NormalTissueComplicationProbability normalTissueComplicationProbability,
            BiologicallyEffectiveUniformDose biologicallyEffectiveUniformDose)
        {
            NormalTissueComplicationProbability = normalTissueComplicationProbability;
            BiologicallyEffectiveUniformDose = biologicallyEffectiveUniformDose;
        }

        protected bool Equals(NtcpDBarBarPoint other)
        {
            return NormalTissueComplicationProbability.Equals(other.NormalTissueComplicationProbability) &&
                   BiologicallyEffectiveUniformDose.Equals(other.BiologicallyEffectiveUniformDose);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((NtcpDBarBarPoint)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(NormalTissueComplicationProbability, BiologicallyEffectiveUniformDose);
        }
    }
}
