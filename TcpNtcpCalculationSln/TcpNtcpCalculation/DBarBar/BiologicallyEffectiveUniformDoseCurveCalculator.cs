﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.DBarBar
{
    public static class BiologicallyEffectiveUniformDoseCurveCalculator
    {
        public static TcpDBarBarCurve CalculateTcpAndDBarBarCurve(this ITumorControlProbabilityCalculator tcpCalculator, uint numberOfFractions,
            UVolume absoluteStructureVolume,
            double step = 0.01, double upperEqd2DoseLimit = 200.0)
        {
            if (absoluteStructureVolume == null) throw new ArgumentNullException(nameof(absoluteStructureVolume));
            if (!absoluteStructureVolume.IsAbsolute())
                throw new ArgumentException(
                    $"Absolute structure volume is required for {nameof(absoluteStructureVolume)}, " +
                    $"but was {absoluteStructureVolume.Unit}.");
            if (numberOfFractions == 0) throw new ArgumentOutOfRangeException(nameof(numberOfFractions));
            if (step <= 0) throw new ArgumentOutOfRangeException(nameof(step));
            if (upperEqd2DoseLimit <= 0) throw new ArgumentOutOfRangeException(nameof(upperEqd2DoseLimit));

            var eqd2DoseValues = Generate.LinearRange(0.0, step, upperEqd2DoseLimit);
            var tcpDBarBarPoints = CalculateTcpDBarBarPoints(tcpCalculator, eqd2DoseValues, numberOfFractions,
                absoluteStructureVolume);
            tcpDBarBarPoints = RemoveLowerPlateau(tcpDBarBarPoints);
            return new TcpDBarBarCurve(tcpDBarBarPoints);
        }
        public static NtcpDBarBarCurve CalculateNtcpAndDBarBarCurve(this INormalTissueComplicationProbabilityCalculator ntcpCalculator,
            uint numberOfFractions, UVolume absoluteStructureVolume, double step = 0.01, double upperEqd2DoseLimit = 200.0)
        {
            if (numberOfFractions == 0) throw new ArgumentOutOfRangeException(nameof(numberOfFractions)); if (absoluteStructureVolume == null) throw new ArgumentNullException(nameof(absoluteStructureVolume));
            if (!absoluteStructureVolume.IsAbsolute())
                throw new ArgumentException(
                    $"Absolute structure volume is required for {nameof(absoluteStructureVolume)}, " +
                    $"but was {absoluteStructureVolume.Unit}.");
            if (step <= 0) throw new ArgumentOutOfRangeException(nameof(step));
            if (upperEqd2DoseLimit <= 0) throw new ArgumentOutOfRangeException(nameof(upperEqd2DoseLimit));

            var eqd2DoseValues = Generate.LinearRange(0.0, step, upperEqd2DoseLimit);
            var ntcpDBarBarPoints = CalculateNtcpDBarBarPoints(ntcpCalculator, eqd2DoseValues, numberOfFractions,
                absoluteStructureVolume);
            ntcpDBarBarPoints = RemoveLowerPlateau(ntcpDBarBarPoints);
            return new NtcpDBarBarCurve(ntcpDBarBarPoints);
        }

        private static List<TcpDBarBarPoint> RemoveLowerPlateau(List<TcpDBarBarPoint> tcpDBarBarPoints)
        {
            var points = new List<TcpDBarBarPoint>(tcpDBarBarPoints.Count);
            for (var i = 0; i < tcpDBarBarPoints.Count - 1; i++)
            {
                var tuple = tcpDBarBarPoints[i];
                var nextTuple = tcpDBarBarPoints[i + 1];

                if (!Equals(tuple.TumorControlProbability.Value, 0.0))
                {
                    points.Add(tuple);
                }
                else if (!Equals(tuple.TumorControlProbability.Value, nextTuple.TumorControlProbability.Value))
                {
                    points.Add(tuple);
                }
            }

            points.Add(tcpDBarBarPoints.Last());
            return points;
        }
        private static List<NtcpDBarBarPoint> RemoveLowerPlateau(List<NtcpDBarBarPoint> ntcpDBarBarPoints)
        {
            var points = new List<NtcpDBarBarPoint>(ntcpDBarBarPoints.Count);
            for (var i = 0; i < ntcpDBarBarPoints.Count - 1; i++)
            {
                var tuple = ntcpDBarBarPoints[i];
                var nextTuple = ntcpDBarBarPoints[i + 1];

                if (!Equals(tuple.NormalTissueComplicationProbability.Value, 0.0))
                {
                    points.Add(tuple);
                }
                else if (!Equals(tuple.NormalTissueComplicationProbability.Value, nextTuple.NormalTissueComplicationProbability.Value))
                {
                    points.Add(tuple);
                }
            }

            points.Add(ntcpDBarBarPoints.Last());
            return points;
        }

        private static List<TcpDBarBarPoint> CalculateTcpDBarBarPoints(ITumorControlProbabilityCalculator tcpCalculator,
            double[] eqd2DoseValues, uint numberOfFractions, UVolume absoluteStructureVolume)
        {
            var tcpDBarBarPoints = new List<TcpDBarBarPoint>(eqd2DoseValues.Length);
            TumorControlProbability previousTcp = new TumorControlProbability(0.0);
            foreach (var value in eqd2DoseValues)
            {
                // Stop adding points if TCP = 1 is reached.
                if (previousTcp.Value.Equals(1.0)) break;

                var dvhCurve =
                    new DVHCurve(
                        new List<Tuple<UDose, UVolume>>
                        {
                            Tuple.Create(new UDose(value, UDose.UDoseUnit.Gy), absoluteStructureVolume)
                        }, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.EQD2);
                var tcp = tcpCalculator.Calculate(dvhCurve, numberOfFractions);
                previousTcp = tcp;
                tcpDBarBarPoints.Add(new TcpDBarBarPoint(tcp, new BiologicallyEffectiveUniformDose(value, UDose.UDoseUnit.Gy)));
            }

            return tcpDBarBarPoints;
        }

        private static List<NtcpDBarBarPoint> CalculateNtcpDBarBarPoints(
            INormalTissueComplicationProbabilityCalculator ntcpCalculator,
            double[] eqd2DoseValues, uint numberOfFractions, UVolume absoluteStructureVolume)
        {
            var tcpDBarBarPoints = new List<NtcpDBarBarPoint>(eqd2DoseValues.Length);
            NormalTissueComplicationProbability previousTcp = new NormalTissueComplicationProbability(0.0);
            foreach (var value in eqd2DoseValues)
            {
                // Stop adding points if NTCP = 1 is reached.
                if (previousTcp.Value.Equals(1.0)) break;

                var dvhCurve =
                    new DVHCurve(
                        new List<Tuple<UDose, UVolume>>
                        {
                            Tuple.Create(new UDose(value, UDose.UDoseUnit.Gy), absoluteStructureVolume)
                        }, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.EQD2);
                var ntcp = ntcpCalculator.Calculate(dvhCurve, numberOfFractions);
                previousTcp = ntcp;
                tcpDBarBarPoints.Add(new NtcpDBarBarPoint(ntcp, new BiologicallyEffectiveUniformDose(value, UDose.UDoseUnit.Gy)));
            }

            return tcpDBarBarPoints;
        }
    }
}
