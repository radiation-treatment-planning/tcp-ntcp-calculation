﻿using System;
using System.Collections.Generic;
using System.Linq;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.DBarBar
{
    public class NtcpDBarBarCurve
    {
        private readonly NtcpDBarBarPoint[] _dataPoints;
        public IEnumerable<NtcpDBarBarPoint> DataPoints => _dataPoints;

        public NtcpDBarBarCurve(IEnumerable<NtcpDBarBarPoint> dataPoints)
        {
            if (dataPoints == null) throw new ArgumentNullException(nameof(dataPoints));
            if (!dataPoints.Any())
                throw new ArgumentException($"At least one {nameof(TcpDBarBarPoint)} must be given.");
            _dataPoints = dataPoints.ToArray();
        }

        public BiologicallyEffectiveUniformDose GetDBarBarWithNtcp(NormalTissueComplicationProbability ntcp)
        {
            // TCP is lower minimum case.
            var firstPoint = DataPoints.First();
            if (ntcp.Value <= firstPoint.NormalTissueComplicationProbability.Value)
                return firstPoint.BiologicallyEffectiveUniformDose;

            // TCP is larger maximum case.
            var lastPoint = DataPoints.Last();
            if (ntcp.Value >= lastPoint.NormalTissueComplicationProbability.Value)
                return lastPoint.BiologicallyEffectiveUniformDose;


            for (var i = 0; i < _dataPoints.Length - 1; i++)
            {
                var point = _dataPoints[i];
                var nextPoint = _dataPoints[i + 1];

                // Exact match case.
                if (ntcp.Value.Equals(point.NormalTissueComplicationProbability.Value))
                    return point.BiologicallyEffectiveUniformDose;

                // Lager than current interval case.
                if (ntcp.Value >= nextPoint.NormalTissueComplicationProbability.Value) continue;

                // In current interval case.
                var lowerDistance = Math.Abs(ntcp.Value - point.NormalTissueComplicationProbability.Value);
                var upperDistance = Math.Abs(ntcp.Value - nextPoint.NormalTissueComplicationProbability.Value);
                if (lowerDistance <= upperDistance) return point.BiologicallyEffectiveUniformDose;
                return nextPoint.BiologicallyEffectiveUniformDose;
            }

            return lastPoint.BiologicallyEffectiveUniformDose;
        }

        protected bool Equals(NtcpDBarBarCurve other)
        {
            if (other._dataPoints.Length != _dataPoints.Length) return false;
            foreach (var point in other._dataPoints)
                if (!_dataPoints.Contains(point))
                    return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((NtcpDBarBarCurve)obj);
        }

        public override int GetHashCode()
        {
            return (_dataPoints != null ? _dataPoints.GetHashCode() : 0);
        }
    }
}
