﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation
{
    public static class TcpCurveCalculator
    {
        public static TcpCurve CalculateTcpCurve(this ITumorControlProbabilityCalculator tcpCalculator,
            uint numberOfFractions, Option<UVolume> structureVolume, double step = 0.01, double upperDoseLimitInGy = 200.0)
        {
            if (tcpCalculator == null) throw new ArgumentNullException(nameof(tcpCalculator));
            if (numberOfFractions == 0) throw new ArgumentOutOfRangeException(nameof(numberOfFractions));
            if (step <= 0) throw new ArgumentOutOfRangeException(nameof(step));
            if (upperDoseLimitInGy <= 0) throw new ArgumentOutOfRangeException(nameof(upperDoseLimitInGy));

            var doseValues = Generate.LinearRange(0.0, step, upperDoseLimitInGy);
            var curvePoints = CalculateCurvePoints(tcpCalculator, numberOfFractions, doseValues,
                structureVolume.ValueOr(new UVolume(1, UVolume.VolumeUnit.ccm)));
            curvePoints = RemoveLowerPlateau(curvePoints);
            return new TcpCurve(curvePoints);
        }

        private static List<TcpCurvePoint> RemoveLowerPlateau(List<TcpCurvePoint> curvePoints)
        {
            var points = new List<TcpCurvePoint>(curvePoints.Count);
            for (var i = 0; i < curvePoints.Count - 1; i++)
            {
                var tuple = curvePoints[i];
                var nextTuple = curvePoints[i + 1];

                if (!Equals(tuple.TumorControlProbability.Value, 0.0))
                {
                    points.Add(tuple);
                }
                else if (!Equals(tuple.TumorControlProbability.Value, nextTuple.TumorControlProbability.Value))
                {
                    points.Add(tuple);
                }
            }

            points.Add(curvePoints.Last());
            points.Capacity = points.Count;
            return points;
        }

        private static List<TcpCurvePoint> CalculateCurvePoints(ITumorControlProbabilityCalculator tcpCalculator,
            uint numberOfFractions, double[] doseValues, UVolume structureVolume)
        {
            var curvePoints = new List<TcpCurvePoint>(doseValues.Length);
            TumorControlProbability previousTcp = new TumorControlProbability(0.0);
            foreach (var value in doseValues)
            {
                // Stop adding points if TCP = 1 is reached.
                if (previousTcp.Value.Equals(1.0)) break;

                var dvhCurve =
                    new DVHCurve(
                        new List<Tuple<UDose, UVolume>>
                        {
                            Tuple.Create(new UDose(value, UDose.UDoseUnit.Gy), structureVolume)
                        }, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL);
                var tcp = tcpCalculator.Calculate(dvhCurve, numberOfFractions);
                previousTcp = tcp;
                curvePoints.Add(new TcpCurvePoint(tcp, new UDose(value, UDose.UDoseUnit.Gy)));
            }

            return curvePoints;
        }
    }
}
