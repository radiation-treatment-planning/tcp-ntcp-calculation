﻿using System;
using Optional;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation
{
    public class PoissonVoxelResponseCalculator : IVoxelResponseCalculationStrategy
    {
        private TcpNtcpParametersDictionary _parametersDict;
        public FiftyPercentControlRate D50 { get; }
        public Slope Gamma { get; }
        public PoissonVoxelResponseCalculator(FiftyPercentControlRate d50, Slope gamma)
        {
            D50 = d50;
            Gamma = gamma;
        }

        /// <summary>
        /// Calculates the voxel response with the Poisson model
        /// DOI: 10.1016/j.ijrobp.2020.11.017
        /// </summary>
        /// <param name="isoeffectiveDoseIn2Gy">The isoeffective Dose for 2 Gy per
        ///     fraction <see cref="IsoeffectiveDoseIn2GyConverter" />.</param>
        /// <returns></returns>
        public VoxelResponse Calculate(IsoeffectiveDoseIn2Gy isoeffectiveDoseIn2Gy)
        {
            if (isoeffectiveDoseIn2Gy.Unit != D50.DoseUnit)
                throw new ArgumentException(
                    $"Dose units of D50 and isoeffectiveDoseIn2Gy must be equal, but were {D50.DoseUnit} and {isoeffectiveDoseIn2Gy.Unit}");

            return new VoxelResponse(Math.Exp(
                -Math.Exp(Math.E * Gamma.Value - isoeffectiveDoseIn2Gy.Value / D50.Value * (Math.E * Gamma.Value - LogLog2()))));
        }

        public TcpNtcpParametersDictionary GetParametersDictionary()
        {
            if (_parametersDict != null) return _parametersDict;
            _parametersDict = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("D50", D50.Value, Option.Some(D50.DoseUnit.ToString()))
                .WithParameterSet("Gamma", Gamma.Value, Option.None<string>())
                .Build();
            return _parametersDict;
        }

        /// <summary>
        ///     Calculates the natural logarithm of the natural logarithm of 2.
        /// </summary>
        /// <returns>A double representing the natural logarithm of the natural logarithm of 2.</returns>
        private double LogLog2()
        {
            return Math.Log(Math.Log(2, Math.E), Math.E);
        }

        protected bool Equals(PoissonVoxelResponseCalculator other)
        {
            return D50.Equals(other.D50) && Gamma.Equals(other.Gamma);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PoissonVoxelResponseCalculator)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(D50, Gamma);
        }

        public static bool operator ==(PoissonVoxelResponseCalculator left, PoissonVoxelResponseCalculator right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PoissonVoxelResponseCalculator left, PoissonVoxelResponseCalculator right)
        {
            return !Equals(left, right);
        }
    }
}
