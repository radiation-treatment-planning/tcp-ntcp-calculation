﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation
{
    public static class NtcpCurveCalculator
    {
        public static NtcpCurve CalculateNtcpCurve(this INormalTissueComplicationProbabilityCalculator ntcpCalculator,
            uint numberOfFractions, Option<UVolume> structureVolume, double step = 0.01, double upperDoseLimitInGy = 200.0)
        {
            if (ntcpCalculator == null) throw new ArgumentNullException(nameof(ntcpCalculator));
            if (numberOfFractions == 0) throw new ArgumentOutOfRangeException(nameof(numberOfFractions));
            if (step <= 0) throw new ArgumentOutOfRangeException(nameof(step));
            if (upperDoseLimitInGy <= 0) throw new ArgumentOutOfRangeException(nameof(upperDoseLimitInGy));

            var doseValues = Generate.LinearRange(0.0, step, upperDoseLimitInGy);
            var curvePoints = CalculateCurvePoints(ntcpCalculator, numberOfFractions, doseValues,
                structureVolume.ValueOr(new UVolume(1, UVolume.VolumeUnit.ccm)));
            curvePoints = RemoveLowerPlateau(curvePoints);
            return new NtcpCurve(curvePoints);
        }

        private static List<NtcpCurvePoint> RemoveLowerPlateau(List<NtcpCurvePoint> curvePoints)
        {
            var points = new List<NtcpCurvePoint>(curvePoints.Count);
            for (var i = 0; i < curvePoints.Count - 1; i++)
            {
                var tuple = curvePoints[i];
                var nextTuple = curvePoints[i + 1];

                if (!Equals(tuple.NormalTissueComplicationProbability.Value, 0.0))
                {
                    points.Add(tuple);
                }
                else if (!Equals(tuple.NormalTissueComplicationProbability.Value, nextTuple.NormalTissueComplicationProbability.Value))
                {
                    points.Add(tuple);
                }
            }

            points.Add(curvePoints.Last());
            points.Capacity = points.Count;
            return points;
        }

        private static List<NtcpCurvePoint> CalculateCurvePoints(
            INormalTissueComplicationProbabilityCalculator ntcpCalculator,
            uint numberOfFractions, double[] doseValues, UVolume structureVolume)
        {
            var curvePoints = new List<NtcpCurvePoint>(doseValues.Length);
            NormalTissueComplicationProbability previousNtcp = new NormalTissueComplicationProbability(0.0);
            foreach (var value in doseValues)
            {
                // Stop adding points if TCP = 1 is reached.
                if (previousNtcp.Value.Equals(1.0)) break;

                var dvhCurve =
                    new DVHCurve(
                        new List<Tuple<UDose, UVolume>>
                        {
                            Tuple.Create(new UDose(value, UDose.UDoseUnit.Gy), structureVolume)
                        }, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL);
                var ntcp = ntcpCalculator.Calculate(dvhCurve, numberOfFractions);
                previousNtcp = ntcp;
                curvePoints.Add(new NtcpCurvePoint(ntcp, new UDose(value, UDose.UDoseUnit.Gy)));
            }

            return curvePoints;
        }
    }
}
