﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.ExampleWpf
{
    readonly struct NtcpDoseTuple
    {
        public NormalTissueComplicationProbability NormalTissueComplicationProbability { get; }
        public UDose Dose { get; }

        public NtcpDoseTuple(NormalTissueComplicationProbability normalTissueComplicationProbability, UDose dose)
        {
            NormalTissueComplicationProbability = normalTissueComplicationProbability;
            Dose = dose ?? throw new ArgumentNullException(nameof(dose));
        }

        public bool Equals(NtcpDoseTuple other)
        {
            return NormalTissueComplicationProbability.Equals(other.NormalTissueComplicationProbability) &&
                   Equals(Dose, other.Dose);
        }

        public override bool Equals(object obj)
        {
            return obj is NtcpDoseTuple other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(NormalTissueComplicationProbability, Dose);
        }
    }
}
