﻿using OxyPlot;
using Prism.Commands;
using Prism.Mvvm;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.ExampleWpf.ViewModels
{
    class TcpPoissonWithNormalizedVolumeCorrectionViewModel : BindableBase
    {
        private TcpWithNormalizedVolumeCorrectionCalculator _calculator;

        public TcpPoissonWithNormalizedVolumeCorrectionViewModel()
        {
            InitializeProperties();
            InitializeAndExecuteCalculator();
        }

        private void InitializeProperties()
        {
            AlphaOverBeta = 26.53;
            D50 = 23.73;
            Gamma = 0.487;
            NumberOfFractions = 12;

            AlphaOverBetaText = AlphaOverBeta.ToString();
            D50Text = D50.ToString();
            GammaText = Gamma.ToString();
            NumberOfFractionsText = NumberOfFractions.ToString();
        }

        private PlotModel _plotModel;
        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set { SetProperty(ref _plotModel, value); }
        }

        private string _modelDescription;
        public string ModelDescription
        {
            get { return _modelDescription; }
            set { SetProperty(ref _modelDescription, value); }
        }

        private string _modelParameters;
        public string ModelParameters
        {
            get { return _modelParameters; }
            set { SetProperty(ref _modelParameters, value); }
        }

        private double _alphaOverBeta;
        public double AlphaOverBeta
        {
            get { return _alphaOverBeta; }
            set { SetProperty(ref _alphaOverBeta, value); }
        }

        private double _d50;
        public double D50
        {
            get { return _d50; }
            set { SetProperty(ref _d50, value); }
        }

        private double _gamma;
        public double Gamma
        {
            get { return _gamma; }
            set { SetProperty(ref _gamma, value); }
        }

        private uint _numberOfFractions;
        public uint NumberOfFractions
        {
            get { return _numberOfFractions; }
            set { SetProperty(ref _numberOfFractions, value); }
        }

        private string _alphaOverBetaText;
        public string AlphaOverBetaText
        {
            get { return _alphaOverBetaText; }
            set { SetProperty(ref _alphaOverBetaText, value); }
        }

        private string _d50Text;
        public string D50Text
        {
            get { return _d50Text; }
            set { SetProperty(ref _d50Text, value); }
        }

        private string _gammaText;
        public string GammaText
        {
            get { return _gammaText; }
            set { SetProperty(ref _gammaText, value); }
        }

        private string _numberOfFractionsText;
        public string NumberOfFractionsText
        {
            get { return _numberOfFractionsText; }
            set { SetProperty(ref _numberOfFractionsText, value); }
        }

        private void InitializeAndExecuteCalculator()
        {
            ParseAllValues();
            _calculator = new TcpWithNormalizedVolumeCorrectionCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(AlphaOverBeta, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(D50, UDose.UDoseUnit.Gy),
                    new Slope(Gamma)));
            PlotModel = TcpNtcpPlotModelCreator.Create(_calculator, NumberOfFractions,
                0.9999);
            ModelDescription = _calculator.Description;
            ModelParameters =
                TcpNtcpPlotHelper.SummarizeParameters(_calculator, NumberOfFractions);
        }

        private void ParseAllValues()
        {
            AlphaOverBeta = double.Parse(AlphaOverBetaText);
            D50 = double.Parse(D50Text);
            Gamma = double.Parse(GammaText);
            NumberOfFractions = uint.Parse(NumberOfFractionsText);
        }

        private DelegateCommand _recalculateCommand;
        public DelegateCommand CalculateCommand =>
            _recalculateCommand ?? (_recalculateCommand = new DelegateCommand(ExecuteCalculateCommand, CanExecuteCalculateCommand)
                .ObservesProperty(() => AlphaOverBetaText)
                .ObservesProperty(() => D50Text)
                .ObservesProperty(() => GammaText)
                .ObservesProperty(() => NumberOfFractionsText));

        void ExecuteCalculateCommand() => InitializeAndExecuteCalculator();

        bool CanExecuteCalculateCommand()
        {
            double alphaOverBeta;
            var alphaOverBetaSuccess = double.TryParse(AlphaOverBetaText, out alphaOverBeta);
            if (!alphaOverBetaSuccess) return false;
            if (alphaOverBeta <= 0) return false;

            double d50;
            var d50Success = double.TryParse(D50Text, out d50);
            if (!d50Success) return false;
            if (d50 < 0) return false;

            double gamma;
            var gammaSuccess = double.TryParse(GammaText, out gamma);
            if (!gammaSuccess) return false;
            if (gamma <= 0) return false;

            uint numberOfFractions;
            var numberOfFractionsSuccess = uint.TryParse(NumberOfFractionsText, out numberOfFractions);
            if (!numberOfFractionsSuccess) return false;
            return numberOfFractions != 0;
        }
    }
}
