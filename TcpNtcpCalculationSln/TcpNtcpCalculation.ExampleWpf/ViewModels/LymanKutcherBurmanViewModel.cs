﻿using OxyPlot;
using Prism.Commands;
using Prism.Mvvm;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.ExampleWpf.ViewModels
{
    class LymanKutcherBurmanViewModel : BindableBase
    {
        private LymanKutcherBurmanNtcpCalculator _calculator;

        public LymanKutcherBurmanViewModel()
        {
            InitializeParameters();
            InitializeAndExecuteCalculator();
        }

        private void InitializeParameters()
        {
            TD50 = 27.95;
            Gamma = 4.63;
            N = 0.048;
            AlphaOverBeta = 3.0;
            NumberOfFractions = 24u;

            TD50Text = TD50.ToString();
            GammaText = Gamma.ToString();
            NText = N.ToString();
            AlphaOverBetaText = AlphaOverBeta.ToString();
            NumberOfFractionsText = NumberOfFractions.ToString();
        }

        private PlotModel _plotModel;
        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set { SetProperty(ref _plotModel, value); }
        }

        private string _modelDescription;
        public string ModelDescription
        {
            get { return _modelDescription; }
            set { SetProperty(ref _modelDescription, value); }
        }

        private string _modelParameters;
        public string ModelParameters
        {
            get { return _modelParameters; }
            set { SetProperty(ref _modelParameters, value); }
        }

        private double _td50;
        public double TD50
        {
            get { return _td50; }
            set { SetProperty(ref _td50, value); }
        }

        private double _gamma;
        public double Gamma
        {
            get { return _gamma; }
            set { SetProperty(ref _gamma, value); }
        }

        private double _n;
        public double N
        {
            get { return _n; }
            set { SetProperty(ref _n, value); }
        }

        private double _alphaOverBeta;
        public double AlphaOverBeta
        {
            get { return _alphaOverBeta; }
            set { SetProperty(ref _alphaOverBeta, value); }
        }

        private uint _numberOfFactions;
        public uint NumberOfFractions
        {
            get { return _numberOfFactions; }
            set { SetProperty(ref _numberOfFactions, value); }
        }

        private string _td50Text;
        public string TD50Text
        {
            get { return _td50Text; }
            set { SetProperty(ref _td50Text, value); }
        }

        private string _gammaText;
        public string GammaText
        {
            get { return _gammaText; }
            set { SetProperty(ref _gammaText, value); }
        }
        private string _nText;
        public string NText
        {
            get { return _nText; }
            set { SetProperty(ref _nText, value); }
        }

        private string _alphaOverBetaText;
        public string AlphaOverBetaText
        {
            get { return _alphaOverBetaText; }
            set { SetProperty(ref _alphaOverBetaText, value); }
        }

        private string _numberOfFractionsText;
        public string NumberOfFractionsText
        {
            get { return _numberOfFractionsText; }
            set { SetProperty(ref _numberOfFractionsText, value); }
        }

        private void InitializeAndExecuteCalculator()
        {
            ParseAllValues();
            _calculator = new LymanKutcherBurmanNtcpCalculator(new TD50(TD50, UDose.UDoseUnit.Gy), new Slope(Gamma),
                new N(N), new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(AlphaOverBeta, UDose.UDoseUnit.Gy)));
            PlotModel = TcpNtcpPlotModelCreator.Create(_calculator, NumberOfFractions,
                0.9999);
            ModelDescription = _calculator.Description;
            ModelParameters =
                TcpNtcpPlotHelper.SummarizeParameters(_calculator, NumberOfFractions);
        }
        private void ParseAllValues()
        {
            AlphaOverBeta = double.Parse(AlphaOverBetaText);
            Gamma = double.Parse(GammaText);
            TD50 = double.Parse(TD50Text);
            N = double.Parse(NText);
            NumberOfFractions = uint.Parse(NumberOfFractionsText);
        }

        private DelegateCommand _recalculateCommand;
        public DelegateCommand CalculateCommand =>
            _recalculateCommand ?? (_recalculateCommand = new DelegateCommand(ExecuteCalculateCommand, CanExecuteCalculateCommand)
                .ObservesProperty(() => AlphaOverBetaText)
                .ObservesProperty(() => TD50Text)
                .ObservesProperty(() => GammaText)
                .ObservesProperty(() => NumberOfFractionsText));

        void ExecuteCalculateCommand() => InitializeAndExecuteCalculator();

        bool CanExecuteCalculateCommand()
        {
            double alphaOverBeta;
            var alphaOverBetaSuccess = double.TryParse(AlphaOverBetaText, out alphaOverBeta);
            if (!alphaOverBetaSuccess) return false;
            if (alphaOverBeta <= 0) return false;

            double gamma;
            var gammaSuccess = double.TryParse(GammaText, out gamma);
            if (!gammaSuccess) return false;
            if (gamma <= 0) return false;

            double td50;
            var td50Success = double.TryParse(TD50Text, out td50);
            if (!td50Success) return false;
            if (td50 <= 0) return false;

            double n;
            var nSuccess = double.TryParse(NText, out n);
            if (!nSuccess) return false;
            if (n <= 0) return false;

            uint numberOfFractions;
            var numberOfFractionsSuccess = uint.TryParse(NumberOfFractionsText, out numberOfFractions);
            if (!numberOfFractionsSuccess) return false;

            return numberOfFractions > 0;
        }
    }
}
