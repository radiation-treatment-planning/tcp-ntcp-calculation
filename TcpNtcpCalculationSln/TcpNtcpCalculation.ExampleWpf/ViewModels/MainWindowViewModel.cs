﻿using Prism.Mvvm;

namespace TcpNtcpCalculation.ExampleWpf.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "TCP NTCP Models";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public MainWindowViewModel()
        {

        }
    }
}
