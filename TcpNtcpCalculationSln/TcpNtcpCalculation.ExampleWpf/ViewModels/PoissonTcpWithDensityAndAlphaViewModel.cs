﻿using OxyPlot;
using Prism.Commands;
using Prism.Mvvm;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.ExampleWpf.ViewModels
{
    public class PoissonTcpWithDensityAndAlphaViewModel : BindableBase
    {
        private PoissonTcpWithDensityAndAlphaCalculator _calculator;

        public PoissonTcpWithDensityAndAlphaViewModel()
        {
            InitializeParameters();
            InitializeAndExecuteCalculator();
        }

        private void InitializeParameters()
        {
            CellDensity = 280000000;
            Alpha = 0.52107;
            AlphaOverBeta = 1.6;
            NumberOfFractions = 24u;
            VolumeInCcm = 100;


            CellDensityText = CellDensity.ToString();
            AlphaText = Alpha.ToString();
            AlphaOverBetaText = AlphaOverBeta.ToString();
            NumberOfFractionsText = NumberOfFractions.ToString();
            VolumeInCcmText = VolumeInCcm.ToString();
        }

        private PlotModel _plotModel;
        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set { SetProperty(ref _plotModel, value); }
        }

        private string _modelDescription;
        public string ModelDescription
        {
            get { return _modelDescription; }
            set { SetProperty(ref _modelDescription, value); }
        }

        private string _modelParameters;
        public string ModelParameters
        {
            get { return _modelParameters; }
            set { SetProperty(ref _modelParameters, value); }
        }

        private double _cellDensity;
        public double CellDensity
        {
            get { return _cellDensity; }
            set { SetProperty(ref _cellDensity, value); }
        }

        private double _alpha;
        public double Alpha
        {
            get { return _alpha; }
            set { SetProperty(ref _alpha, value); }
        }

        private double _alphaOverBeta;
        public double AlphaOverBeta
        {
            get { return _alphaOverBeta; }
            set { SetProperty(ref _alphaOverBeta, value); }
        }

        private uint _numberOfFactions;
        public uint NumberOfFractions
        {
            get { return _numberOfFactions; }
            set { SetProperty(ref _numberOfFactions, value); }
        }

        private double _volumeInCcm;
        public double VolumeInCcm
        {
            get { return _volumeInCcm; }
            set { SetProperty(ref _volumeInCcm, value); }
        }

        private string _cellDensityText;
        public string CellDensityText
        {
            get { return _cellDensityText; }
            set { SetProperty(ref _cellDensityText, value); }
        }

        private string _alphaText;
        public string AlphaText
        {
            get { return _alphaText; }
            set { SetProperty(ref _alphaText, value); }
        }

        private string _alphaOverBetaText;
        public string AlphaOverBetaText
        {
            get { return _alphaOverBetaText; }
            set { SetProperty(ref _alphaOverBetaText, value); }
        }

        private string _numberOfFractionsText;
        public string NumberOfFractionsText
        {
            get { return _numberOfFractionsText; }
            set { SetProperty(ref _numberOfFractionsText, value); }
        }

        private string _volumeInCcmText;
        public string VolumeInCcmText
        {
            get { return _volumeInCcmText; }
            set { SetProperty(ref _volumeInCcmText, value); }
        }

        private void InitializeAndExecuteCalculator()
        {
            ParseAllValues();
            _calculator = new PoissonTcpWithDensityAndAlphaCalculator(
                new ClonogenicCellDensity(CellDensity), new Alpha(Alpha),
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(AlphaOverBeta, UDose.UDoseUnit.Gy)));
            PlotModel = TcpNtcpPlotModelCreator.Create(_calculator, NumberOfFractions,
                0.9999, VolumeInCcm);
            ModelDescription = _calculator.Description;
            ModelParameters =
                TcpNtcpPlotHelper.SummarizeParameters(_calculator, NumberOfFractions);
        }
        private void ParseAllValues()
        {
            AlphaOverBeta = double.Parse(AlphaOverBetaText);
            Alpha = double.Parse(AlphaText);
            CellDensity = double.Parse(CellDensityText);
            NumberOfFractions = uint.Parse(NumberOfFractionsText);
            VolumeInCcm = double.Parse(VolumeInCcmText);
        }

        private DelegateCommand _recalculateCommand;
        public DelegateCommand CalculateCommand =>
            _recalculateCommand ?? (_recalculateCommand = new DelegateCommand(ExecuteCalculateCommand, CanExecuteCalculateCommand)
                .ObservesProperty(() => AlphaOverBetaText)
                .ObservesProperty(() => CellDensityText)
                .ObservesProperty(() => AlphaText)
                .ObservesProperty(() => NumberOfFractionsText)
                .ObservesProperty(() => VolumeInCcmText)
            );

        void ExecuteCalculateCommand() => InitializeAndExecuteCalculator();

        bool CanExecuteCalculateCommand()
        {
            double alphaOverBeta;
            var alphaOverBetaSuccess = double.TryParse(AlphaOverBetaText, out alphaOverBeta);
            if (!alphaOverBetaSuccess) return false;
            if (alphaOverBeta <= 0) return false;

            double alpha;
            var alphaSuccess = double.TryParse(AlphaText, out alpha);
            if (!alphaSuccess) return false;
            if (alpha <= 0) return false;

            double cellDensity;
            var cellDensitySuccess = double.TryParse(CellDensityText, out cellDensity);
            if (!cellDensitySuccess) return false;
            if (cellDensity == 0) return false;

            double volume;
            var volumeSuccess = double.TryParse(VolumeInCcmText, out volume);
            if (!volumeSuccess) return false;
            if (volume <= 0) return false;

            uint numberOfFractions;
            var numberOfFractionsSuccess = uint.TryParse(NumberOfFractionsText, out numberOfFractions);
            if (!numberOfFractionsSuccess) return false;

            return numberOfFractions > 0;
        }
    }
}
