﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.ExampleWpf
{
    readonly struct TcpDoseTuple
    {
        public TumorControlProbability TumorControlProbability { get; }
        public UDose Dose { get; }

        public TcpDoseTuple(TumorControlProbability tumorControlProbability, UDose dose)
        {
            TumorControlProbability = tumorControlProbability;
            Dose = dose ?? throw new ArgumentNullException(nameof(dose));
        }

        public bool Equals(TcpDoseTuple other)
        {
            return TumorControlProbability.Equals(other.TumorControlProbability) && Equals(Dose, other.Dose);
        }

        public override bool Equals(object obj)
        {
            return obj is TcpDoseTuple other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TumorControlProbability, Dose);
        }
    }
}
