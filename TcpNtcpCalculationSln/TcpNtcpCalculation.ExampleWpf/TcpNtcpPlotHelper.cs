﻿using System.Linq;
using MathNet.Numerics;
using RadiationTreatmentPlanner.Utils.Dose;

namespace TcpNtcpCalculation.ExampleWpf
{
    static class TcpNtcpPlotHelper
    {
        public static UDose[] GenerateDoseRange(double minDoseValue, double maxDoseValue, double step,
            UDose.UDoseUnit doseUnit)
        {
            var values = Generate.LinearRange(minDoseValue, step, maxDoseValue);
            return values.Select(x => new UDose(x, doseUnit)).ToArray();
        }

        public static string SummarizeParameters(ITumorControlProbabilityCalculator tcpCalculator, uint numberOfFractions)
        {
            var parameterSets = tcpCalculator.GetParametersDictionary().GetAllParameterSets();
            var msg = "";
            foreach (var parameterSet in parameterSets)
            {
                msg += $"{parameterSet.ParameterId}: \t";
                msg += $"{parameterSet.ValueUnitTuple.Value}";
                msg += parameterSet.ValueUnitTuple.Unit.ValueOr("");
                msg += "\n";
            }

            msg += $"Number of fractions: {numberOfFractions}";
            return msg;
        }

        public static string SummarizeParameters(INormalTissueComplicationProbabilityCalculator ntcpCalculator, uint numberOfFractions)
        {
            var parameterSets = ntcpCalculator.GetParametersDictionary().GetAllParameterSets();
            var msg = "";
            foreach (var parameterSet in parameterSets)
            {
                msg += $"{parameterSet.ParameterId}: \t";
                msg += $"{parameterSet.ValueUnitTuple.Value}";
                msg += parameterSet.ValueUnitTuple.Unit.ValueOr("");
                msg += "\n";
            }

            msg += $"Number of fractions: {numberOfFractions}";
            return msg;
        }
    }
}
