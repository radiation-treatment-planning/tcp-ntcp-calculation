﻿using System;
using System.Collections.Generic;
using System.Linq;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.DBarBar;

namespace TcpNtcpCalculation.ExampleWpf
{
    static class TcpNtcpPlotModelCreator
    {

        public static PlotModel CreateDBarBarPlotModel(ITumorControlProbabilityCalculator tcpCalculator,
            uint numberOfFractions, UVolume structureVolume, double maxTcpValue = 0.999)
        {
            var plotModel = new PlotModel { DefaultFontSize = 18 };
            var tcpAndDBarBarCurve = tcpCalculator.CalculateTcpAndDBarBarCurve(numberOfFractions, structureVolume);
            var points = tcpAndDBarBarCurve.DataPoints.Where(x => x.TumorControlProbability.Value <= maxTcpValue);
            var curve = new TcpDBarBarCurve(points);
            AddDBarBarAxes(plotModel, curve);
            var lineSeries = CreateDBarBarLineSeries(curve);
            plotModel.Series.Add(lineSeries);
            return plotModel;
        }

        public static PlotModel CreateDBarBarPlotModel(INormalTissueComplicationProbabilityCalculator ntcpCalculator,
            uint numberOfFractions, UVolume structureVolume, double maxTcpValue = 0.999)
        {
            var plotModel = new PlotModel { DefaultFontSize = 18 };
            var tcpAndDBarBarCurve = ntcpCalculator.CalculateNtcpAndDBarBarCurve(numberOfFractions, structureVolume);
            var points = tcpAndDBarBarCurve.DataPoints.Where(x => x.NormalTissueComplicationProbability.Value <= maxTcpValue);
            var curve = new NtcpDBarBarCurve(points);
            AddDBarBarAxes(plotModel, curve);
            var lineSeries = CreateDBarBarLineSeries(curve);
            plotModel.Series.Add(lineSeries);
            return plotModel;
        }

        public static PlotModel Create(ITumorControlProbabilityCalculator tcpCalculator, uint numberOfFractions, double maxTcpValue, double structureVolumeInCcm = 100)
        {
            if (structureVolumeInCcm <= 0)
                throw new ArgumentOutOfRangeException($"{nameof(structureVolumeInCcm)} must be larger 0.");
            var plotModel = new PlotModel { DefaultFontSize = 18 };
            var tcpDoseTuples = CreateTcpDoseTuples(tcpCalculator, numberOfFractions, maxTcpValue, structureVolumeInCcm);
            var linearSeries = CreateLineSeries(tcpDoseTuples);
            plotModel.Series.Add(linearSeries);
            AddAxes(plotModel, tcpDoseTuples);
            return plotModel;
        }

        public static PlotModel Create(INormalTissueComplicationProbabilityCalculator ntcpCalculator, uint numberOfFractions, double maxTcpValue)
        {
            var plotModel = new PlotModel { DefaultFontSize = 18 };
            var ntcpDoseTuples = CreateNtcpDoseTuples(ntcpCalculator, numberOfFractions, maxTcpValue);
            var linearSeries = CreateLineSeries(ntcpDoseTuples);
            plotModel.Series.Add(linearSeries);
            AddAxes(plotModel, ntcpDoseTuples);
            return plotModel;
        }

        private static List<NtcpDoseTuple> CreateNtcpDoseTuples(INormalTissueComplicationProbabilityCalculator ntcpCalculator,
            uint numberOfFractions, double maxTcpValue)
        {
            var doseRange = TcpNtcpPlotHelper.GenerateDoseRange(0, 200, 0.01, UDose.UDoseUnit.Gy);
            var ntcpDoseTuples = new List<NtcpDoseTuple>(doseRange.Length);
            foreach (var dose in doseRange)
            {
                try
                {
                    var dvh = new DVHCurve(
                        new List<Tuple<UDose, UVolume>>
                            { Tuple.Create(dose, new UVolume(100, UVolume.VolumeUnit.ccm)) },
                        DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL);
                    var ntcp = ntcpCalculator.Calculate(dvh, numberOfFractions);

                    // Stop adding points at the plateau.
                    if (ntcp.Value > maxTcpValue && !Equals(ntcp.Value, 1.0)) break;
                    ntcpDoseTuples.Add(new NtcpDoseTuple(ntcp, dose));
                }
                catch
                {
                    // ignored
                }
            }

            return RemoveLowerPlateau(ntcpDoseTuples);
        }

        private static void AddDBarBarAxes(PlotModel plotModel, TcpDBarBarCurve curve)
        {
            var doseUnit = curve.DataPoints.First().BiologicallyEffectiveUniformDose.Unit.ToString();
            plotModel.Axes.Add(new LinearAxis
            {
                Title = "D\u0305\u0305",
                Unit = doseUnit,
                Position = AxisPosition.Bottom,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray,
            });
            plotModel.Axes.Add(new LinearAxis
            {
                Title = "TCP",
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray,
            });
        }

        private static void AddDBarBarAxes(PlotModel plotModel, NtcpDBarBarCurve curve)
        {
            var doseUnit = curve.DataPoints.First().BiologicallyEffectiveUniformDose.Unit.ToString();
            plotModel.Axes.Add(new LinearAxis
            {
                Title = "D\u0305\u0305",
                Unit = doseUnit,
                Position = AxisPosition.Bottom,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray,
            });
            plotModel.Axes.Add(new LinearAxis
            {
                Title = "NTCP",
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray,
            });
        }

        private static void AddAxes(PlotModel plotModel, List<TcpDoseTuple> doseTuples)
        {
            if (!doseTuples.Any()) return;
            var doseUnit = doseTuples.First().Dose.Unit;
            plotModel.Axes.Add(new LinearAxis
            {
                Title = "Physical dose",
                Unit = doseUnit.ToString(),
                Position = AxisPosition.Bottom,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray,
            });
            plotModel.Axes.Add(new LinearAxis
            {
                Title = "TCP",
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray,
            });
        }

        private static void AddAxes(PlotModel plotModel, List<NtcpDoseTuple> doseTuples)
        {
            if (!doseTuples.Any()) return;
            var doseUnit = doseTuples.First().Dose.Unit;
            plotModel.Axes.Add(new LinearAxis
            {
                Title = "Physical dose",
                Unit = doseUnit.ToString(),
                Position = AxisPosition.Bottom,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray,
            });
            plotModel.Axes.Add(new LinearAxis
            {
                Title = "NTCP",
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray,
            });
        }

        private static LineSeries CreateLineSeries(List<TcpDoseTuple> tcpDoseTuples)
        {
            if (!tcpDoseTuples.Any()) return new LineSeries();
            var dataPoints = tcpDoseTuples.Select(x => new DataPoint(x.Dose.Value, x.TumorControlProbability.Value));
            return new LineSeries { ItemsSource = dataPoints };
        }

        private static LineSeries CreateLineSeries(List<NtcpDoseTuple> tcpDoseTuples)
        {
            if (!tcpDoseTuples.Any()) return new LineSeries();
            var dataPoints = tcpDoseTuples.Select(x =>
                new DataPoint(x.Dose.Value, x.NormalTissueComplicationProbability.Value));
            return new LineSeries { ItemsSource = dataPoints };
        }


        private static LineSeries CreateDBarBarLineSeries(TcpDBarBarCurve curve)
        {
            var dataPoints = curve.DataPoints.Select(x =>
                new DataPoint(x.BiologicallyEffectiveUniformDose.Value, x.TumorControlProbability.Value));
            return new LineSeries { ItemsSource = dataPoints };
        }

        private static LineSeries CreateDBarBarLineSeries(NtcpDBarBarCurve curve)
        {
            var dataPoints = curve.DataPoints.Select(x =>
                new DataPoint(x.BiologicallyEffectiveUniformDose.Value, x.NormalTissueComplicationProbability.Value));
            return new LineSeries { ItemsSource = dataPoints };
        }

        private static List<TcpDoseTuple> CreateTcpDoseTuples(ITumorControlProbabilityCalculator tcpCalculator,
            uint numberOfFractions, double maxTcpValue, double volumeInCcm = 100.0)
        {
            var doseRange = TcpNtcpPlotHelper.GenerateDoseRange(0, 200, 0.01, UDose.UDoseUnit.Gy);
            var tcpDoseTuples = new List<TcpDoseTuple>(doseRange.Length);
            foreach (var dose in doseRange)
            {
                try
                {
                    var dvh = new DVHCurve(
                        new List<Tuple<UDose, UVolume>>
                            { Tuple.Create(dose, new UVolume(volumeInCcm, UVolume.VolumeUnit.ccm)) },
                        DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL);
                    var tcp = tcpCalculator.Calculate(dvh, numberOfFractions);

                    // Stop adding points at the plateau.
                    if (tcp.Value > maxTcpValue && !Equals(tcp.Value, 1.0)) break;
                    tcpDoseTuples.Add(new TcpDoseTuple(tcp, dose));
                }
                catch
                {
                    // ignored
                }
            }

            return RemoveLowerPlateau(tcpDoseTuples);
        }

        private static List<TcpDoseTuple> RemoveLowerPlateau(List<TcpDoseTuple> tcpDoseTuples)
        {
            var tuples = new List<TcpDoseTuple>(tcpDoseTuples.Count);
            for (var i = 0; i < tcpDoseTuples.Count - 1; i++)
            {
                var tuple = tcpDoseTuples[i];
                var nextTuple = tcpDoseTuples[i + 1];

                if (!Equals(tuple.TumorControlProbability.Value, 0.0))
                {
                    tuples.Add(tuple);
                }
                else if (!Equals(tuple.TumorControlProbability.Value, nextTuple.TumorControlProbability.Value))
                {
                    tuples.Add(tuple);
                }
            }

            tuples.Add(tcpDoseTuples.Last());
            return tuples;
        }

        private static List<NtcpDoseTuple> RemoveLowerPlateau(List<NtcpDoseTuple> ntcpDoseTuples)
        {
            var tuples = new List<NtcpDoseTuple>(ntcpDoseTuples.Count);
            for (var i = 0; i < ntcpDoseTuples.Count - 1; i++)
            {
                var tuple = ntcpDoseTuples[i];
                var nextTuple = ntcpDoseTuples[i + 1];

                if (!Equals(tuple.NormalTissueComplicationProbability.Value, 0.0))
                {
                    tuples.Add(tuple);
                }
                else if (!Equals(tuple.NormalTissueComplicationProbability.Value, nextTuple.NormalTissueComplicationProbability.Value))
                {
                    tuples.Add(tuple);
                }
            }

            tuples.Add(ntcpDoseTuples.Last());
            return tuples;
        }
    }
}
