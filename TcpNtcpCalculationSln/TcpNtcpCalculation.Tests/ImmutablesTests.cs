﻿using System;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class ImmutablesTests
    {
        [TestCase(0.0)]
        [TestCase(-1.0)]
        public void ThrowArgumentExceptionIf_AlphaOverBetaIsLowerOrEqualZero_Test(double value)
        {
            Assert.Throws<ArgumentException>(() => new AlphaOverBeta(value, UDose.UDoseUnit.Gy));
        }

        [TestCase(-1.0)]
        [TestCase(1.1)]
        public void ThrowArgumentExceptionIf_ComplicationFreeTumorControl_IsLowerZeroOrLargerOne_Test(double value)
        {
            Assert.Throws<ArgumentException>(() => new ComplicationFreeTumorControl(value));
        }

        [TestCase(0.0)]
        [TestCase(-1.0)]
        public void ThrowArgumentExceptionIf_DegreeOfSeriality_IsLowerOrEqualZero_Test(double value)
        {
            Assert.Throws<ArgumentException>(() => new DegreeOfSeriality(value));
        }

        [TestCase(-1.0)]
        public void ThrowArgumentExceptionIf_FiftyPercentControlRate_IsLowerThanZero_Test(double value)
        {
            Assert.Throws<ArgumentException>(() => new FiftyPercentControlRate(value, UDose.UDoseUnit.Gy));
        }

        [TestCase(-1.0)]
        public void ThrowArgumentExceptionIf_IsoeffectiveDoseIn2Gy_IsLowerThanZero_Test(double value)
        {
            Assert.Throws<ArgumentException>(() => new IsoeffectiveDoseIn2Gy(value, UDose.UDoseUnit.Gy));
        }

        [TestCase(-1.0)]
        [TestCase(1.1)]
        public void ThrowArgumentExceptionIf_TumorControlProbability_IsLowerZeroOrHigherOne_Test(double value)
        {
            Assert.Throws<ArgumentException>(() => new TumorControlProbability(value));
        }

        [TestCase(-1.0)]
        [TestCase(1.1)]
        public void ThrowArgumentExceptionIf_NormalTissueComplicationProbability_IsLowerZeroOrHigherOne_Test(double value)
        {
            Assert.Throws<ArgumentException>(() => new NormalTissueComplicationProbability(value));
        }

        [TestCase(-1.0)]
        public void ThrowArgumentExceptionIf_Slope_IsLowerZero_Test(double value)
        {
            Assert.Throws<ArgumentException>(() => new Slope(value));
        }

        [TestCase(-1.0)]
        [TestCase(1.1)]
        public void ThrowArgumentExceptionIf_VoxelResponse_IsLowerZeroOrLargerOne_Test(double value)
        {
            Assert.Throws<ArgumentException>(() => new VoxelResponse(value));
        }
    }
}
