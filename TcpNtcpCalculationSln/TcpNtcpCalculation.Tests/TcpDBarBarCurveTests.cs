﻿using System.Collections.Generic;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.DBarBar;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    class TcpDBarBarCurveTests
    {
        [TestCase(0, 10)]
        [TestCase(0.8, 26)]
        [TestCase(0.7, 24)]
        [TestCase(0.75, 24)]
        [TestCase(1, 30)]
        public void GetDBarBarWithTcp_Test(double tcpValue, double expectedResultValue)
        {
            var curve = new TcpDBarBarCurve(new List<TcpDBarBarPoint>
            {
                new TcpDBarBarPoint(new TumorControlProbability(0.0),
                    new BiologicallyEffectiveUniformDose(10, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.1),
                    new BiologicallyEffectiveUniformDose(12, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.2),
                    new BiologicallyEffectiveUniformDose(14, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.3),
                    new BiologicallyEffectiveUniformDose(16, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.4),
                    new BiologicallyEffectiveUniformDose(18, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.5),
                    new BiologicallyEffectiveUniformDose(20, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.6),
                    new BiologicallyEffectiveUniformDose(22, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.7),
                    new BiologicallyEffectiveUniformDose(24, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.8),
                    new BiologicallyEffectiveUniformDose(26, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.9),
                    new BiologicallyEffectiveUniformDose(28, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(1.0),
                    new BiologicallyEffectiveUniformDose(30, UDose.UDoseUnit.Gy)),
            });

            var expectedResult = new BiologicallyEffectiveUniformDose(expectedResultValue, UDose.UDoseUnit.Gy);
            var result = curve.GetDBarBarWithTcp(new TumorControlProbability(tcpValue));
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase(0, 12)]
        [TestCase(0.8, 26)]
        [TestCase(0.7, 24)]
        [TestCase(0.75, 24)]
        [TestCase(1, 28)]
        public void GetDBarBarWithTcp_LowerAndUpperTcpBoundsNotDefined_Test(double tcpValue, double expectedResultValue)
        {
            var curve = new TcpDBarBarCurve(new List<TcpDBarBarPoint>
            {
                new TcpDBarBarPoint(new TumorControlProbability(0.1),
                    new BiologicallyEffectiveUniformDose(12, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.2),
                    new BiologicallyEffectiveUniformDose(14, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.3),
                    new BiologicallyEffectiveUniformDose(16, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.4),
                    new BiologicallyEffectiveUniformDose(18, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.5),
                    new BiologicallyEffectiveUniformDose(20, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.6),
                    new BiologicallyEffectiveUniformDose(22, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.7),
                    new BiologicallyEffectiveUniformDose(24, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.8),
                    new BiologicallyEffectiveUniformDose(26, UDose.UDoseUnit.Gy)),
                new TcpDBarBarPoint(new TumorControlProbability(0.9),
                    new BiologicallyEffectiveUniformDose(28, UDose.UDoseUnit.Gy)),
            });

            var expectedResult = new BiologicallyEffectiveUniformDose(expectedResultValue, UDose.UDoseUnit.Gy);
            var result = curve.GetDBarBarWithTcp(new TumorControlProbability(tcpValue));
            Assert.AreEqual(expectedResult, result);
        }
    }
}
