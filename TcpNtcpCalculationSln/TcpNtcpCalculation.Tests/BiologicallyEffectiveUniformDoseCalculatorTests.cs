﻿using System;
using System.Diagnostics;
using System.Linq;
using MathNet.Numerics;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class BiologicallyEffectiveUniformDoseCalculatorTests
    {
        [TestCase(0.999, 111.938)]
        [TestCase(0.01, 40.19153)]
        [TestCase(0.3, 51.6033)]
        [TestCase(0.5, 56.3)]
        [TestCase(0.7, 61.9518)]
        [TestCase(0.8, 65.9414)]
        public void CalculateBiologicallyEffectiveUniformDose_From_NtcpWithRelativeSerialityCalculator_Test(double ntcpValue, double expectedResultValue)
        {
            var ntcpCalculator = new NtcpWithRelativeSerialityCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(3.0, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(56.3, UDose.UDoseUnit.Gy),
                    new Slope(2.3)), new DegreeOfSeriality(1));

            var ntcp = new NormalTissueComplicationProbability(ntcpValue);

            var result =
                ntcpCalculator.CalculateBiologicallyEffectiveUniformDose(ntcp, 12,
                    new UVolume(1, UVolume.VolumeUnit.ccm));
            var expectedResult = new BiologicallyEffectiveUniformDose(expectedResultValue, UDose.UDoseUnit.Gy);
            Assert.AreEqual(expectedResult.Value, result.Value, 0.01);
        }

        [TestCase(0.999, 115.5541)]
        [TestCase(0.3, 15.9786)]
        [TestCase(0.5, 23.73)]
        [TestCase(0.7, 33.0576)]
        [TestCase(0.8, 39.6419)]
        public void CalculateBiologicallyEffectiveUniformDose_TcpWithNormalizedVolumeCorrectionCalculator_AsInput_Test(double tcpValue, double expectedResultValue)
        {
            var numberOfFractions = 12u;
            var tcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(26.53, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(23.73, UDose.UDoseUnit.Gy),
                    new Slope(0.487)));

            var tcp = new TumorControlProbability(tcpValue);

            var result = tcpCalculator.CalculateBiologicallyEffectiveUniformDose(tcp, numberOfFractions,
                new UVolume(1, UVolume.VolumeUnit.ccm));
            var expectedResult = new BiologicallyEffectiveUniformDose(expectedResultValue, UDose.UDoseUnit.Gy);

            Assert.AreEqual(expectedResult.Value, result.Value, 0.01);
        }

        [Test]
        public void CalculateBiologicallyEffectiveUniformDose_PerformanceMeasurementWrtTime_Test()
        {

            var numberOfFractions = 12u;
            var tcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(26.53, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(23.73, UDose.UDoseUnit.Gy),
                    new Slope(0.487)));

            var tcps = Generate.LinearRange(0.0, 0.01, 1)
                .Select(x => new TumorControlProbability(x));

            var sw = new Stopwatch();
            sw.Start();
            foreach (var tcp in tcps)
            {
                var result = tcpCalculator.CalculateBiologicallyEffectiveUniformDose(tcp, numberOfFractions,
                    new UVolume(1, UVolume.VolumeUnit.ccm));
            }
            sw.Stop();
            var elapsed = sw.Elapsed;
            Assert.LessOrEqual(elapsed.Seconds, 3);
        }

        [Test]
        public void CalculateBiologicallyEffectiveUniformDose_ArgumentOutOfRangeException_For0Fractions_Tests()
        {
            var tcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(26.53, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(23.73, UDose.UDoseUnit.Gy),
                    new Slope(0.487)));

            var tcp = new TumorControlProbability(0.5);
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                tcpCalculator.CalculateBiologicallyEffectiveUniformDose(tcp, 0u,
                    new UVolume(1, UVolume.VolumeUnit.ccm)));
        }

        [Test]
        public void CalculateBiologicallyEffectiveUniformDose_ArgumentOutOfRangeException_ForNonAbsoluteVolume_Tests()
        {
            var tcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(26.53, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(23.73, UDose.UDoseUnit.Gy),
                    new Slope(0.487)));

            var tcp = new TumorControlProbability(0.5);
            Assert.Throws<ArgumentException>(() =>
                tcpCalculator.CalculateBiologicallyEffectiveUniformDose(tcp, 1u,
                    new UVolume(1, UVolume.VolumeUnit.percent)));
            Assert.Throws<ArgumentException>(() =>
                tcpCalculator.CalculateBiologicallyEffectiveUniformDose(tcp, 1u,
                    new UVolume(1, UVolume.VolumeUnit.Undefined)));
        }

        [Test]
        public void CalculateBiologicallyEffectiveUniformDose_1ccmStructureVolume_Test()
        {
            var tcpCalculator = new PoissonTcpWithDensityAndAlphaCalculator(new ClonogenicCellDensity(2.8e8),
                new Alpha(0.1205), new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(1.6, UDose.UDoseUnit.Gy)));

            var tcp = new TumorControlProbability(0.99);

            var expectedResult = new BiologicallyEffectiveUniformDose(88.71, UDose.UDoseUnit.Gy);
            var result =
                tcpCalculator.CalculateBiologicallyEffectiveUniformDose(tcp, 20u,
                    new UVolume(1, UVolume.VolumeUnit.ccm));

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void CalculateBiologicallyEffectiveUniformDose_50ccmStructureVolume_Test()
        {
            var tcpCalculator = new PoissonTcpWithDensityAndAlphaCalculator(new ClonogenicCellDensity(2.8e8),
                new Alpha(0.1205), new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(1.6, UDose.UDoseUnit.Gy)));

            var tcp = new TumorControlProbability(0.99);

            var expectedResult = new BiologicallyEffectiveUniformDose(103.13, UDose.UDoseUnit.Gy);
            var result =
                tcpCalculator.CalculateBiologicallyEffectiveUniformDose(tcp, 20u,
                    new UVolume(50, UVolume.VolumeUnit.ccm));

            Assert.AreEqual(expectedResult, result);
        }
    }
}
