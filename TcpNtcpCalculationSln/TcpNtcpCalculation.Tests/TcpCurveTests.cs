﻿using System.Collections.Generic;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    class TcpCurveTests
    {
        [TestCase(0, 10)]
        [TestCase(0.8, 26)]
        [TestCase(0.7, 24)]
        [TestCase(0.75, 24)]
        [TestCase(1, 30)]
        public void GetDBarBarWithTcp_Test(double tcpValue, double expectedResultValue)
        {
            var curve = new TcpCurve(new List<TcpCurvePoint>
            {
                new TcpCurvePoint(new TumorControlProbability(0.0),
                    new UDose(10, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.1),
                    new UDose(12, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.2),
                    new UDose(14, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.3),
                    new UDose(16, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.4),
                    new UDose(18, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.5),
                    new UDose(20, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.6),
                    new UDose(22, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.7),
                    new UDose(24, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.8),
                    new UDose(26, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.9),
                    new UDose(28, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(1.0),
                    new UDose(30, UDose.UDoseUnit.Gy)),
            });

            var expectedResult = new UDose(expectedResultValue, UDose.UDoseUnit.Gy);
            var result = curve.GetDoseWithTcp(new TumorControlProbability(tcpValue));
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase(0, 12)]
        [TestCase(0.8, 26)]
        [TestCase(0.7, 24)]
        [TestCase(0.75, 24)]
        [TestCase(1, 28)]
        public void GetDBarBarWithTcp_LowerAndUpperTcpBoundsNotDefined_Test(double tcpValue, double expectedResultValue)
        {
            var curve = new TcpCurve(new List<TcpCurvePoint>
            {
                new TcpCurvePoint(new TumorControlProbability(0.1),
                    new UDose(12, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.2),
                    new UDose(14, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.3),
                    new UDose(16, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.4),
                    new UDose(18, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.5),
                    new UDose(20, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.6),
                    new UDose(22, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.7),
                    new UDose(24, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.8),
                    new UDose(26, UDose.UDoseUnit.Gy)),
                new TcpCurvePoint(new TumorControlProbability(0.9),
                    new UDose(28, UDose.UDoseUnit.Gy)),
            });

            var expectedResult = new UDose(expectedResultValue, UDose.UDoseUnit.Gy);
            var result = curve.GetDoseWithTcp(new TumorControlProbability(tcpValue));
            Assert.AreEqual(expectedResult, result);
        }
    }
}
