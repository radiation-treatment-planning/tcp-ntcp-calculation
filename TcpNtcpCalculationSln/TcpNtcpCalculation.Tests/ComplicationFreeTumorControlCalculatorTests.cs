﻿using System.Collections.Generic;
using NUnit.Framework;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class ComplicationFreeTumorControlCalculatorTests
    {
        [TestCase(0.0, 1.0, 0.0)]
        [TestCase(1.0, 0.0, 1.0)]
        [TestCase(0.1, 0.4, 0.06)]
        [TestCase(0.4, 0.1, 0.36)]
        public void Calculate_Test(double tcp, double ntcp, double expectedResult)
        {
            var complicationFreeTumorControlCalculator =
                new ComplicationFreeTumorControlCalculator();
            var tcpNtcpPoint = new TcpNtcpResult(new TumorControlProbability(tcp),
                new NormalTissueComplicationProbability(ntcp));
            var expectedComplicationFreeTumorControl = new ComplicationFreeTumorControl(expectedResult);
            Assert.AreEqual(expectedComplicationFreeTumorControl.Value, complicationFreeTumorControlCalculator.Calculate(tcpNtcpPoint).Value, 1E-04);
        }

        [Test]
        public void Calculate_ProbabilityOfCureAndInjury_AsArguments_Test()
        {
            var complicationFreeTumorControlCalculator = new ComplicationFreeTumorControlCalculator();
            var probabilityOfCureCalculationResult = new ProbabilityOfCureCalculationResult(new List<TcpPoint>
            {
                new TcpPoint("S1", new TumorControlProbability(0.90)),
                new TcpPoint("S2", new TumorControlProbability(0.87))
            }, new ProbabilityOfCure(0.783));

            var probabilityOfInjuryCalculationResult = new ProbabilityOfInjuryCalculationResult(new List<NtcpPoint>
            {
                new NtcpPoint("S3", new NormalTissueComplicationProbability(0.12), 1),
                new NtcpPoint("S4", new NormalTissueComplicationProbability(0.10), 1),
            }, new ProbabilityOfInjury(0.208));

            var probabilityOfInjuryCalculationResult2 = new ProbabilityOfInjuryCalculationResult(new List<NtcpPoint>
            {
                new NtcpPoint("S3", new NormalTissueComplicationProbability(0.12), 0.5),
                new NtcpPoint("S4", new NormalTissueComplicationProbability(0.10), 1),
            }, new ProbabilityOfInjury(0.604));

            var expectedComplicationFreeTumorControl1 = new ComplicationFreeTumorControl(0.620136);
            var expectedComplicationFreeTumorControl2 = new ComplicationFreeTumorControl(0.310068);

            var result1 = complicationFreeTumorControlCalculator.Calculate(probabilityOfCureCalculationResult,
                probabilityOfInjuryCalculationResult);
            var result2 = complicationFreeTumorControlCalculator.Calculate(probabilityOfCureCalculationResult,
                probabilityOfInjuryCalculationResult2);

            Assert.AreEqual(expectedComplicationFreeTumorControl1, result1);
            Assert.AreEqual(expectedComplicationFreeTumorControl2, result2);
        }
    }

}
