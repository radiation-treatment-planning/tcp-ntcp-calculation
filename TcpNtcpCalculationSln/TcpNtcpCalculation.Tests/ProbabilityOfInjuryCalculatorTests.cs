﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.DVH;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class ProbabilityOfInjuryCalculatorTests
    {
        [Test]
        public void Constructor_ThrowArgumentNullExceptionForNullArgument_Test()
        {
            Assert.Throws<ArgumentNullException>(() => new ProbabilityOfInjuryCalculator(null));
        }

        [Test]
        public void Constructor_ThrowArgumentException_ForEmptyArgument_Test()
        {
            Assert.Throws<ArgumentException>(() => new ProbabilityOfInjuryCalculator(new List<NtcpCalculationConfig>()));
        }

        [Test]
        public void Calculate_ThrowArgumentNullExceptionForNullArgument_Test()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.OrganAtRisk.AlphaOverBeta);
            var poissonVoxelResponseCalculator =
                new PoissonVoxelResponseCalculator(poissonParameterConfig.OrganAtRisk.D50,
                    poissonParameterConfig.OrganAtRisk.Gamma);
            var poissonNtcpCalculator = new NtcpWithRelativeSerialityCalculator(poissonIsoeffectiveDoseIn2GyConverter,
                poissonVoxelResponseCalculator, poissonParameterConfig.OrganAtRisk.DegreeOfSeriality);

            var config1 = new NtcpCalculationConfig(poissonNtcpCalculator, "OAR", 0.5,
                poissonParameterConfig.OrganAtRisk.AlphaOverBeta);
            var config2 = new NtcpCalculationConfig(poissonNtcpCalculator, "OAR2", 1,
                poissonParameterConfig.OrganAtRisk.AlphaOverBeta);

            var calculator = new ProbabilityOfInjuryCalculator(new List<NtcpCalculationConfig> { config1, config2 });

            Assert.Throws<ArgumentNullException>(() => calculator.Calculate(null, 12));
        }

        [Test]
        public void Calculate_ThrowArgumentOutOfRangeException_ForNumberOfFractionsLower1_Test()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.OrganAtRisk.AlphaOverBeta);
            var poissonVoxelResponseCalculator =
                new PoissonVoxelResponseCalculator(poissonParameterConfig.OrganAtRisk.D50,
                    poissonParameterConfig.OrganAtRisk.Gamma);
            var poissonNtcpCalculator = new NtcpWithRelativeSerialityCalculator(poissonIsoeffectiveDoseIn2GyConverter,
                poissonVoxelResponseCalculator, poissonParameterConfig.OrganAtRisk.DegreeOfSeriality);

            var config1 = new NtcpCalculationConfig(poissonNtcpCalculator, "OAR", 0.5,
                poissonParameterConfig.OrganAtRisk.AlphaOverBeta);
            var config2 = new NtcpCalculationConfig(poissonNtcpCalculator, "OAR2", 1,
                poissonParameterConfig.OrganAtRisk.AlphaOverBeta);

            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/DVH_OAR.txt");
            var dvhCurve = dvhFileParser.GetDVHCurve();

            var structureDvhTuples = new StructureDvhTuple[]
                { new StructureDvhTuple("OAR", dvhCurve), new StructureDvhTuple("OAR2", dvhCurve) };

            var calculator = new ProbabilityOfInjuryCalculator(new List<NtcpCalculationConfig> { config1, config2 });

            Assert.Throws<ArgumentOutOfRangeException>(() => calculator.Calculate(structureDvhTuples, 0));

        }

        [Test]
        public void Calculate_Test()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.OrganAtRisk.AlphaOverBeta);
            var poissonVoxelResponseCalculator =
                new PoissonVoxelResponseCalculator(poissonParameterConfig.OrganAtRisk.D50,
                    poissonParameterConfig.OrganAtRisk.Gamma);
            var poissonNtcpCalculator = new NtcpWithRelativeSerialityCalculator(poissonIsoeffectiveDoseIn2GyConverter,
                poissonVoxelResponseCalculator, poissonParameterConfig.OrganAtRisk.DegreeOfSeriality);

            var config1 = new NtcpCalculationConfig(poissonNtcpCalculator, "OAR", 0.5,
                poissonParameterConfig.OrganAtRisk.AlphaOverBeta);
            var config2 = new NtcpCalculationConfig(poissonNtcpCalculator, "OAR2", 1,
                poissonParameterConfig.OrganAtRisk.AlphaOverBeta);

            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/DVH_OAR.txt");
            var dvhCurve = dvhFileParser.GetDVHCurve();

            var structureDvhTuple1 = new StructureDvhTuple("OAR", dvhCurve);
            var structureDvhTuple2 = new StructureDvhTuple("OAR2", dvhCurve);
            var structureDvhTuples = new[] { structureDvhTuple1, structureDvhTuple2 };

            var calculator = new ProbabilityOfInjuryCalculator(new List<NtcpCalculationConfig> { config1, config2 });

            var ntcp1 = config1.NtcpCalculator.Calculate(structureDvhTuple1.DvhCurve, 12);
            var ntcp2 = config2.NtcpCalculator.Calculate(structureDvhTuple2.DvhCurve, 12);
            var expectedResultValue = 1 - config1.Weight * (1 - ntcp1.Value) * (config2.Weight * (1 - ntcp2.Value));

            var ntcpPoint1 = new NtcpPoint("OAR", ntcp1, config1.Weight);
            var ntcpPoint2 = new NtcpPoint("OAR2", ntcp2, config2.Weight);
            var expectedResult = new ProbabilityOfInjuryCalculationResult(new NtcpPoint[] { ntcpPoint1, ntcpPoint2 },
                new ProbabilityOfInjury(expectedResultValue));

            var result = calculator.Calculate(structureDvhTuples, 12);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void Calculate_ThrowArgumentException_IfTcpCalculatorIsDefined_ButNoAccordingDvhCurve_Test()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.OrganAtRisk.AlphaOverBeta);
            var poissonVoxelResponseCalculator =
                new PoissonVoxelResponseCalculator(poissonParameterConfig.OrganAtRisk.D50,
                    poissonParameterConfig.OrganAtRisk.Gamma);
            var poissonNtcpCalculator = new NtcpWithRelativeSerialityCalculator(poissonIsoeffectiveDoseIn2GyConverter,
                poissonVoxelResponseCalculator, poissonParameterConfig.OrganAtRisk.DegreeOfSeriality);

            var config1 = new NtcpCalculationConfig(poissonNtcpCalculator, "OAR", 0.5,
                poissonParameterConfig.OrganAtRisk.AlphaOverBeta);
            var config2 = new NtcpCalculationConfig(poissonNtcpCalculator, "OAR2", 1,
                poissonParameterConfig.OrganAtRisk.AlphaOverBeta);

            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/DVH_OAR.txt");
            var dvhCurve = dvhFileParser.GetDVHCurve();

            var structureDvhTuple1 = new StructureDvhTuple("something else", dvhCurve);
            var structureDvhTuple2 = new StructureDvhTuple("something else 2", dvhCurve);
            var structureDvhTuples = new[] { structureDvhTuple1, structureDvhTuple2 };

            var calculator = new ProbabilityOfInjuryCalculator(new List<NtcpCalculationConfig> { config1, config2 });


            Assert.Throws<ArgumentException>(() => calculator.Calculate(structureDvhTuples, 12));
        }
    }
}
