﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    class PoissonTcpWithDensityAndAlphaCalculatorTests
    {
        private DVHCurve _dvhCurve;
        private DVHCurve _dvhCurveWithLowDose;
        private DVHCurve _dvhCurveCmmPoints;

        [SetUp]
        public void SetUp()
        {
            var doseVolumePoints = new List<Tuple<UDose, UVolume>>
            {
                new Tuple<UDose, UVolume>(new UDose(50, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(51, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(52, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(53, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(54, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(55, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(56, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(57, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(58, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(59, UDose.UDoseUnit.Gy), new UVolume(125, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(60, UDose.UDoseUnit.Gy), new UVolume(120, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(61, UDose.UDoseUnit.Gy), new UVolume(110, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(62, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(63, UDose.UDoseUnit.Gy), new UVolume(85, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(64, UDose.UDoseUnit.Gy), new UVolume(60, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(65, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(66, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(67, UDose.UDoseUnit.Gy), new UVolume(15, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(68, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(69, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(2, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm))
            };
            _dvhCurve = new DVHCurve(doseVolumePoints, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var lowDoseVolumePoints = new List<Tuple<UDose, UVolume>>
            {
                new Tuple<UDose, UVolume>(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(21, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(22, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(23, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(24, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(25, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(26, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(27, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(28, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(29, UDose.UDoseUnit.Gy), new UVolume(125, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(120, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(31, UDose.UDoseUnit.Gy), new UVolume(110, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(32, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(33, UDose.UDoseUnit.Gy), new UVolume(85, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(34, UDose.UDoseUnit.Gy), new UVolume(60, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(35, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(36, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(37, UDose.UDoseUnit.Gy), new UVolume(15, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(38, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(39, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(40, UDose.UDoseUnit.Gy), new UVolume(2, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(40, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm))
            };
            _dvhCurveWithLowDose = new DVHCurve(lowDoseVolumePoints, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            var cmmDoseVolumePoints = new List<Tuple<UDose, UVolume>>
            {
                new Tuple<UDose, UVolume>(new UDose(28, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(29, UDose.UDoseUnit.Gy), new UVolume(125, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(120, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(31, UDose.UDoseUnit.Gy), new UVolume(110, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(32, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(33, UDose.UDoseUnit.Gy), new UVolume(85, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(34, UDose.UDoseUnit.Gy), new UVolume(60, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(35, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(36, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(37, UDose.UDoseUnit.Gy), new UVolume(15, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(38, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(39, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(40, UDose.UDoseUnit.Gy), new UVolume(2, UVolume.VolumeUnit.cmm)),
                new Tuple<UDose, UVolume>(new UDose(40, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.cmm))
            };
            _dvhCurveCmmPoints = new DVHCurve(cmmDoseVolumePoints, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
        }

        [TestCase(280000000, 0.52107, 0.98, 1.0)]
        [TestCase(280000000, 0.07, 0.98, 0.8911)]
        [TestCase(280000000, 0.52107, 3, 1.0)]
        public void Calculate_Test(double cellDensityValue, double alphaValue, double alphaOverBetaValue, double expectedTcp)
        {
            var calculator = new PoissonTcpWithDensityAndAlphaCalculator(new ClonogenicCellDensity(cellDensityValue),
                new Alpha(alphaValue),
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(alphaOverBetaValue, UDose.UDoseUnit.Gy)));

            var result = calculator.Calculate(_dvhCurve, 12);
            Assert.AreEqual(expectedTcp, result.Value, 0.0001);
        }


        [TestCase(280000000, 0.52107, 0.98, 12u, 1.0)]
        [TestCase(280000000, 0.52107, 0.98, 40u, 0.9717)]
        [TestCase(2800000000, 0.52107, 0.98, 40u, 0.7505)]
        [TestCase(280000000, 0.52107, 1.5, 40u, 0.2785)]
        [TestCase(280000000, 0.47, 0.98, 40u, 0.6981)]
        public void Calculate_DvhWithLowerDoes_Test(double cellDensityValue, double alphaValue, double alphaOverBetaValue,
            uint numberOfFractions, double expectedTcp)
        {
            var calculator = new PoissonTcpWithDensityAndAlphaCalculator(new ClonogenicCellDensity(cellDensityValue),
                new Alpha(alphaValue),
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(alphaOverBetaValue, UDose.UDoseUnit.Gy)));

            var result = calculator.Calculate(_dvhCurveWithLowDose, numberOfFractions);
            Assert.AreEqual(expectedTcp, result.Value, 0.0001);
        }

        [Test]
        public void Calculate_ThrowArgumentException_ForDvhWithWrongVolumeUnit_Test()
        {
            var calculator = new PoissonTcpWithDensityAndAlphaCalculator(new ClonogenicCellDensity(280000000),
                new Alpha(0.52107),
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(0.98, UDose.UDoseUnit.Gy)));

            Assert.Throws<ArgumentException>(() => calculator.Calculate(_dvhCurveCmmPoints, 40));
        }

        [Test]
        public void GetParametersDictionary_Test()
        {
            var tcpCalculator = new PoissonTcpWithDensityAndAlphaCalculator(new ClonogenicCellDensity(10000),
                new Alpha(10), new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(20, UDose.UDoseUnit.Gy)));

            var expectedResult = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("Alpha/Beta", 20, Option.Some(UDose.UDoseUnit.Gy.ToString()))
                .WithParameterSet("Density", 10000, Option.Some($"/{UVolume.VolumeUnit.ccm.ToString()}"))
                .WithParameterSet("Alpha", 10, Option.None<string>())
                .Build();

            var result = tcpCalculator.GetParametersDictionary();

            Assert.AreEqual(expectedResult, result);
        }
    }
}
