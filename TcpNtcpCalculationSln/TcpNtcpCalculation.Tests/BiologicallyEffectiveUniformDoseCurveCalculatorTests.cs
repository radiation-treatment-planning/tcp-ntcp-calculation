﻿using System;
using System.Linq;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.DBarBar;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class BiologicallyEffectiveUniformDoseCurveCalculatorTests
    {
        [Test]
        public void CalculateTcpAndDBarBarCurve_Test()
        {
            var tcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(26.53, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(23.73, UDose.UDoseUnit.Gy),
                    new Slope(0.487)));

            var result = tcpCalculator.CalculateTcpAndDBarBarCurve(12, new UVolume(1, UVolume.VolumeUnit.ccm), 0.1, 200);
            var resultPoints = result.DataPoints.ToArray();
            Assert.AreEqual(2001, result.DataPoints.Count());
            Assert.AreEqual(0.0, resultPoints.First().BiologicallyEffectiveUniformDose.Value, 0.01);
            Assert.AreEqual(200, resultPoints.Last().BiologicallyEffectiveUniformDose.Value, 0.01);
            Assert.AreEqual(0.024, resultPoints.First().TumorControlProbability.Value, 0.01);
            Assert.AreEqual(1.0, resultPoints.Last().TumorControlProbability.Value, 0.01);
        }

        [Test]
        public void CalculateTcpAndDBarBarCurve_ThrowArgumentException_Test()
        {
            var tcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(26.53, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(23.73, UDose.UDoseUnit.Gy),
                    new Slope(0.487)));

            Assert.Throws<ArgumentOutOfRangeException>(() => tcpCalculator.CalculateTcpAndDBarBarCurve(0, new UVolume(1, UVolume.VolumeUnit.ccm), 0.1, 100));
            Assert.Throws<ArgumentOutOfRangeException>(() => tcpCalculator.CalculateTcpAndDBarBarCurve(12, new UVolume(1, UVolume.VolumeUnit.ccm), 0, 100));
            Assert.Throws<ArgumentOutOfRangeException>(() => tcpCalculator.CalculateTcpAndDBarBarCurve(12, new UVolume(1, UVolume.VolumeUnit.ccm), -1, 100));
            Assert.Throws<ArgumentOutOfRangeException>(() => tcpCalculator.CalculateTcpAndDBarBarCurve(12, new UVolume(1, UVolume.VolumeUnit.ccm), 0.1, 0));
            Assert.Throws<ArgumentOutOfRangeException>(() => tcpCalculator.CalculateTcpAndDBarBarCurve(12, new UVolume(1, UVolume.VolumeUnit.ccm), 0.1, -1));
        }


        [Test]
        public void CalculateNtcpAndDBarBarCurve_Test()
        {
            var ntcpCalculator = new NtcpWithRelativeSerialityCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(3.0, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(56.3, UDose.UDoseUnit.Gy),
                    new Slope(2.3)), new DegreeOfSeriality(1));

            var result = ntcpCalculator.CalculateNtcpAndDBarBarCurve(12, new UVolume(1, UVolume.VolumeUnit.ccm), 0.1, 500);
            var resultPoints = result.DataPoints.ToArray();
            Assert.AreEqual(3494, result.DataPoints.Count());
            Assert.AreEqual(22.3, resultPoints.First().BiologicallyEffectiveUniformDose.Value, 0.01);
            Assert.AreEqual(371.6, resultPoints.Last().BiologicallyEffectiveUniformDose.Value, 0.01);
            Assert.AreEqual(0.0, resultPoints.First().NormalTissueComplicationProbability.Value, 0.01);
        }
    }
}
