﻿using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class PoissonVoxelResponseCalculatorTests
    {
        [TestCase(34.5, 2.0, 34.5, 0.5)]
        [TestCase(34.5, 2.0, 0.0, 0.0)]
        [TestCase(34.5, 2.0, 1000, 1.0)]
        [TestCase(34.5, 0.5, 34.5, 0.5)]
        [TestCase(34.5, 0.5, 1000, 1.0)]
        [TestCase(0.5, 0.0, 0.5, 0.5)]
        [TestCase(0.5, 10.0, 0.5, 0.5)]
        public void CalculateProbability_Test(double d50, double gamma, double doseInEqd2, double expectedResponse)
        {
            var fiftyPercentControlRate = new FiftyPercentControlRate(d50, UDose.UDoseUnit.Gy);
            var slope = new Slope(gamma);
            var tcpCalculator = new PoissonVoxelResponseCalculator(fiftyPercentControlRate, slope);
            Assert.AreEqual(expectedResponse,
                tcpCalculator.Calculate(new IsoeffectiveDoseIn2Gy(doseInEqd2, UDose.UDoseUnit.Gy)).Value, 1E-4);
        }

        [Test]
        public void GetParametersDictionary_Test()
        {
            var fiftyPercentControlRate = new FiftyPercentControlRate(20.0, UDose.UDoseUnit.Gy);
            var slope = new Slope(5.0);
            var tcpCalculator = new PoissonVoxelResponseCalculator(fiftyPercentControlRate, slope);

            var expectedResult = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("D50", fiftyPercentControlRate.Value,
                    Option.Some(fiftyPercentControlRate.DoseUnit.ToString()))
                .WithParameterSet("Gamma", slope.Value, Option.None<string>())
                .Build();

            var result = tcpCalculator.GetParametersDictionary();
            Assert.AreEqual(expectedResult, result);
        }
    }
}
