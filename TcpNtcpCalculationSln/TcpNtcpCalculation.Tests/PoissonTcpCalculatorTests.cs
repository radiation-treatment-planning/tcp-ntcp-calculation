﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class TcpCalculatorTests
    {
        private ITumorControlProbabilityCalculator _poissonTcpCalculator;
        private ITumorControlProbabilityCalculator _logisticTcpCalculator;

        [SetUp]
        public void SetUp()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var logisticParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForLogisticModel();

            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.TargetVolume.AlphaOverBeta);
            var logisticIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(logisticParameterConfig.TargetVolume.AlphaOverBeta);

            var poissonVoxelResponseCalculator = new PoissonVoxelResponseCalculator(poissonParameterConfig.TargetVolume.D50, poissonParameterConfig.TargetVolume.Gamma);
            var logisticVoxelResponseCalculator = new LogisticVoxelResponseCalculator(logisticParameterConfig.TargetVolume.D50, logisticParameterConfig.TargetVolume.Gamma);

            _poissonTcpCalculator =
                new TcpWithNormalizedVolumeCorrectionCalculator(poissonIsoeffectiveDoseIn2GyConverter, poissonVoxelResponseCalculator);
            _logisticTcpCalculator =
                new TcpWithNormalizedVolumeCorrectionCalculator(logisticIsoeffectiveDoseIn2GyConverter, logisticVoxelResponseCalculator);
        }

        [Test]
        public void Calculate_PoissonVoxelResponseCalculator_CumulativeDVHCurveAsInput_Tests()
        {

            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/DVH_GTV.txt");
            var dvhCurve = dvhFileParser.GetDVHCurve();

            Assert.AreEqual(0.955,
                _poissonTcpCalculator.Calculate(dvhCurve, 12).Value, 1E-3);
        }

        [Test]
        public void Calculate_PoissonVoxelResponseCalculator_CumulativeDVHCurveAsInput_CustomDvh_Tests()
        {
            var doseVolumePoints = new List<Tuple<UDose, UVolume>>
            {
                new Tuple<UDose, UVolume>(new UDose(50, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(51, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(52, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(53, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(54, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(55, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(56, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(57, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(58, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(59, UDose.UDoseUnit.Gy), new UVolume(125, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(60, UDose.UDoseUnit.Gy), new UVolume(120, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(61, UDose.UDoseUnit.Gy), new UVolume(110, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(62, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(63, UDose.UDoseUnit.Gy), new UVolume(85, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(64, UDose.UDoseUnit.Gy), new UVolume(60, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(65, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(66, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(67, UDose.UDoseUnit.Gy), new UVolume(15, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(68, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(69, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(2, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm))
            };
            var dvhCurve = new DVHCurve(doseVolumePoints, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            Assert.AreEqual(0.975261,
                _poissonTcpCalculator.Calculate(dvhCurve, 12).Value, 1E-3);
        }
        [Test]
        public void Calculate_LogisticVoxelResponseCalculator_CumulativeDVHCurveAsInput_Tests()
        {

            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/DVH_GTV.txt");
            var dvhCurve = dvhFileParser.GetDVHCurve();

            Assert.AreEqual(0.945,
                _logisticTcpCalculator.Calculate(dvhCurve, 12).Value, 1E-3);
        }

        [Test]
        public void Calculate_DVHCurveOfUnknownDoseTypeAsInput_Tests()
        {
            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>() { }, DVHCurve.Type.CUMULATIVE,
                DVHCurve.DoseType.UNDEFINED);

            Assert.Throws<ArgumentException>(() => _poissonTcpCalculator.Calculate(dvhCurve, 12));
            Assert.Throws<ArgumentException>(() => _logisticTcpCalculator.Calculate(dvhCurve, 12));
        }

        [Test]
        public void GetParametersDictionary_Test()
        {
            var tcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(10.0, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(20, UDose.UDoseUnit.cGy),
                    new Slope(30)));

            var expectedResult = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("Alpha/Beta", 10, Option.Some<string>(UDose.UDoseUnit.Gy.ToString()))
                .WithParameterSet("D50", 20, Option.Some<string>(UDose.UDoseUnit.cGy.ToString()))
                .WithParameterSet("Gamma", 30, Option.None<string>())
                .Build();

            var result = tcpCalculator.GetParametersDictionary();

            Assert.AreEqual(expectedResult, result);
        }
    }
}
