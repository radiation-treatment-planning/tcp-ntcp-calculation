﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.DVH;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    class ProbabilityOfCureCalculatorTests
    {
        [Test]
        public void Constructor_ThrowArgumentNullExceptionForNullArgument_Test()
        {
            Assert.Throws<ArgumentNullException>(() => new ProbabilityOfCureCalculator(null));
        }

        [Test]
        public void Constructor_ThrowArgumentException_ForEmptyArgument_Test()
        {
            Assert.Throws<ArgumentException>(() => new ProbabilityOfCureCalculator(new List<TcpCalculationConfig>()));
        }

        [Test]
        public void Calculate_ThrowArgumentNullExceptionForNullArgument_Test()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.TargetVolume.AlphaOverBeta);
            var poissonVoxelResponseCalculator =
                new PoissonVoxelResponseCalculator(poissonParameterConfig.TargetVolume.D50,
                    poissonParameterConfig.TargetVolume.Gamma);
            var poissonTcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(poissonIsoeffectiveDoseIn2GyConverter,
                poissonVoxelResponseCalculator);

            var config1 = new TcpCalculationConfig(poissonTcpCalculator, "GTV",
                poissonParameterConfig.TargetVolume.AlphaOverBeta);
            var config2 = new TcpCalculationConfig(poissonTcpCalculator, "GTV2",
                poissonParameterConfig.TargetVolume.AlphaOverBeta);

            var calculator = new ProbabilityOfCureCalculator(new List<TcpCalculationConfig> { config1, config2 });

            Assert.Throws<ArgumentNullException>(() => calculator.Calculate(null, 12));
        }

        [Test]
        public void Calculate_ThrowArgumentOutOfRangeException_ForNumberOfFractionsLower1_Test()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.TargetVolume.AlphaOverBeta);
            var poissonVoxelResponseCalculator =
                new PoissonVoxelResponseCalculator(poissonParameterConfig.TargetVolume.D50,
                    poissonParameterConfig.TargetVolume.Gamma);
            var poissonTcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(poissonIsoeffectiveDoseIn2GyConverter,
                poissonVoxelResponseCalculator);

            var config1 = new TcpCalculationConfig(poissonTcpCalculator, "GTV",
                poissonParameterConfig.TargetVolume.AlphaOverBeta);
            var config2 = new TcpCalculationConfig(poissonTcpCalculator, "GTV2",
                poissonParameterConfig.TargetVolume.AlphaOverBeta);

            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/DVH_GTV.txt");
            var dvhCurve = dvhFileParser.GetDVHCurve();

            var structureDvhTuples = new StructureDvhTuple[]
                { new StructureDvhTuple("GTV", dvhCurve), new StructureDvhTuple("GTV2", dvhCurve) };

            var calculator = new ProbabilityOfCureCalculator(new List<TcpCalculationConfig> { config1, config2 });

            Assert.Throws<ArgumentOutOfRangeException>(() => calculator.Calculate(structureDvhTuples, 0));

        }

        [Test]
        public void Calculate_Test()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.TargetVolume.AlphaOverBeta);
            var poissonVoxelResponseCalculator =
                new PoissonVoxelResponseCalculator(poissonParameterConfig.TargetVolume.D50,
                    poissonParameterConfig.TargetVolume.Gamma);
            var poissonTcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(poissonIsoeffectiveDoseIn2GyConverter,
                poissonVoxelResponseCalculator);

            var config1 = new TcpCalculationConfig(poissonTcpCalculator, "GTV",
                poissonParameterConfig.TargetVolume.AlphaOverBeta);
            var config2 = new TcpCalculationConfig(poissonTcpCalculator, "GTV2",
                poissonParameterConfig.TargetVolume.AlphaOverBeta);

            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/DVH_GTV.txt");
            var dvhCurve = dvhFileParser.GetDVHCurve();

            var structureDvhTuple1 = new StructureDvhTuple("GTV", dvhCurve);
            var structureDvhTuple2 = new StructureDvhTuple("GTV2", dvhCurve);
            var structureDvhTuples = new[] { structureDvhTuple1, structureDvhTuple2 };

            var calculator = new ProbabilityOfCureCalculator(new List<TcpCalculationConfig> { config1, config2 });

            var tcp1 = config1.TcpCalculator.Calculate(structureDvhTuple1.DvhCurve, 12);
            var tcp2 = config2.TcpCalculator.Calculate(structureDvhTuple2.DvhCurve, 12);
            var expectedResultValue = tcp1.Value * tcp2.Value;

            var tcpPoint1 = new TcpPoint("GTV", tcp1);
            var tcpPoint2 = new TcpPoint("GTV2", tcp2);
            var expectedResult = new ProbabilityOfCureCalculationResult(new TcpPoint[] { tcpPoint1, tcpPoint2 },
                new ProbabilityOfCure(expectedResultValue));

            var result = calculator.Calculate(structureDvhTuples, 12);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void Calculate_ThrowArgumentException_IfTcpCalculatorIsDefined_ButNoAccordingDvhCurve_Test()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.TargetVolume.AlphaOverBeta);
            var poissonVoxelResponseCalculator =
                new PoissonVoxelResponseCalculator(poissonParameterConfig.TargetVolume.D50,
                    poissonParameterConfig.TargetVolume.Gamma);
            var poissonTcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(poissonIsoeffectiveDoseIn2GyConverter,
                poissonVoxelResponseCalculator);

            var config1 = new TcpCalculationConfig(poissonTcpCalculator, "GTV",
                poissonParameterConfig.TargetVolume.AlphaOverBeta);
            var config2 = new TcpCalculationConfig(poissonTcpCalculator, "GTV2",
                poissonParameterConfig.TargetVolume.AlphaOverBeta);

            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/DVH_GTV.txt");
            var dvhCurve = dvhFileParser.GetDVHCurve();

            var structureDvhTuple1 = new StructureDvhTuple("sommething else", dvhCurve);
            var structureDvhTuple2 = new StructureDvhTuple("something else 2", dvhCurve);
            var structureDvhTuples = new[] { structureDvhTuple1, structureDvhTuple2 };

            var calculator = new ProbabilityOfCureCalculator(new List<TcpCalculationConfig> { config1, config2 });

            Assert.Throws<ArgumentException>(() => calculator.Calculate(structureDvhTuples, 12));
        }
    }
}
