﻿using System;
using System.Linq;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.DBarBar;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    class TcpCurveCalculatorTests
    {
        [Test]
        public void CalculateTcpCurve_TcpWithNormalizedVolumeCorrectionCalculator_Test()
        {
            var tcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(26.53, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(23.73, UDose.UDoseUnit.Gy),
                    new Slope(0.487)));

            var result = tcpCalculator.CalculateTcpCurve(12, Option.None<UVolume>(), 0.1, 200);
            var resultPoints = result.CurvePoints.ToArray();
            Assert.AreEqual(2001, result.CurvePoints.Count());
            Assert.AreEqual(0.0, resultPoints.First().Dose.Value, 0.01);
            Assert.AreEqual(200, resultPoints.Last().Dose.Value, 0.01);
            Assert.AreEqual(0.023, resultPoints.First().TumorControlProbability.Value, 0.01);
            Assert.AreEqual(1.0, resultPoints.Last().TumorControlProbability.Value, 0.01);
        }

        [Test]
        public void CalculateTcpCurve_PoissonTcpWithDensityAndAlphaCalculator_Test()
        {
            var tcpCalculator = new PoissonTcpWithDensityAndAlphaCalculator(new ClonogenicCellDensity(2.8e8),
                new Alpha(0.52107), new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(1.6, UDose.UDoseUnit.Gy)));

            var result = tcpCalculator.CalculateTcpCurve(20, Option.None<UVolume>(), 0.1, 200);
            var resultPoints = result.CurvePoints.ToArray();
            Assert.AreEqual(291, result.CurvePoints.Count());
            Assert.AreEqual(16.3, resultPoints.First().Dose.Value, 0.01);
            Assert.AreEqual(45.3, resultPoints.Last().Dose.Value, 0.01);
            Assert.AreEqual(0, resultPoints.First().TumorControlProbability.Value, 0.01);
            Assert.AreEqual(21.3, resultPoints[50].Dose.Value, 0.01);
            Assert.AreEqual(0.07, resultPoints[50].TumorControlProbability.Value, 0.01);
            Assert.AreEqual(24.1, resultPoints[78].Dose.Value, 0.01);
            Assert.AreEqual(0.93, resultPoints[78].TumorControlProbability.Value, 0.01);
            Assert.AreEqual(1.0, resultPoints.Last().TumorControlProbability.Value, 0.01);
        }

        [Test]
        public void CalculateTcpCurve_PoissonTcpWithDensityAndAlphaCalculator_CustomStructureVolumeOf100ccm_Test()
        {
            var tcpCalculator = new PoissonTcpWithDensityAndAlphaCalculator(new ClonogenicCellDensity(2.8e8),
                new Alpha(0.52107), new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(1.6, UDose.UDoseUnit.Gy)));

            var result =
                tcpCalculator.CalculateTcpCurve(20, Option.Some(new UVolume(100, UVolume.VolumeUnit.ccm)), 0.1, 200);
            var resultPoints = result.CurvePoints.ToArray();
            Assert.AreEqual(272, result.CurvePoints.Count());
            Assert.AreEqual(20.4, resultPoints.First().Dose.Value, 0.01);
            Assert.AreEqual(47.5, resultPoints.Last().Dose.Value, 0.01);
            Assert.AreEqual(0, resultPoints.First().TumorControlProbability.Value, 0.01);
            Assert.AreEqual(22.7, resultPoints[23].Dose.Value, 0.01);
            Assert.AreEqual(0, resultPoints[23].TumorControlProbability.Value, 0.01);
            Assert.AreEqual(25.4, resultPoints[50].Dose.Value, 0.01);
            Assert.AreEqual(0.25, resultPoints[50].TumorControlProbability.Value, 0.01);
            Assert.AreEqual(26.1, resultPoints[57].Dose.Value, 0.01);
            Assert.AreEqual(0.59, resultPoints[57].TumorControlProbability.Value, 0.01);
            Assert.AreEqual(27.6, resultPoints[72].Dose.Value, 0.01);
            Assert.AreEqual(0.94, resultPoints[72].TumorControlProbability.Value, 0.01);
            Assert.AreEqual(1.0, resultPoints.Last().TumorControlProbability.Value, 0.01);
        }

        [Test]
        public void CalculateTcpCurve_ThrowArgumentExceptions_Test()
        {
            var tcpCalculator = new TcpWithNormalizedVolumeCorrectionCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(26.53, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(23.73, UDose.UDoseUnit.Gy),
                    new Slope(0.487)));

            Assert.Throws<ArgumentOutOfRangeException>(() => tcpCalculator.CalculateTcpCurve(0, Option.None<UVolume>(), 0.1, 200));
            Assert.Throws<ArgumentOutOfRangeException>(() => tcpCalculator.CalculateTcpCurve(12, Option.None<UVolume>(), 0.0, 200));
            Assert.Throws<ArgumentOutOfRangeException>(() => tcpCalculator.CalculateTcpCurve(12, Option.None<UVolume>(), -0.1, 200));
            Assert.Throws<ArgumentOutOfRangeException>(() => tcpCalculator.CalculateTcpCurve(12, Option.None<UVolume>(), 0.1, 0));
            Assert.Throws<ArgumentOutOfRangeException>(() => tcpCalculator.CalculateTcpCurve(12, Option.None<UVolume>(), 0.1, -1));
        }
    }
}
