﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class LymanKutcherBurmanNtcpCalculatorTests
    {
        private DVHCurve _dvhCurve;
        private DVHCurve _dvhCurveEqd2;

        [SetUp]
        public void SetUp()
        {
            var doseVolumePoints = new List<Tuple<UDose, UVolume>>
            {
                new Tuple<UDose, UVolume>(new UDose(50, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(51, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(52, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(53, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(54, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(55, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(56, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(57, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(58, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(59, UDose.UDoseUnit.Gy), new UVolume(125, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(60, UDose.UDoseUnit.Gy), new UVolume(120, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(61, UDose.UDoseUnit.Gy), new UVolume(110, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(62, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(63, UDose.UDoseUnit.Gy), new UVolume(85, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(64, UDose.UDoseUnit.Gy), new UVolume(60, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(65, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(66, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(67, UDose.UDoseUnit.Gy), new UVolume(15, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(68, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(69, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(2, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm))
            };
            _dvhCurve = new DVHCurve(doseVolumePoints, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            _dvhCurveEqd2 = new DVHCurve(doseVolumePoints, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.EQD2);
        }

        [TestCase(50.0, 2.3, 0.98, 12u, 0.5458)]
        [TestCase(40.0, 2.3, 0.98, 12u, 0.5997)]
        [TestCase(80.0, 2.3, 0.98, 12u, 0.4637)]
        [TestCase(50, 1.5, 0.98, 12u, 0.5701)]
        [TestCase(50, 3.5, 0.98, 12u, 0.5302)]
        [TestCase(50.0, 2.3, 0.1, 12u, 0.5475)]
        [TestCase(50.0, 2.3, 0.98, 8u, 0.5459)]
        public void Calculate_EQD2_DVH_AsInput_Test(double td50ValueInGy, double mValue, double nValue,
            uint numberOfFractions, double expectedResultValue)
        {
            var td50 = new TD50(td50ValueInGy, UDose.UDoseUnit.Gy);
            var m = new Slope(mValue);
            var n = new N(nValue);

            var ntcpCalculator = new LymanKutcherBurmanNtcpCalculator(td50, m, n,
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(3, UDose.UDoseUnit.Gy)));
            var result = ntcpCalculator.Calculate(_dvhCurveEqd2, numberOfFractions);

            Assert.AreEqual(expectedResultValue, result.Value, 0.001);
        }

        [TestCase(50.0, 2.3, 0.98, 12u, 0.6829)]
        [TestCase(40.0, 2.3, 0.98, 12u, 0.7592)]
        [TestCase(80.0, 2.3, 0.98, 12u, 0.5535)]
        [TestCase(50, 1.5, 0.98, 12u, 0.7672)]
        [TestCase(50, 3.5, 0.98, 12u, 0.6228)]
        [TestCase(50.0, 2.3, 0.1, 12u, 0.6897)]
        [TestCase(50.0, 2.3, 0.98, 8u, 0.7782)]
        public void Calculate_Physical_DVH_AsInput_Test(double td50ValueInGy, double mValue, double nValue,
            uint numberOfFractions, double expectedResultValue)
        {
            var td50 = new TD50(td50ValueInGy, UDose.UDoseUnit.Gy);
            var m = new Slope(mValue);
            var n = new N(nValue);

            IIsoeffectiveDoseIn2GyConverter eqd2Converter =
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(3, UDose.UDoseUnit.Gy));
            var ntcpCalculator = new LymanKutcherBurmanNtcpCalculator(td50, m, n, eqd2Converter);

            var result = ntcpCalculator.Calculate(_dvhCurve, numberOfFractions);

            Assert.AreEqual(expectedResultValue, result.Value, 0.001);
        }

        private DVHCurve ConvertDvhCurveToEQD2(DVHCurve dvhCurve,
            IIsoeffectiveDoseIn2GyConverter isoeffectiveDoseIn2GyConverter, uint numberOfFractions)
        {
            if (dvhCurve.TypeOfDose == DVHCurve.DoseType.EQD2) return dvhCurve;
            var doseVolumeTuples = dvhCurve.CurvePoints.Select(x =>
            {
                var eqd2Dose = isoeffectiveDoseIn2GyConverter.Convert(x.Item1, numberOfFractions);
                var dose = new UDose(eqd2Dose.Value, eqd2Dose.Unit);
                return Tuple.Create(dose, x.Item2);
            });

            return new DVHCurve(doseVolumeTuples.ToList(), dvhCurve.DVHType, DVHCurve.DoseType.EQD2);
        }

        [Test]
        public void GetParametersDictionary_Test()
        {
            var ntcpCalculator = new LymanKutcherBurmanNtcpCalculator(new TD50(10, UDose.UDoseUnit.Gy), new Slope(5),
                new N(20), new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(30, UDose.UDoseUnit.cGy)));

            var expectedResult = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("Alpha/Beta", 30, Option.Some<string>(UDose.UDoseUnit.cGy.ToString()))
                .WithParameterSet("TD50", 10, Option.Some<string>(UDose.UDoseUnit.Gy.ToString()))
                .WithParameterSet("M", 5, Option.None<string>())
                .WithParameterSet("N", 20, Option.None<string>())
                .Build();

            var result = ntcpCalculator.GetParametersDictionary();

            Assert.AreEqual(expectedResult, result);
        }
    }
}
