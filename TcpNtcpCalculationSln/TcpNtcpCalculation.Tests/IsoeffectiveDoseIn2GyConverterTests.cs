﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using System;
using Optional;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class IsoeffectiveDoseIn2GyConverterTests
    {
        [TestCase(10u, 10, 62, 83.7)]
        [TestCase(10u, 10, 32, 35.2)]
        [TestCase(10u, 1, 32, 44.8)]
        [TestCase(1u, 10, 32, 112)]
        public void Convert_Test(uint numberOfFractions, double alphaOverBeta, double physicalDose,
            double expectedResponse)
        {
            var physicalUDose = new UDose(physicalDose, UDose.UDoseUnit.Gy);
            var isoeffectiveDoseIn2GyCalculator = new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(alphaOverBeta, UDose.UDoseUnit.Gy));
            var expectedResponseUDose = new UDose(expectedResponse, physicalUDose.Unit);

            Assert.AreEqual(expectedResponseUDose.Value,
                isoeffectiveDoseIn2GyCalculator.Convert(physicalUDose, numberOfFractions).Value, 1E-4);
            Assert.AreEqual(expectedResponseUDose.Unit,
                isoeffectiveDoseIn2GyCalculator.Convert(physicalUDose, numberOfFractions).Unit);
        }


        [Test]
        public void Convert_ThrowArgumentExceptionIfNumberOfFractionsIsZero_Test()
        {
            var converter = new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(1.0, UDose.UDoseUnit.Gy));
            Assert.Throws<ArgumentException>(() => converter.Convert(new UDose(60, UDose.UDoseUnit.Gy), 0));
        }

        [Test]
        public void GetParametersDictionary_Test()
        {
            var converter = new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(10.0, UDose.UDoseUnit.Gy));
            var expectedResult = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("Alpha/Beta", 10.0, Option.Some(UDose.UDoseUnit.Gy.ToString()))
                .Build();
            var result = converter.GetParametersDictionary();

            Assert.AreEqual(expectedResult, result);
        }
    }
}
