﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class BiologicallyEffectiveUniformDoseConverterTests
    {
        [Test]
        public void ToString_Test()
        {
            var dose = new BiologicallyEffectiveUniformDose(1.03, UDose.UDoseUnit.Gy);
            var dose2 = new BiologicallyEffectiveUniformDose(2.73, UDose.UDoseUnit.cGy);

            Assert.AreEqual($"1.03{UDose.UDoseUnit.Gy.ToDescription()}", dose.ToString());
            Assert.AreEqual($"2.73{UDose.UDoseUnit.cGy.ToDescription()}", dose2.ToString());
        }

        [Test]
        public void ToBiologicallyEffectiveUniformDose_Test()
        {
            var expectedDose = new BiologicallyEffectiveUniformDose(1.03, UDose.UDoseUnit.Gy);
            var expectedDose2 = new BiologicallyEffectiveUniformDose(2.73, UDose.UDoseUnit.cGy);

            var doseAsString = $"1.03{UDose.UDoseUnit.Gy.ToDescription()}";
            var doseAsString2 = $"2.73{UDose.UDoseUnit.cGy.ToDescription()}";

            var result = doseAsString.ToBiologicallyEffectiveUniformDose();
            var result2 = doseAsString2.ToBiologicallyEffectiveUniformDose();

            Assert.AreEqual(expectedDose, result);
            Assert.AreEqual(expectedDose2, result2);
        }
    }
}
