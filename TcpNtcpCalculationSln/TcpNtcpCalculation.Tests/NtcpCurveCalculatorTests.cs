﻿using System;
using System.Linq;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.DBarBar;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class NtcpCurveCalculatorTests
    {
        [Test]
        public void CalculateNtcpCurve_Test()
        {
            var ntcpCalculator = new NtcpWithRelativeSerialityCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(3.0, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(56.3, UDose.UDoseUnit.Gy),
                    new Slope(2.3)), new DegreeOfSeriality(1));

            var result = ntcpCalculator.CalculateNtcpCurve(12, Option.None<UVolume>(), 0.1, 500);
            var resultPoints = result.CurvePoints.ToArray();
            Assert.AreEqual(1097, result.CurvePoints.Count());
            Assert.AreEqual(22.8, resultPoints.First().Dose.Value, 0.01);
            Assert.AreEqual(132.4, resultPoints.Last().Dose.Value, 0.01);
            Assert.AreEqual(0.0, resultPoints.First().NormalTissueComplicationProbability.Value, 0.01);
            Assert.AreEqual(1.0, resultPoints.Last().NormalTissueComplicationProbability.Value, 0.01);
        }

        [Test]
        public void CalculateNtcpCurve_ThrowArgumentExceptions_Test()
        {
            var ntcpCalculator = new NtcpWithRelativeSerialityCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(3.0, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(56.3, UDose.UDoseUnit.Gy),
                    new Slope(2.3)), new DegreeOfSeriality(1));

            Assert.Throws<ArgumentOutOfRangeException>(() => ntcpCalculator.CalculateNtcpCurve(0, Option.None<UVolume>(), 0.1, 500));
            Assert.Throws<ArgumentOutOfRangeException>(() => ntcpCalculator.CalculateNtcpCurve(12, Option.None<UVolume>(), 0, 500));
            Assert.Throws<ArgumentOutOfRangeException>(() => ntcpCalculator.CalculateNtcpCurve(12, Option.None<UVolume>(), -0.1, 500));
            Assert.Throws<ArgumentOutOfRangeException>(() => ntcpCalculator.CalculateNtcpCurve(12, Option.None<UVolume>(), 0.1, 0));
            Assert.Throws<ArgumentOutOfRangeException>(() => ntcpCalculator.CalculateNtcpCurve(12, Option.None<UVolume>(), 0.1, -1));
        }
    }
}
