﻿using System;
using NUnit.Framework;
using Optional;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    class TcpNtcpParametersDictionaryBuilderTests
    {
        [Test]
        public void Build_Test()
        {
            var expectedResult = new TcpNtcpParametersDictionary();
            expectedResult.AddParameterSet("S1", new ValueUnitTuple(10.0, Option.None<string>()));
            expectedResult.AddParameterSet("S2", new ValueUnitTuple(5.0, Option.Some("ccm")));

            var result = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("S1", 10.0, Option.None<string>())
                .WithParameterSet("S2", 5.0, Option.Some("ccm"))
                .Build();

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void WithParameterSet_ThrowArgumentNullException_ForNullArgument_Test()
        {
            Assert.Throws<ArgumentNullException>(() => TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet(null, 10.0, Option.None<string>()));
        }
    }
}
