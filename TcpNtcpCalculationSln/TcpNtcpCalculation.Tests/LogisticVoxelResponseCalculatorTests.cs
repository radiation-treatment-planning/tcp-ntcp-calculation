﻿using System;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class LogisticVoxelResponseCalculatorTests
    {
        [TestCase(17.6, 0.64, 0, 0)]
        [TestCase(17.6, 0.64, 5, 0.038358700691102)]
        [TestCase(17.6, 0.64, 10, 0.190432346172875)]
        [TestCase(17.6, 0.64, 20, 0.581090944629615)]
        [TestCase(17.6, 0.64, 30, 0.796610680817586)]
        [TestCase(17.6, 0.64, 40, 0.891069291389735)]
        [TestCase(17.6, 0.64, 50, 0.935411956)]
        [TestCase(17.6, 0.64, 50, 0.935411956)]
        [TestCase(25.37, 0.801, 0, 0)]
        [TestCase(25.37, 0.801, 5, 0.005466089178289)]
        [TestCase(25.37, 0.801, 10, 0.04820590378916)]
        [TestCase(25.37, 0.801, 20, 0.318207068930365)]
        [TestCase(25.37, 0.801, 30, 0.63113441414049)]
        [TestCase(25.37, 0.801, 40, 0.811352116116094)]
        [TestCase(25.37, 0.801, 50, 0.89786936399424)]
        public void Calculate(double fiftyPercentControlRate, double slope, double isoeffectiveDoseIn2Gy, double expectedTcp)
        {
            var d50 = new FiftyPercentControlRate(fiftyPercentControlRate, UDose.UDoseUnit.Gy);
            var s = new Slope(slope);
            var eqd2 = new IsoeffectiveDoseIn2Gy(isoeffectiveDoseIn2Gy, UDose.UDoseUnit.Gy);
            var expectedResult = new TumorControlProbability(expectedTcp);

            var logisticVoxelResponseCalculator = new LogisticVoxelResponseCalculator(d50, s);
            var result = logisticVoxelResponseCalculator.Calculate(eqd2);

            Assert.AreEqual(expectedResult.Value, result.Value, 0.001);
        }

        [Test]
        public void Calculate_ThrowExceptionIfDoseUnitsDoNotMatch_Test()
        {
            var d50 = new FiftyPercentControlRate(10, UDose.UDoseUnit.Gy);
            var slope = new Slope(10);
            var isoeffectiveDoseIn2Gy = new IsoeffectiveDoseIn2Gy(10, UDose.UDoseUnit.Percent);

            var logisticVoxelResponseCalculator = new LogisticVoxelResponseCalculator(d50, slope);
            Assert.Throws<ArgumentException>(() => logisticVoxelResponseCalculator.Calculate(isoeffectiveDoseIn2Gy));
        }

        [Test]
        public void GetParametersDictionary_Test()
        {
            var fiftyPercentControlRate = new FiftyPercentControlRate(20.0, UDose.UDoseUnit.Gy);
            var slope = new Slope(5.0);
            var tcpCalculator = new LogisticVoxelResponseCalculator(fiftyPercentControlRate, slope);

            var expectedResult = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("D50", fiftyPercentControlRate.Value,
                    Option.Some(fiftyPercentControlRate.DoseUnit.ToString()))
                .WithParameterSet("Gamma", slope.Value, Option.None<string>())
                .Build();

            var result = tcpCalculator.GetParametersDictionary();
            Assert.AreEqual(expectedResult, result);
        }
    }
}
