﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Optional;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    class TcpNtcpParametersDictionaryTests
    {
        [Test]
        public void GetAllParameterSets_Test()
        {
            var dict = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("S1", 10.0, Option.None<string>())
                .WithParameterSet("S2", 5.0, Option.Some("ccm"))
                .Build();

            var expectedResult = new List<TcpNtcpParameterSet>
            {
                new TcpNtcpParameterSet("S1", new ValueUnitTuple(10.0, Option.None<string>())),
                new TcpNtcpParameterSet("S2", new ValueUnitTuple(5.0, Option.Some<string>("ccm"))),
            };

            var result = dict.GetAllParameterSets();

            Assert.AreEqual(expectedResult.Count, result.Count());
            foreach (var parameterSet in result) Assert.Contains(parameterSet, expectedResult);
        }

        [Test]
        public void AddParameterSet_Test()
        {
            var dict = new TcpNtcpParametersDictionary();
            dict.AddParameterSet("S1", new ValueUnitTuple(10.0, Option.None<string>()));
            dict.AddParameterSet("S2", new ValueUnitTuple(5.0, Option.Some("ccm")));

            var expectedResult = new List<TcpNtcpParameterSet>
            {
                new TcpNtcpParameterSet("S1", new ValueUnitTuple(10.0, Option.None<string>())),
                new TcpNtcpParameterSet("S2", new ValueUnitTuple(5.0, Option.Some("ccm"))),
            };

            var result = dict.GetAllParameterSets();

            Assert.AreEqual(expectedResult.Count, result.Count());
            foreach (var parameterSet in result) Assert.Contains(parameterSet, expectedResult);
        }

        [Test]
        public void AddParameterSet_ThrowArgumentNullException_ForNullArgument_Test()
        {
            var dict = new TcpNtcpParametersDictionary();
            Assert.Throws<ArgumentNullException>(() =>
                dict.AddParameterSet(null, new ValueUnitTuple(10.0, Option.None<string>())));
            Assert.Throws<ArgumentNullException>(() =>
                dict.AddParameterSet("S1", null));
        }

        [Test]
        public void Equals_Test()
        {
            var dict = new TcpNtcpParametersDictionary();
            dict.AddParameterSet("S1", new ValueUnitTuple(10.0, Option.None<string>()));
            dict.AddParameterSet("S2", new ValueUnitTuple(5.0, Option.Some("ccm")));

            var dict2 = new TcpNtcpParametersDictionary();
            dict2.AddParameterSet("S1", new ValueUnitTuple(10.0, Option.None<string>()));
            dict2.AddParameterSet("S2", new ValueUnitTuple(5.0, Option.Some("ccm")));

            var dict3 = new TcpNtcpParametersDictionary();
            dict3.AddParameterSet("NA", new ValueUnitTuple(10.0, Option.None<string>()));
            dict3.AddParameterSet("S2", new ValueUnitTuple(5.0, Option.Some("ccm")));

            var dict4 = new TcpNtcpParametersDictionary();
            dict4.AddParameterSet("S1", new ValueUnitTuple(20.0, Option.None<string>()));
            dict4.AddParameterSet("S2", new ValueUnitTuple(5.0, Option.Some("ccm")));

            var dict5 = new TcpNtcpParametersDictionary();
            dict5.AddParameterSet("S1", new ValueUnitTuple(10.0, Option.Some("ccm")));
            dict5.AddParameterSet("S2", new ValueUnitTuple(5.0, Option.Some("ccm")));

            var dict6 = new TcpNtcpParametersDictionary();
            dict6.AddParameterSet("S1", new ValueUnitTuple(10.0, Option.None<string>()));
            dict6.AddParameterSet("S2", new ValueUnitTuple(5.0, Option.Some("undefined")));

            var dict7 = new TcpNtcpParametersDictionary();
            dict7.AddParameterSet("S1", new ValueUnitTuple(10.0, Option.None<string>()));

            Assert.AreEqual(dict, dict2);
            Assert.AreNotEqual(dict, dict3);
            Assert.AreNotEqual(dict, dict4);
            Assert.AreNotEqual(dict, dict5);
            Assert.AreNotEqual(dict, dict6);
            Assert.AreNotEqual(dict, dict7);
        }
    }
}
