﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class TcpNtcpParameterConfigTests
    {
        [Test]
        public void CreateDefaultConfigForPoissonModel_Test()
        {
            var config = Parameters.TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();

            Assert.AreEqual(new AlphaOverBeta(26.53, UDose.UDoseUnit.Gy), config.TargetVolume.AlphaOverBeta);
            Assert.AreEqual(new FiftyPercentControlRate(23.73, UDose.UDoseUnit.Gy), config.TargetVolume.D50);
            Assert.AreEqual(new Slope(0.487), config.TargetVolume.Gamma);

            Assert.AreEqual(new AlphaOverBeta(3.0, UDose.UDoseUnit.Gy), config.OrganAtRisk.AlphaOverBeta);
            Assert.AreEqual(new FiftyPercentControlRate(56.3, UDose.UDoseUnit.Gy), config.OrganAtRisk.D50);
            Assert.AreEqual(new Slope(2.3), config.OrganAtRisk.Gamma);
            Assert.AreEqual(new DegreeOfSeriality(1.5), config.OrganAtRisk.DegreeOfSeriality);
        }

        [Test]
        public void CreateDefaultConfigForLogisticModel_Test()
        {
            var config = Parameters.TcpNtcpParameterConfig.CreateDefaultConfigForLogisticModel();

            Assert.AreEqual(new AlphaOverBeta(27.45, UDose.UDoseUnit.Gy), config.TargetVolume.AlphaOverBeta);
            Assert.AreEqual(new FiftyPercentControlRate(25.37, UDose.UDoseUnit.Gy), config.TargetVolume.D50);
            Assert.AreEqual(new Slope(0.801), config.TargetVolume.Gamma);

            Assert.AreEqual(new AlphaOverBeta(3.0, UDose.UDoseUnit.Gy), config.OrganAtRisk.AlphaOverBeta);
            Assert.AreEqual(new FiftyPercentControlRate(56.3, UDose.UDoseUnit.Gy), config.OrganAtRisk.D50);
            Assert.AreEqual(new Slope(2.3), config.OrganAtRisk.Gamma);
            Assert.AreEqual(new DegreeOfSeriality(1.5), config.OrganAtRisk.DegreeOfSeriality);
        }


        [Test]
        public void CreateCustom_Test()
        {
            var organAtRiskParameters = OrganAtRiskParameters.CreateCustomParameters(new FiftyPercentControlRate(17.2, UDose.UDoseUnit.Gy), new Slope(0.1), new AlphaOverBeta(3.4, UDose.UDoseUnit.Gy), new DegreeOfSeriality(8.1));
            var targetVolumeParameters = TargetVolumeParameters.CreateCustomParameters(new FiftyPercentControlRate(16.2, UDose.UDoseUnit.Gy), new Slope(0.5), new AlphaOverBeta(1.3, UDose.UDoseUnit.Gy));
            var config = TcpNtcpParameterConfig.CreateCustom(organAtRiskParameters, targetVolumeParameters);

            Assert.AreEqual(new AlphaOverBeta(1.3, UDose.UDoseUnit.Gy), config.TargetVolume.AlphaOverBeta);
            Assert.AreEqual(new FiftyPercentControlRate(16.2, UDose.UDoseUnit.Gy), config.TargetVolume.D50);
            Assert.AreEqual(new Slope(0.5), config.TargetVolume.Gamma);

            Assert.AreEqual(new AlphaOverBeta(3.4, UDose.UDoseUnit.Gy), config.OrganAtRisk.AlphaOverBeta);
            Assert.AreEqual(new FiftyPercentControlRate(17.2, UDose.UDoseUnit.Gy), config.OrganAtRisk.D50);
            Assert.AreEqual(new Slope(0.1), config.OrganAtRisk.Gamma);
            Assert.AreEqual(new DegreeOfSeriality(8.1), config.OrganAtRisk.DegreeOfSeriality);
        }
    }
}
