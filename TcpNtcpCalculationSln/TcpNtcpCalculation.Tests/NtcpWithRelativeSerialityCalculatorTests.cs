﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Tests
{
    [TestFixture]
    public class NtcpWithRelativeSerialityCalculatorTests
    {
        private NtcpWithRelativeSerialityCalculator _poissonNtcpWithRelativeSerialityCalculator;
        private NtcpWithRelativeSerialityCalculator _logisticNtcpWithRelativeSerialityCalculator;

        [SetUp]
        public void SetUp()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var logisticParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForLogisticModel();

            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.OrganAtRisk.AlphaOverBeta);
            var logisticIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(logisticParameterConfig.OrganAtRisk.AlphaOverBeta);

            var poissonVoxelResponseCalculator = new PoissonVoxelResponseCalculator(poissonParameterConfig.OrganAtRisk.D50, poissonParameterConfig.OrganAtRisk.Gamma);
            var logisticVoxelResponseCalculator = new LogisticVoxelResponseCalculator(logisticParameterConfig.OrganAtRisk.D50, logisticParameterConfig.OrganAtRisk.Gamma);

            _poissonNtcpWithRelativeSerialityCalculator = new NtcpWithRelativeSerialityCalculator(poissonIsoeffectiveDoseIn2GyConverter,
                poissonVoxelResponseCalculator, poissonParameterConfig.OrganAtRisk.DegreeOfSeriality);
            _logisticNtcpWithRelativeSerialityCalculator = new NtcpWithRelativeSerialityCalculator(logisticIsoeffectiveDoseIn2GyConverter,
                logisticVoxelResponseCalculator, poissonParameterConfig.OrganAtRisk.DegreeOfSeriality);
        }

        [Test]
        public void ThrowArgumentNullException_ForNullArguments_Test()
        {
            var poissonParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();

            var poissonIsoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(poissonParameterConfig.OrganAtRisk.AlphaOverBeta);

            var poissonVoxelResponseCalculator = new PoissonVoxelResponseCalculator(poissonParameterConfig.OrganAtRisk.D50, poissonParameterConfig.OrganAtRisk.Gamma);

            Assert.Throws<ArgumentNullException>(() => new NtcpWithRelativeSerialityCalculator(
                null, poissonVoxelResponseCalculator,
                poissonParameterConfig.OrganAtRisk.DegreeOfSeriality));
            Assert.Throws<ArgumentNullException>(() => new NtcpWithRelativeSerialityCalculator(
                poissonIsoeffectiveDoseIn2GyConverter, null,
                poissonParameterConfig.OrganAtRisk.DegreeOfSeriality));
        }

        [Test]
        public void Calculate_PoissonModel_Test()
        {
            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/DVH_OAR.txt");
            var dvhCurve = dvhFileParser.GetDVHCurve();

            Assert.AreEqual(0.145, _poissonNtcpWithRelativeSerialityCalculator.Calculate(dvhCurve, 12).Value, 1E-2);
        }

        [TestCase(56.3, 2.3, 3.0, 1.5, 12u, 0.996812964)]
        [TestCase(56.3, 2.3, 3.0, 1.5, 20u, 0.939091)]
        [TestCase(56.3, 2.3, 3.0, 3, 12u, 0.996812964)]
        [TestCase(56.3, 2.3, 1.3, 1.5, 12u, 0.999677)]
        [TestCase(56.3, 1, 3.0, 1.5, 12u, 0.948952)]
        [TestCase(60, 2.3, 3.0, 1.5, 12u, 0.99342)]
        public void Calculate_PoissonModel_SmallDvh_Test(double d50InGy, double gammaValue, double ab, double s,
            uint numberOfFractions, double expectedNtcp)
        {
            var d50 = new FiftyPercentControlRate(d50InGy, UDose.UDoseUnit.Gy);
            var gamma = new Slope(gammaValue);
            var alphaOverBeta = new AlphaOverBeta(ab, UDose.UDoseUnit.Gy);
            var degreeOfSeriality = new DegreeOfSeriality(s);

            var ntcpCalculator = new NtcpWithRelativeSerialityCalculator(new IsoeffectiveDoseIn2GyConverter(alphaOverBeta),
                new PoissonVoxelResponseCalculator(d50, gamma), degreeOfSeriality);

            var doseVolumePoints = new List<Tuple<UDose, UVolume>>
            {
                new Tuple<UDose, UVolume>(new UDose(50, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(51, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(52, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(53, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(54, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(55, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(56, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(57, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(58, UDose.UDoseUnit.Gy), new UVolume(130, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(59, UDose.UDoseUnit.Gy), new UVolume(125, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(60, UDose.UDoseUnit.Gy), new UVolume(120, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(61, UDose.UDoseUnit.Gy), new UVolume(110, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(62, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(63, UDose.UDoseUnit.Gy), new UVolume(85, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(64, UDose.UDoseUnit.Gy), new UVolume(60, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(65, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(66, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(67, UDose.UDoseUnit.Gy), new UVolume(15, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(68, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(69, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(2, UVolume.VolumeUnit.ccm)),
                new Tuple<UDose, UVolume>(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm))
            };
            var dvhCurve = new DVHCurve(doseVolumePoints, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var result = ntcpCalculator.Calculate(dvhCurve, numberOfFractions);
            Assert.AreEqual(expectedNtcp, result.Value, 0.01);
        }

        [Test]
        public void Calculate_LogisticModel_Test()
        {
            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/DVH_OAR.txt");
            var dvhCurve = dvhFileParser.GetDVHCurve();

            Assert.AreEqual(0.148, _logisticNtcpWithRelativeSerialityCalculator.Calculate(dvhCurve, 12).Value, 1E-2);
        }

        [Test]
        public void Calculate_DVHCurveOfUnknownDoseTypeAsInput_Tests()
        {
            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>() { }, DVHCurve.Type.CUMULATIVE,
                DVHCurve.DoseType.UNDEFINED);

            Assert.Throws<ArgumentException>(() => _poissonNtcpWithRelativeSerialityCalculator.Calculate(dvhCurve, 12));
            Assert.Throws<ArgumentException>(() => _logisticNtcpWithRelativeSerialityCalculator.Calculate(dvhCurve, 12));
        }

        [Test]
        public void GetParametersDictionary_Test()
        {
            var ntcpCalculator = new NtcpWithRelativeSerialityCalculator(
                new IsoeffectiveDoseIn2GyConverter(new AlphaOverBeta(10.0, UDose.UDoseUnit.Gy)),
                new PoissonVoxelResponseCalculator(new FiftyPercentControlRate(20, UDose.UDoseUnit.cGy),
                    new Slope(30)),
                new DegreeOfSeriality(2.4));

            var expectedResult = TcpNtcpParametersDictionaryBuilder.Initialize()
                .WithParameterSet("Alpha/Beta", 10, Option.Some<string>(UDose.UDoseUnit.Gy.ToString()))
                .WithParameterSet("D50", 20, Option.Some<string>(UDose.UDoseUnit.cGy.ToString()))
                .WithParameterSet("Gamma", 30, Option.None<string>())
                .WithParameterSet("Seriality", 2.4, Option.None<string>())
                .Build();

            var result = ntcpCalculator.GetParametersDictionary();

            Assert.AreEqual(expectedResult, result);
        }
    }
}
